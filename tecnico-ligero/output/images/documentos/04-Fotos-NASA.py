#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
import datetime
import random

fecha=datetime.date(random.randint(2005,2022), random.randint(1,12),random.randint(1,28))

numero = random.randint(0,10000)
params = {
    "api_key": "to5gfAyIUfqz7pKwfl1zjRYh06ElbcZXszhmk1Qg",
    "date": fecha
}
#r = requests.get("https://api.nasa.gov/planetary/apod", params=params)
r = requests.get("https://api.nasa.gov/planetary/apod", params=params)
if r.status_code == 200:
    results = r.json()
    url = results["url"]
    # Si es una imagen guardar el archivo.
    if results["media_type"] == "image":
        with open("fotoNASA-"+str(numero)+".jpg", "wb") as f:
            f.write(requests.get(url).content)
        print ("La imagen se guardó corretamente")
    else:
        print(url)
else:
    print("No se pudo obtener la imagen.")
