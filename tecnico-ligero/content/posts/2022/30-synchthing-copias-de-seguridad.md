Title: Probando Syncthing para hacer copias de seguridad por doquier... y librándome de Google de a poco y por qué no, de Dropbox también
Date: 2022-05-29 21:46
Author: Hugovksy
Category: Copias de Seguridad
Tags: copias de seguridad, syncthing
Slug: synchthing-copias-de-seguridad
Imagen: /images/syncthing-por-hugo.jpg


En esas luchas que uno tiene para librarse de las garras del software privativo y por qué no, de los colosos corporativos que lo único que quieren es robarnos nuestra información, además, claro está, de usarnos de conejillos de indias para su maldita inteligencia artificial (si no, qué fue lo que hicimos con Google fotos y su reconocimiento facial, ¿eh, eh?). No señor, no queremos que estas empresas se enriquezcan a costillas nuestras. Mi batalla por estos días es, entonces, tratar de salirme de las garras de Alphabet. Lejos quedaron las buenas intenciones de un cuasi proyecto salido de un garage (aunque sabemos que todo eso también puede no ser tan cierto).

En fin, dejando todo ese preámbulo insensato de lado, me pasé a Dropbox para tratar de escapar de esas garras enormes, pero después de un tiempo también quise escapar de la posibilidad de que mi info pase por algun servidor lejano... y buscando y buscando di con Syncthing, que promete copias de segurdiad descentralizadas. ¡Esto sí punta bueno!

En principio mi idea era hacerme unas humildes copias desde mi celular a un disco en una PC que uso exclusivamente para las fotos. Uno mecánico de 1TB, qué va, que uso solamente para eso. Fotos y videos.

Confieso que al principio, después de estar conforme con Dropbox, me resultó un poco extraña la configuración o cómo debia de sincronizar. Los pasos, para quien pueda llegar a servirle, son los siguientes:

![Synchthing a puro rock](/images/syncthing-por-hugo.jpg){:.img-fluid}

Hola, ¿cómo están? Les comparto esta aplicación, más que interesante, en base a lo compartido y charlado en el encuentro virtual. La misma sirve para tratar de dejar de lado los intermediarios a la hora de resguardar nuestros archivos, es decir, un servidor ajeno a nuestro control, como puede ser el/los de Google Fotos por citar uno de ellos.

Hice un pequeño instructivo a modo de ejemplo en un escenario donde quisiéramos resguardar nuestras fotos desde el celu.

1\) Instalamos la aplicacion que descargamos desde su web <https://syncthing.net/>o bien desde los repos, o bien desde[Fdroid](https://f-droid.org/es/)del celular (otro día vemos esta aplicación excelente para zafar de las garras de App Store). En este caso el primero va a ser CELU

2\) Instalamos también en el otro dispositivo, puede ser una PC, que va a hacer la copia. En este caso va a ser PC.

3\) En CELU primero agregamos una carpeta para compartir.

4\) Acto seguido instalamos en nuestro equipo la aplicación Syncthing. Ejecutamos start Syncthing y después ejecutar Syncthing Web UI, lo que nos abrirá una instancia de navegador con esta dirección local: http://127.0.0.1:8384/

Vamos a la esquina superior derecha y ponemos mostrar ID y lo dejamos ahí...

5\) Volvemos al celu, seleccionamos añadir dispositivo y con el ID de PC le permitimos que pueda agregar la compu.

5\) En la PC van a aparecer mensajes que dirán si queremos agregar el dispositivo nuevo y después la carpeta compartida. Podemos definir la ruta donde pueda estar ubicada ésta físicamente.

6\) Agregamos info de un lado o del otro en las carpetas creadas. A disfrutar pasando de un lado a otro a lo loco.

PD: podemos agregar múltiples carpetas y mútiples dispositivos.

Cabe aclarar que si paso desde la PC a CELU también se sincronizará, es un ida y vuelta infinito amigos.

 

 

 

 
