Title: Un lago...
Date: 2022-07-17 12:41
Autor: Hugovksy 
Slug: un-lago
Lang: es
Tags: lagos
Category: Escribo
Summary: Cuando me imagino por algún lado para descansar siempre pienso en el agua...
Imagen: /images/SMDLA01.jpg


![Foto de un lago](/images/SMDLA01.jpg){:.img-fluid}

Cuando me imagino por algún lado para descansar siempre pienso en el agua. El agua me repone las fuerzas y por qué no, también me inspira a soñar con un mañana mucho mejor. 
A veces, en el trajín diario un podría pensar que el futuro puede ser no muy, digamos, favorable por así decir, pero ver un lago como este, te transporta. No me acuerco cuál es, tampoco importa. No sé si es el Lacar, si es el Correntoso, o son los dos al mismo tiempo. O quizás ninguno. 


    
  
