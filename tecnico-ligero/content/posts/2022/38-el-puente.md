Title: El puente
Date: 2022-07-31 14:13
Autor: Hugovksy 
Slug: el-puente
Lang: es
Tags: Escribo
Category: Escribo
Summary: Había un puente en el futuro o la posibilidad de estar bien todos los días...

Había un puente en el futuro o la posibilidad de estar bien todos los días, levantarse y hacer las cosas que uno tiene ganas, pero no lo veía: ahora lo veo. Y tiene que ver con el software libre y la lucha contra la obsolescencia pprogramada. Usar herramientas libres, restaurar equipos viejos, revivir la revista literaria en breve o cuando se pueda. Escribir cuentos cortos como el que me sugirió este buen hombre, lo encontré en una esquina de la nave... Trato de transcribir lo que soñó. No volví a verlo.

En un tiempo no muy lejando, quizás hacia adelante pero bien podría ser hacia atrás, quién sabe, había una empresa y un desprendimiento de ella, de dos amigos que se pelaron porque querían viajar al espacio, pero cada uno a lugares distintos. El proyecto había comenzado sin mayor objetivo que fijar las estrellas como destino... pero como sucede con muchos proyectos en conjunto de dos cabezas, la minucia lo arruina todo y que yo quería ir a Marte, y que yo quería Saturno... y nos fuimos separando y me fui a fundar mi empresa.

Estuve días buscando a este hombre, al final habla en primera persona, supongo que es ficción porque Marte no está en nuestro sistema solar hace miles de años... es más sólo unos pocos avezados conocen la realidad. 
    
  
