Title: Script Depurador Python de un archivo de texto
Date: 2022-10-26 10:02
Autor: Hugovksy 
Slug: depurador-python-archivo-texto
Lang: es
Tags: Python, depurador, archivo, texto
Category: Python
Summary: Script Depurador Python de un archivo de texto


Supongamos que tenes un archivo de texto con algunos nombres y querés sacarle a todos alguna cosa, como por ejemplo del legajo hasta el final en estas líneas, pero multiplicado por miles:


```
X Departamental;Sofia Gomez;sofia.gomez@xxx.com.ar;Oficial - Legajo 693
X Departamental;Pia Lapa;pia.lapa@xxx.com.ar;Oficial - Legajo 6242
X Departamental;Tobias San;tobias.san@xxx.com.ar;Auxiliar - Legajo 7163
X Departamental;Viviana Gimenez;viviana.gimenez@xxx.com.ar;Auxiliar - Legajo 7957
Archivo de X.;Elena Manchado;e.machado@pxxx.com.ar;Oficial - Legajo 7196
Archivo de X;Jorge Rodriguez;jorge.rodriguez@xxx.com.ar;Oficial - Legajo 1291


```


Entonces, el proceso sería más o menos así:

* Abro el archivo.
* Leo línea por línea.
* Busco la palabra legajo y guardo la posicion en una variable
* La linea ahora va a ir desde el principio hasta donde comienza legajo
* Guardo en un nuevo archivo.

```

archivo = open('usuarios.txt', encoding="utf8")
datos = archivo.readlines()
archivo.close()

for linea in datos:
    dato =linea.find(" - Legajo")
    linea = linea[:dato]
    archivo2 = open('usuarios-sin-legajo.txt', 'a')
    archivo2.write(linea+"\n")

archivo2.close()

``` 

Para probarlo simplemente abrite un Idle de Python, copiate esto de arriba y dale F5, previo a eso genera un archivo de texto con el ejemplo de los usuarios.

[O descargá desde acá](https://gitlab.com/enlacepilar/scripts-de-python/-/raw/main/05-depurador.py?inline=false)
