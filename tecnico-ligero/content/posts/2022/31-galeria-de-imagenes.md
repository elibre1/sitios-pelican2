Title: Probando una galería de imágenes solo html y css
Date: 2022-07-17 12:14
Author: Hugovksy
Slug: galeria-de-imagenes
Category: Proyectos
Tags: Galería, fotos, imágenes, css
Summary: Probando una galería de imágenes solo html y css
Imagen: /images/galeria-fotos/01.jpg


Vamos a ver que tal con algunas fotos de San Martín de los Andes, uno de los lugares más hermosos de mi país...
Está hecho con html y css nomás, lo copié de algún lado que no me acuerdo, ¡pero está genial!
Lo que hice fue lo siguiente, generé un archivo css con el código, un index que apunta al mismo en la parte de "head" 
agregué unas fotos en la misma raíz y listo. 

Acá una captura del resultado:

 
![Galería de fotos](/images/galeria-imagenes.jpg){:.img-fluid}


Desde este enlace podés ver el resultado en vivo y probarlo:

[Galería de fotos en vivo](/images/galeria-fotos/galeria.html)


El css puede descargarse de acá:
[CSS Galería de fotos](/images/galeria-fotos/galeria.css)
