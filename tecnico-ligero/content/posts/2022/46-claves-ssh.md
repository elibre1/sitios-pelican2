Title: Acceder por SSH sin claves
Date: 2022-12-19 21:24
Autor: Hugovksy 
Slug: acceder-por-ssh-sin-claves
Lang: es
Tags: ssh, terminal, gnu-linux, consola
Category: ssh
Summary: Acceder por SSH sin claves


Parace una boludez, pero muchas veces me olvido los comandos y es bueno ternerlo a mano para la próxima.
Primero abrimos la terminal y tipeamos:


```
ssh-keygen

```

Esto nos generará la clave para copiar al servidor remoto.
Como segundo paso la copiamos al servidor

```
ssh-copy-id -p(número de puerto, ej 22) usuario@192.168.0.1

```

¡Te pide la clave del servidor por última vez y listo! A gozarla Billy.

