Title: Estuve probando el generador de sitios estáticos Pelican
Date: 2022-02-05 22:05
Author: Hugovksy
Category: Pelican
Slug: sitio-estaticos-pelican
Status: published
Imagen: /images/blog-elibre.jpg



<img src="/images/blog-elibre.jpg" alt="Logo Elibre" width="300px">

En un momento me cansé un poco de este sitio en Wordpress, más que nada porque me agarró la loca de probar nuevas variantes. Al principio porque estuve leyendo sobre el generador hecho con Ruby, Jekyll, pero como de Ruby conozco poco, me decanté por uno hecho en Python, llamado [Pelican](https://blog.getpelican.com/). A eso vengo para dar mi opinión personal.

La verdad es que me volví loco para aprender el concepto, había cosas que me perdían en Jekyll que capaz con un buen tutorial de Pelican lo podés sacar adelante. Hasta donde puse entender cada vez que escribía generaba todas las páginas de nuevo y eso me empezó a romper las bolas un poco. Está bueno que no consuma bases de datos y que sea más rápido en la carga, sobre todo si lo tenés cargado en una Raspberry Pi como yo. Pero a veces me hacía falta el buscador. Tengo un par de cosas que las hago, en su mayoría, para mí. Entonces cuando instalo un linux nuevo en algún lado o monto una web nueva con apache y todas las boludeces, busco en el blog y copio por ejemplo para instalar los componentes PHP, o cuando me falta algo para Laravel, alguna línea que se me olvidó... eso no lo puedo hacer con un sitio estático, está más que claro. Comentarios no tengo muchos por acá, por no decir nada así que eso vale mierdecilla...

Pero como mi mente no descansa nunca y sigue buscando no sé qué mierda ahora estoy preparando mi propio sistema de blog en Laravel... asi que veremos si lo subo. Por el momento he sacado de línea el sitio estático en Pelican y vuelvo a este en Worpress para las cosillas diarias, oh si.

Acá describo algunos pasos como para empezar de a poquito:

1) Instalamos Python3 en nuestra PC:  

```
    sudo apt install python3

```

2) Instalamos venv para aislar el entorno (un principio de Python que si no lo conocés deberías investigar):

```
    sudo apt install python3-venv

```

3) Generamos la carpeta que va a tener todos los elementos de Python aislados, como la dependencia Pelican.

```
    python3 -m venv envsitioPelican

```

4) Activamos el entorno virtual. vamos a notar que se activó porque aparece el nombre de la carpeta entre paréntesis en la consola:

```
    source envsitioPelican/bin/activate

```

5) Entramos a la carpeta de nuestro futuro proyecto. No te preocupes si el envSitioPelican está en otra carpeta. A mí me parece más ordenado. Hay quienes lo hacen en la misma carpeta donde está su blog. Da igual.

```
    cd sitioPelican

```

6) Instalamos Pelican con opción para lenguaje de marcado Markdown. Notá que ya no usamos python3 como sentencia si no simplemente python

```
    python -m pip install "pelican\[markdown\]

```

7) Ahora sí, cramos nuestra carpeta del blog:

```
    mkdir nuevo\_blog

```

8) Entramos a la misma y ejecutamos la sentencia de pelican que nos va a permitir automatizar algunas cosillas:

```
    cd nuevo\_blog pelican-quickstart

```

9) Luego de respondidas estas preguntas (a las que no sabés dales que no y ya), vamos a ver algunos detalles para la generación del contenido. Un tema muy simple pero que a mí me costó un tanto comprender.  
En la carpeta **content** está todo lo que escribís en markdown, después en la carpeta **ouput** sale todo lo que está listo para subir  
En *content* creá la carpeta *pages* para las paginas estáticas. Podemos tener 2 estilos de páginas, las estáticas y los artículos del blog.

10) Cuando todo está terminado como mencioné arriba, todo lo de content va a ir a parar a output, pero formateado para que pueda usarse en un servidor web.  
Escribimos la sentencias que exporta:

```
    pelicant content

```

11) Finalmente, antes de subir con esta sentencia podemos crear un servidor local para ver cómo queda exportado todo.

```
    pelican –-listen

```

Obviamente hay mucho más por investigar, como los [temas para el blog](http://pelicanthemes.com/), que es un aspecto interesantísimo, sobre todo para lavarle la cara a nuestro sitio. Dejo de muestra lo que hice:

Este mismo sitio al día de hoy está hecho en Pelican. Una cosa a destacar que me hizo modiicicar todos los artículos por sugerencia de <a href="https://pornohardware.com/2017/03/23/adios-octopress-hola-pelican/" target="_blank">este sitio</a> es el hecho de tenerlos ordenados por fechas. Yo preferí por números, me resulta más sencillo. Es algo a tener en cuenta definitivamente.

Este sitio donde tengo una radio guerrilla anti obsolescencia también está hecha con Pelican:

<a href="https://elibre.netlify.app/" target="_blank">Radio Elibre</a>



 
