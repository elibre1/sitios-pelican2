Title: Mi laberinto
Date: 2022-07-24 14:31
Autor: Hugovksy 
Slug: mi-laberinto
Lang: es
Tags: laberinto
Category: Escribo
Summary: Mi laberinto tiene forma de pasillos de interminables escuelas...

Mi laberinto tiene forma de pasillos de interminables escuelas; cuando veo la luz del día en esa especie de patio subo y me pierdo por escaleras que me llevan a otras puertas que se pierden en otros pisos, llevándome nuevamente a otros pasillos, algunos con luz diáfana, otros tenebrosos como la espera de la muerte misma.

Mi laberinto no sé a ciencia cierta qué es lo que encierra, pero estoy convencido de que el centro me está esperando en algún lugar, aguardándome expectante a su único morador, a su único merodeador.

Mi laberinto se presenta cuando menos lo espero, me atacan en sueños sus sendas infinitas, me avasalla con imágenes mezcladas de pasado y situaciones que nunca sucedieron, me acongoja la soledad de sus interminables caminos, me intriga la posibilidad de estar acompañado en él de a ratos y después darme cuenta que nunca lo estuve.

 
