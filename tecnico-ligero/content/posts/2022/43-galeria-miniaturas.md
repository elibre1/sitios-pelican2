Title: Script para generar miniaturas en Python
Date: 2022-09-12 20:26
Autor: Hugovksy 
Slug: script-miniaturas-python
Lang: es
Tags: Python, fotos, miniaturas
Category: Python
Summary: Generando miniaturas de fotos con enlaces en Python
Imagen: /images/miniaturas-prueba/20220506_111808.jpg


Cuando estás obsesionado con una cosa, estás obsesionado, vieja, no hay nada que hacerle. Y yo quiero hacer de manera sencilla, como buen técnico ligero que soy, una galería de imágenes con Python y miniaturas sin tener que andar volviéndome loco convirtiendo una por una en el GIMP. Entocnes fue que di con un script en portugués que reacondicioné a mi gusto, eso es lo que tienen de bueno los pequeños scripts que me encanta, se pueden reacondicionar fácilmente.

<a href="/images/miniaturas-prueba/20220507_113311.jpg"><img src="/images/miniaturas-prueba/20220507_113311_thumb.jpg" class="img-fluid" /></a><br>


<a href="/images/miniaturas-prueba/20220506_111808.jpg"><img src="/images/miniaturas-prueba/20220506_111808_thumb.jpg" class="img-fluid" /></a><br>


<a href="/images/miniaturas-prueba/20220503_140800.jpg"><img src="/images/miniaturas-prueba/20220503_140800_thumb.jpg" class="img-fluid" /></a><br>


<a href="/images/miniaturas-prueba/20220503_100557.jpg"><img src="/images/miniaturas-prueba/20220503_100557_thumb.jpg" class="img-fluid" /></a><br>

**El código para generarlo**

```

import os
from PIL import Image

cwd = os.getcwd()
print (cwd)
for archivo in os.listdir(cwd):
    if archivo.endswith('.jpg'):
        print (archivo)
        im = Image.open(archivo)
        im.thumbnail((500,375), Image.ANTIALIAS)
        nom = archivo.split ('.')[0]
        tnom = nom + "_thumb.jpg"
        im.save(tnom, "JPEG")
        print ('<a href="%s"><img src= "%s"/></a>
        <br>' % (archivo, tnom))

```

fuente:

<a href="https://silveiraneto.net/2007/11/11/criando-uma-galeria-de-imagens-com-python/" target="_blank">Galería de imágenes</a>


