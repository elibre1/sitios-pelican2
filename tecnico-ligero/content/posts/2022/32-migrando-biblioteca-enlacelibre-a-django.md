Title: Desplegando la Biblioteca EnlaceLibre hecha en Django... o por lo menos estoy viendo qué ondas
Date: 2022-07-17 12:21
Author: Hugovksy
Category: Django
Tags: Django
Slug: deplegando-biblioteca-enlacelibre-django
Status: published
Imagen: /images/django.jpg


*** Nota de último momento: *** En <a href="instalando-django-desde-cero.html" target="_blank">esta enlace</a> arranco una breve intro para instalar Django en tu equipo.


![logo Django](/images/django.jpg){:.img-fluid}

En la UNL donde me encuentro estudiando al tecnicatura en Software Libre me topé con grata sorpresa, en la materia programación 2, con este archiconocido marco de trabajo de Python, Django. Me dije, vamos a darle una oportunidad, ya que lo poco que había hecho a nivel programación web estaba del lado de PHP y Laravel como marco de trabajo. Entonces me dije, qué tal si asumo el reto saljave de migrar la biblioteca a Django para probar el rendimiento de ambas en la Raspberry.

En eso estoy...


===DESPLIEGUE===

Tengo un listado de los pasos que di para desplegar, que considero es la parte más difícil de la vida...

1) Clonamos el repo de gitlab o donde se encuentre:

```
  git clone https://gitlab.com/enlacepilar/enlace-libre-django.git

```

2) Lo que hago a continuación es crearme una rama que se llama "despliegue" para hacer todas las modificaciones que tengan que ver con la puesta en producción

```
  git checkout -b despliegue

```

Creo la rama con ese comando y salto directamente a ella.

3) Ahora, si en nuestro proyecto guardamos el archivo de texto con los complementos instalados de Python como Django, es más sencillo. Vamos a repasar los pasos, que he publicado en otro lado, por ahí en este blog.

  * Instalar Venv para entornos virtuales desde la consola:
    
```
  sudo apt install python3-venv

```
  
  * Crear la carpeta con el entorno virtual
    
```
  python3 -m venv envBiblio

```

  * Activar el entorno virtual
  
```
  source tutorial/bin/activate
    
```

Ahora sí una vez activado el entorno aislado, podemos probar traer todo los complementos de Python que instalamos en nuestro entorno de desarrollo:

```
  pip install -r requirements.txt

```
4) Después de esto instalamos gunicorn, que sí sirve solo en el proyecto de producción.  

```
  pip install gunicorn

```

5) Crear la carpeta en /var/log/ ----\> gunicorn_proyecto/  

```
  sudo mkdir gunicorn_proyecto

```

6) ajustar el propietario en base a lo del punto 2  

```
  sudo chown -R propietario:www-data gunicorn_proyecto/

```


7) Crear un servicio en /etc/systemd/system/enlacelibre.service  
pegar esto:

```
[Unit]  
Description=gunicorn daemon  
After=network.target

[Service]  
User=enlacepilar  
Group=www-data  
WorkingDirectory=/var/www/html/enlace-libre-django/principal  
ExecStart=/var/www/html/enlace-libre-django/envBiblio/bin/gunicorn --access-logfile - --workers 3 --bind unix:/var/log/gunicorn_enlace/principal.sock principal.wsgi:application

[Install]  
WantedBy=multi-user.target

```

8) Luego podemos probar si anda  

```
sudo systemctl start enlacelibre

```

9) Y habilitarlo  

```
sudo systemctl enable enlacelibre

```

Si por alún motivo desconocido da error y no aparece "active", podemos verificar con este comando:  

```
sudo journalctl -u enlacelibre

```

Corregimos y recargamos todo  

```
sudo systemctl daemon-reload  
sudo systemctl restart enlacelibre

```

10) Verificar si esta en producción  

```
python manage.py check --deploy  

```

Esto advierte si hay algún error para corregir.

11) Modificamos el archivo settings.py para separar los archivos estáticos y todo lo media


```
STATIC_URL = '/pagina_inicio/static/'  
STATIC_ROOT = '/var/www/html/static_biblio'

MEDIA_URL = '/media/'  
MEDIA_ROOT = '/var/www/html/media_biblio'

```

12) Ahora hay que pasar todo lo estático a esta carpeta (antes claro crearlas en la ruta deseada):  

```
mkdir /var/www/html/static_biblio  
mkdir /var/www/html/media_biblio

python manage.py collectstatic  
(copiando todo...)

```

10) Crear el archivo de nginx en /etc/nginx/sites-available/biblio:  

```
sudo nvim /etc/nginx/sites-available/biblio

```

```
server {  
listen 80;  
server_name biblioteca-enlacelibre.duckdns.org biblioteca-enlacelibre.enlacepilar.com.ar;

location = /favicon.ico { access_log off; log_not_found off; }  
location /pagina_inicio/static {  
autoindex on;  
alias /var/www/html/static_biblio;  
}

location /media {  
autoindex on;  
alias /var/www/html/media_biblio;  
}

location / {  
include proxy_params;  
proxy_pass http://unix:/var/log/gunicorn_enlace/principal.sock;  
}

```

11) Crear el en enlace simbolico para activarlo  

```
sudo ln -s /etc/nginx/sites-available/biblio /etc/nginx/sites-enabled/biblio

```

12) Reseteemos toditito: demonios, servicio creado y nginx.

```
sudo systemctl daemon-reload
sudo systemctl restart enlacelibre
sudo service nginx reload

```


Se puede ver cómo va evolucionando acá:

 <a href="https://biblioteca-enlacelibre.enlacepilar.com.ar/" target="_blank">Biblioteca EnlaceLibre</a>


 # Bono: Generar la clave secreta que te pide Django en el archivo settings.py:

 ## Generador de SECRET_KEY

En consola, escirbir:

```
  openssl rand -base64 32

```

 

 
