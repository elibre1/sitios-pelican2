Title: Estoy trabajando en un generador de bibliotecas autogestionadas experimentales y anti obsolescentes
Date: 2022-09-04 20:32
Autor: Hugovksy 
Slug: generador-de-bibliotecas
Lang: es
Tags: generador de bibliotecas, Python
Category: Proyectos
Summary: Estoy trabajando en mi proyecto final para la UNL
Imagen: /images/generador-bibliotecas.jpg


![Generador de bibliotecas](/images/generador-bibliotecas.jpg){:.img-fluid}


Estuve pensando para mi proyecto final la <a href="https://biblioteca-enlacelibre.enlacepilar.com.ar/" target="_blank">biblioteca EnlaceLibre</a>, que está hecha en Django (Python) y traspasada desde Laravel (PHP) porque un buen día me agarró la loca y quise probar el rendimiento de ambos marcos de trabajo en mi Raspberry y francamente no llegué a ningún resultado. El tema es que con este fierrito que me agarró de luchar por las bajas tecnologías me miré al espejo y me dije: "desnudo para siempreeee". Ah no, eso no, eso es de La Renga. Me dije, vamos a hacer algo a nivel más bajo y que quien quiera poner su biblioteca de libros para que otro pueda descargarlo, como yo por ejemplo, que me encanta revolver bibliotecas autogestionadas, pueda hacerlo.
Es decir, una página sencilla que te genera un HTML con el listado de una carpeta que tengas llena de libracos malditos que hablen sobre temas que no puedan salir a la luz como algún script maligno para copiar mentes, etc...

Bueno, acá voy empezando y tengo una interfaz pedorra visual que te permite leer una ayuda y generar la carpeta. Falta camino por recorrer pero quien me quita lo programado...


```
from tkinter import *

caja = Tk()

caja.title("Instalador de libros Guerrilla")
caja.geometry('400x400')


texto1 = Label(caja, text="Instalador de la biblioteca guerrilla personal")
texto1.grid()



def generador_de_libros():
    texto1.configure(text="Hiciste click en la biblio guerrilla")
    import os
    directorio = "libros"
    datos =""

    for ruta, carpeta, archivos in os.walk(directorio):
        for archivo in archivos:
            datos += "<a href='"+directorio+"/"+archivo+"' target='_blank'>"+archivo+"</a><br><br>"


    inicio = open ('inicio.html', 'w')

    inicio.write (""" <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Libros para compartir</title>
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
      </head>
      <body class='p-5 bg-warning'>
        <h1 align='center'>Mis libros para compartir</h1>
        """+datos+"""

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    </body>
    </html>""")

    inicio.close()

def ayuda():
    archivo_de_texto = open ('ayuda.txt','r')
    mensaje = archivo_de_texto.read()
    
    ventana_ayuda = Toplevel() #Crea la caja donde va el texto
    ventana_ayuda.geometry('700x700')
    inserta = Label(ventana_ayuda,text=mensaje)
    inserta.pack()

boton_html= Button(caja, text="Genera HTML de tus libros",
               fg="red", command=generador_de_libros)
boton_html.grid(column=0, row=4)

boton_ayuda =Button(caja, text="Ayuda",
                    fg ="green", command=ayuda)
boton_ayuda.grid(column=0, row=9)

caja.mainloop()

```
    
  
