Title: La Resistencia Anti Obsolescencia
Date: 2022-07-25 15:24
Autor: Hugovksy
Slug: la-resistencia
lang: es
tag: resistencia
Category: Escribo

Bueno aquí estoy en mi búnker virtual de resistencia anti obsolescencia programada, usando una PC de hace más de 10 años, con Huayra GNU/Linux y publicando en un blog/sitio-web estático hecho con Pelican, en una Raspberry Pi 4. No hay que dejarse seducir por los maníaticos adictos al consumo de hardware nuevo, no hay que dejarse llevar por los sentidos de los sistemas privativos que prometen mejores compatibilidades, mejores juegos y mejores rendimientos en la vida, y mejorar como persona y ser buenos ciudadanos. En la Resistencia usamos Gnu/Linux, no Linux a secas porque sabemos que eso es solo el kernel y además leemos a Milan Kundera. En la Resistencia además de ser fanáticos de la literatura, del Software Libre y de la música que pasan por las radios que tienen como S.O. Etertics, una distro para radios libres, a veces, cuando necesitamos inspiración ponemos en New Pipe el gol de Maradona a los ingleses, una pincelada de arte comparable con alguna creación de Mozart. 

Los invito a que de sus lugares se animen a resistir. Que prescindan de a poco de Google (Alphabet), Facebook e Instagram (Meta), de Amazon que todo lo devora y demás... Que en sus celulares instalen F-Droid para gestionar sus propias aplicaciones. Que no vayan comprando por la vida artefactos nuevos, denle una segunda oportunidad a esos que la gente tira o que no quiere usar más. Hay una distro que está esperándote.  

Sin ponerme demasiado serio, claro está, y dejando la parafernalia de más arriba, me recontra copé con un par de artículos de cybercirujas, acá se puede leer lo que escribe Soldán, que lo hace mucho mejor que yo y en ese grupo tienen las cosas más en claro, pero me siento identificado con revivir pcs viejas y darle duro a la autogestión de mis sitios.

<a href="https://cybercirujas.sutty.nl/" target="_blank">Cybercirujas</a>
