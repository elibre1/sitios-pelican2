Title: Instalando Django desde cero en un entorno aislado
Date: 2022-09-06 19:00
Autor: Hugovksy 
Slug: instalando-django-desde-cero
Lang: es
Tags: Django, Python
Category: Django
Summary: Vamos a instalar Django
Imagen: /images/django.jpg


![logo Django:w](/images/django.jpg){:.img-fluid}


En <a href="deplegando-biblioteca-enlacelibre-django.html" target="_blank">esta publicación</a> realizo los pasos minuciosos para desplegar Django, después de haberme roto la cabeza un buen rato, días, eones digamos. 
Pero todo tiene un principio y es que si vamos a crear la aplicación de cero, también tiene que haber una guía para el buen técnico ligero, y es esta breve introducción: 


1) Instalar Venv para entornos virtuales desde la consola:

```
  sudo apt install python3-venv

```

2)Crear la carpeta con el entorno virtual

```
  python3 -m venv tutorial

```

3)Activar el entorno virtual

```
  source tutorial/bin/activate

```

4) Installar django

```
  python -m pip install django

```

5)Generar la carpeta de Django

```
  django-admin startproject principal

```

6) Ingresar en la carpeta blog e inicializar la aplicacion poll

```
  python manage.py startapp pagina_principal

```


# Importar y exportar

1) Exportar las dependencias instaladas

```
  pip freeze > requirements.txt

```

2) Para importar si existen las dependencias en otro lado, otro equipo, etc

```
  pip install -r requirements.txt

```

