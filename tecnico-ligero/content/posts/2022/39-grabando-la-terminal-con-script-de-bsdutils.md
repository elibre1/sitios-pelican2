Title: Grabando la terminal con script de bsdutils
Date: 2022-08-05 18:03
Author: Hugovksy
Category: Terminal
Tags: bsdutils, gnu-linux, script, terminal
Slug: grabando-la-terminal-con-script-de-bsdutils
Status: published
Summary: Un verdadero técnico ligero que se respeta debe saber usar la terminal como un verdadero campeón.


Como digo siempre, un verdadero técnico ligero que se respeta debe saber usar la terminal como un verdadero campeón. En este caso si queremos grabarla, ¿con qué miércoles? He aquí que sale en nuestra ayuda bsdutils con la herramienta script. El proceso es muy simple, vean:

1) Instalamos en Debian y derivados:

```
    sudo apt-get install bsdutils

```

2) Abrimos la terminal y asignamos 2 archivos, uno de tiempo y otro de grabación. En este caso yo lo puse en la raiz de DATOS, un disco esclavo que tengo. Vos ponele la ruta que quieras :)

```
    script -t -a 2\> /media/DATOS/tiempo.txt /media/DATOS/graba.txt

```

3) Para salir simplemente tipeamos ***"exit"***.

4) Para reproducir, tipeamos lo siguiente:

```
    scriptreplay /media/DATOS/tiempo.txt /media/DATOS/graba.txt

```

fuente:

[maketecheasier](https://www.maketecheasier.com/record-terminal-session-in-ubuntu/)
