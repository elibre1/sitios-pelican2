Title: Mensaje Público
Date: 2022-07-25 15:08
Autor: Hugovksy 
Slug: mensaje-publico
Lang: es
Tags: cuento
Category: Escribo
Summary: El otro día me iba del trabajo y una compañera ya entrada en años...

Soy fan del relato corto, Por lo menos para mí, esto es, cuando escribo quise decir. Hacer algo extenso o prolongarlo se me hace tedioso hasta en la mente. Ya sé, ya sé que las cosas se hacen paso a paso, pero resulta que con esta nueva actualización de Mensajer_app te pueden agregar a un grupo y conectarte a una conferencia por defecto, sin habértelo consultado. Y me tiene francamente preocupado...

El otro día me iba del trabajo y una compañera ya entrada en años y no muy ducha en temas informáticos me dice: "A ver Hugo, te agrego al grupo, que no estás". Como no me importó demasiado, quizá porque no era una consulta, era casi un llamamiento de culpa, dije meh. Al rato empezó a sonar mi celular a lo loco, plim, plim, plim. Mensajes, mensajes. Me dije, bueno, lo pongo en silencio y ya. Después me olvidé un poco de eso y seguí el día. A la tarde me fui a mi otro tabajo de soporte técnico en un taller. No estoy en esta vida para andar justificándome ante la gente pero no te puedo negar que el dinero extra me viene bien, sobre todo para afrontar algunas cosas pendientes. El tema es la gente ahí, es un poco... áspera digamos. Especialmente una mujer que tiene un carácter que ni te cuento. Y en particular hoy se juntaron los planetas, porque ella tenía un día de perros y yo no estaba de humor como para andar conciliando, venía enroscado por otra cosa. Y así fue como sucedió que una mala contestación me llevó lisa y llanamente a mandarla al recarajo sin antestesia.
Al minuto me llega un audio al celu. ¡Hugo! ¡Qué carácter! A quién puteabas así. Un compañero del otro trabajo. Parece que la actualización de este programa de mensajería te deja el micrófono abierto de una y conectado a un servidor. Todos los del grupo podían oírme. Apagué el celular sin pensarlo. 
