Title: Sigo puliendo el script de la galería
Date: 2022-09-21 20:22
Autor: Hugovksy 
Slug: script-miniaturas-python-para-pelican
Lang: es
Tags: Python, fotos, miniaturas, Pelican
Category: Python
Summary: Generando miniaturas de fotos con enlaces en Python para Pelican
Imagen: /images/logo1.jpg


Sigo puliendo mi script para generar galerías de fotos. En esta oportunidad lo estoy acomodando para que no me lleve demasiado trabajo publicar con Pelican, el generador de sitios estáticos de Python con el que vengo trabajando y que claramente me tiene enmanijado jajaja. Para ello hay que seguir uns pequeños, pequeñísimos pasos iniciales:

* Seleccionar las fotos de tu preferencia
* Copiarlas a la carpeta "content/images" del generador de tu sitio con el nombre del mes, ej **septiembre2022**
* Copiar este script en el raiz del sitio, donde están las carpetas **content** y **output**, junto a los archivos de configuración
* Ejecutar el script
* Cuando pregunte el mes, asisgnarle el nombre de la carpeta
* El número que te pide es para mantener el orden de las publicaciones.

```

import os
from PIL import Image
import datetime

fecha_publicacion = datetime.date.today()
fecha_publicacion = fecha_publicacion.strftime('%Y-%m-%d %H:%M')


def escribe_archivo (datos, nro_public, mes, dia_public):
    archivo = open ('content/'+nro_public+'-'+mes+'.md', 'w')
    archivo.write("""Title: Galería de """+mes+"""
Date: """+dia_public+"""
Autor: Hugovksy 
Slug: """+mes+"""
Lang: es 
Tags: """+mes+""" 
Category: """+mes+"""
Summary: Galería de """+mes+""" 

\n
\n

"""+datos+"""

    """)
    archivo.close()


#obtengo el directorio actual
cwd = os.getcwd()


directorio = "images"
mes = input ("¿Qué mes vas a publicar (igual nombre de carpeta): ")
nro_publicacion = input("nro de publicacion: ")
nro_publicacion = str(nro_publicacion)

lee_desde = cwd+"/content/images/"+mes
print (lee_desde)

fotos = ''
fotos += '<table style="border: 1px solid black;"><tr>'
n = 1

for archivo in os.listdir(lee_desde):
    if archivo.endswith('.jpg'):
        #print (archivo)
        if (n < 4):
            im = Image.open(lee_desde+'/'+archivo)
            im.thumbnail((200,175), Image.ANTIALIAS)
            nom = archivo.split ('.')[0]
            tnom = "zz-mini_ "+ nom + ".jpg"
            im.save(lee_desde+"/"+tnom, "JPEG")
            fotos+= '<td style ="border: 1px solid black; border-radius: 10px;"><a href="%s/%s/%s" target="_blank"><img src="%s/%s/%s"></a></td>' % (directorio, mes, archivo, directorio, mes, tnom)
            n +=1
            print (archivo)
        else:
            im = Image.open(lee_desde+'/'+archivo)
            im.thumbnail((200,175), Image.ANTIALIAS)
            nom = archivo.split ('.')[0]
            tnom = "zz-mini_ "+ nom + ".jpg"
            im.save(lee_desde+"/"+tnom, "JPEG")
            fotos +='<td style ="border: 1px solid black; border-radius: 10px;"><a href="%s/%s/%s" target="_blank"><img src="%s/%s/%s"></a></td></tr>' % (directorio, mes, archivo, directorio, mes, tnom)
            n = 1
            print (archivo)

fotos+= ('</tr></table>')
                    
escribe_archivo (fotos, nro_publicacion, mes, fecha_publicacion)



```

Cabe aclarar que donde dice "Hugovsky" le tenés que poner tu nombre. Y lo demás claramante se puede modificar a gusto.

<a href="images/documentos/genera-miniaturas-con-TABLA.py" target="_blank">Descargá el script de Python</a>

Fuente o mejor dicho, me basé en:

<a href="https://silveiraneto.net/2007/11/11/criando-uma-galeria-de-imagens-com-python/" target="_blank">Galería de imágenes</a>


