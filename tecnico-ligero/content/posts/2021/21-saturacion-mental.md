Title: Saturación mental cuando le das mucho tiempo a la pantalla
Date: 2021-09-09 20:30
Author: Hugovksy
Category: Cosas
Tags: Quemado
Slug: saturacion-mental
Status: published
Imagen: /images/quemado.jpg

Esta vez no vengo a hablar sobre cuestiones a resolver técnicas, sino más bien humanas, que bueno, sirve un poco para posteriormente resolver mejor las cosas técnicas. Me pasa que a veces estoy pegándole varias horas al código y de repente me doy cuenta que no puedo solucionar ni una coma. Me digo qué me pasa, no puede ser que no encuentre la solución a esta cosa, y por más que le de y le de no hay caso. Ese es el momento, o mejor dicho un poco antes, de cortar, loco. Digo cortar y ponerte a meditar o ir a dar una vuelta en bici y dejarlo. Me pasa que a la mañana estoy en el laburo y en mis ratos libres le doy un poco a alguna aplicación y después a la tarde empiezo a la 5 y son las 9 de la noche y sigo. ¡¡Es demasiado, vieja!! Hay que aprender a cortar. Y si se puede, alguno días ni tocar nada a la tarde si es que viste algo a la mañana y otro nada de nada y ver una peli. Cuando uno regresa vuelve con otro ímpetu y con mejores soluciones.

!![Quemado](/images/quemado.jpg){:.img-fluid}
