Title: Los pasos para tener un Neovim decente y ser un verdadero técnico ligero que se respeta
Date: 2021-12-07 23:16
Author: Hugovksy
Category: Neovim
Tags: neovim, Ubuntu server
Slug: neovim-decente
Status: published

Estos pasos fueron probados y testeados en ubuntu server, (sólo consola obviamente). Nada de entornos gráficos, eso es para principiantes, debés decirte y sacar pecho. Nada de Vscodium, eso ha quedado atrás, de ahora en más voy a programar mi sitio íntegramente en la terminal, debes decirte también y volver a sacar pecho, esta vez con un leve golpe como diciendo: acá estoy yo. Bueno, sin más preámbulos, ahí va:

1) Instalar neovim  
sudo apt install neovim

2) Crear en /home/usuario una carpeta .config  
adentro crear otra carpeta nvim

3) Ejecutar  

```
curl -fLo \~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

```

4) Crear archivo adentro init.vim

5) Pegar lo siguiente

```

"Directorio de plugins  
call plug\#begin('\~/.local/share/nvim/plugged')

" Aquí irán los plugins a instalar

Plug 'tpope/vim-surround' " Es buena idea agregar una descripción del plugin

Plug 'morhetz/gruvbox'

"IDE  
Plug 'easymotion/vim-easymotion'

""Nerdtree  
Plug 'scrooloose/nerdtree'

"Para poder desplazarme entre archivos abiertos  
Plug 'christoomey/vim-tmux-navigator'

call plug\#end()  
"  
"Luego de esta línea puedes agregar tus configuraciones y mappings  
"  
set number  
set mouse=a  
set numberwidth=1  
set clipboard=unnamed  
syntax enable  
set showcmd  
set ruler  
set encoding=utf-8  
set showmatch  
set sw=2  
set relativenumber  
set laststatus=2

colorscheme gruvbox  
let g:gruvbox\_contrast\_dark = 'hard'  
let NERDTreeQuitOnOpen=1  
let mapleader= " "  
nmap s (easymotion-s2)  
nmap nt :NERDTreeFind

"Atajos de teclado  
nmap w :w  
nmap q :q  
"  
"

```

Despues entrar en neovim (nvim) y teclear:

```
:PlugInstall

```

De este modo se van a instalar los plugins que hemos copiado en el archivo creado con el texto de arriba.


<a href="https://stsewd.dev/es/posts/neovim-plugins/" target="_blank">Fuente</a>

