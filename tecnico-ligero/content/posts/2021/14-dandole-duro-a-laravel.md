Title: Dándole duro a Laravel (o el camino que me llevó hasta ahí)
Date: 2021-08-02 10:36
Author: Hugovksy
Tags: Laravel, PHP
Slug: dandole-duro-a-laravel
Status: published
Category: Laravel
Summary: Hace un tiempo hice una aplicación de formularios para el laburo...

Hace un tiempo hice una aplicación de formularios para el laburo (tomé la posta casi sin quererlo), la cual empecé tímidamente como scripts de Python, después descubrí que existían unas cosas que se llamaban marcos de trabajo o "frameworks" en su anglicismo y me dije, ey por qué mierda no llevarlo a la web. Y así fue. El más reconocido es Django pero la curva era muy pronunciada y un buen técnico ligero que quiere aprender busca primero lo más sencillo. Empecé con un minimarco llamado Bottle (demasiado sencillo) y finalmente me pasé a Flask. Así que ahí empecé a ver qué onda en ese mundillo. ¡Mira mamá, soy programador! Después de romperme los cocos unos meses, había que desplegar este monstruito pequeñito. Ahí es donde empezaron los problemas, porque mis conocimientos de Apache o Nginx erar rudimentarios y bueno, nada que no se puede buscar en internet. Terminé concluyendo que lo mejor era volver a las raíces, si es web, usá PHP macho. Como había hecho un curso de Pildoras informáticas (sí me clavé los 98 videos), me dije: por qué no migrar toda esta mierda de Flask que dicho sea de paso, había encontrado un "servidor"de estas páginas que se llama Waitress que sirve para windows, donde estaba desplegando (en el laburo usan Windows, me resisto). Y así fue que un mes y pico pasé a los golpes la aplicación de formularios de Flask a Php puro con Mysql y consultas a Sql server.

Hasta ahí todo muy lindo. Aprendí un poco cómo desplegar con Apache, que no requería mucho para lo que necesitaba... pero los problemas empezaron cuando quería mantener esa aplicación. Si estás todos los días todo bien, te acordás, porque sumale a eso que le meté javascript con Fetch primero y Async-Await después... Inmantenible sería la palabra. Archivos por todos lados, no me acordaba qué había hecho, me pasaba (me paso) horas tratando de entender qué carajos era lo que había programado o copiado y pegado, jeje. En fin, tenía que encontrar otra solución. Fue así que me animé en varios tramos de meses a aprender Laravel. Me costó un huevo comprender donde estaban las cosas, nada que ver con Flask, pero después de unos meses logré esbozar mis primeras cosillas. Así que, ¿por qué no pasar la mini aplicación del laburo de nuevo a Laravel para probar? Pero esta vez a todo trapo, con Gates para que se loguee el admin y vea otras cosas, adminLte para el panel y darle un poco más de profesionalismo. Y así voy. Todavía no la tengo completamente terminada pero puedo resumir lo que hice y aprendí:

-Antes hacía las consultas en caliente en Sql server, ahora hago copias por las noches a una base Mysql de lo que necesito con Crontab y PHP para poder realizar consultas locales en Laravel

-En Laravel aprendí a adaptarme a las miles de carpetas desconocidas del framework, a crear controladores y modelos por consola, instalar complementos, cosa que antes resistía y me parecía un poco invasivo de mi trabajo (celoso de sus archivos el tipo).

-Migré todo a Ubuntu server virtualizado, lo que me hizo prescindir (en parte) de Windows

-Todavía no está desplegado al 100x100, pero volver después de un tiempo a tocar el código es más sencillo, sé donde están las cosas después de haberlas memorizado, claro, para eso están los marcos de trabajo.

-Aprendí a asegurar el apache de posibles ataques y a configurar las rutas de acceso

-Aprendí a desplegar Laravel en un servidor, cosa que no tenía ni idea. Ahora sé cómo hacer que apunte a public y que puedan estar otros sitios conviviendo en el mismo ubuntu.

El resumen nivel 5 de esto es mostrar el camino recorrido dependiendo de las necesidades. Eso no implica que volvería a Flask porque me resolvió muchas cosas que necesitaba en su momento. A veces temo olvidarme todo, después me acuerdo que me pasa siempre y se me pasa buscando en internet.
