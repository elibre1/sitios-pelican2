Title: Conectar a MSSQL con PHP desde Linux
Date: 2021-09-18 19:41
Author: Hugovksy
Category: PHP
Tags: linux, mssql, PHP, sql server
Slug: mssql-php-linux
Status: published

A veces y por alguna razón desconocida necesitamos conectarnos a Sql server de Microsoft desde una pc con alguna distro Linux, preferentemente en este ejemplo basada en Debian. En mi caso fue en el laburo desarrollando una aplicación y todo el contenido estaba generado en ese motor de bases de datos. Después de mucho rebuscar soluciones por internet pude dar con la, valga la redundancia, menos rebuscada de todas, a saber:

Primero instalamos Apache  

```
sudo apt-get install php7.4 libapache2-mod-php7.4 php7.4-common php7.4-curl php7.4-mbstring php7.4-xmlrpc php7.4-mysql php7.4-gd php7.4-xml php7.4-intl php7.4-ldap php7.4-imagick php7.4-json php7.4-cli

```

#### FINALMENTE PARA QUE ESTA MIERDECILLA CONECTE TUVE QUE INSTALAR ESTO (POR EL CONECTOR DBLIB):

```
sudo apt-get install php7.4-odbc php7.4-sybase tdsodbc

```