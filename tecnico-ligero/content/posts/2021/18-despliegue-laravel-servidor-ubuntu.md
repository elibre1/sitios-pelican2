Title: Los puntos definitivos para el despliegue de Laravel en un servidor Ubuntu
Date: 2021-08-23 20:30
Author: Hugovksy
Tags: Proyectos, Despliegue, Laravel, Raspberry
Slug: despliegue-laravel-servidor-ubuntu
Status: published
Category: Laravel
Summary: Desplegar un proyecto Laravel

A modo de ampliación de esta [publicación](desplegando-laravel-en-un-servidor-ubuntu-permisos-y-propietarios.html), los pasos completos para desplegar un proyecto Laravel en un servidor Ubuntu, suponiendo que ya tenemos configurado Apache, Mysql o MariaDB correctamente:

1) Clonar el repositorio (lo va a clonar sin la carpeta vendor ni el archivo env)

2) PERMISOS DE LA CARPETA  

* Configurar como propietario a www-data de la carpeta y asignarle el grupo  

```
sudo chown -R www-data:www-data /ruta-completa-a-tu-direcctorio-laravel

```

Ej: /var/www/html/MiProyecto/

* Incluir mi usuario en dicho grupo:  
sudo usermod -a -G www-data tu-usuario

* Asignar permisos a los archivos y las carpetas:  

** Para los archivos:  

```
sudo find /var/www/html/MiProyecto/ -type f -exec chmod 644 {} \\;

```

** Para las carpetas:  

```
sudo find /var/www/html/MiProyecto/ -type d -exec chmod 755 {} \\;

```

3) Copiar el archivo env de prueba  
4) Para la carpeta vendor, ejecutar composer install o update  
5) Regenerar tambien si tengo un link simbolico a storage:  

```
sudo php artisan storage:link

```