Title: Instalar PHP 8 en Debian y derivados.
Date: 2021-10-11 19:07
Author: Hugovksy
Category: PHP
Tags: php 8
Slug: instalar-php-8-en-debian-y-derivados
Status: published

sudo apt install php8.0 php8.0-cli php8.0-common php8.0-curl php8.0-gd php8.0-intl php8.0-mbstring php8.0-mysql php8.0-opcache php8.0-readline php8.0-xml php8.0-xsl php8.0-zip php8.0-bz2 libapache2-mod-php8.0 -y

Estaba intentando instalar phpmyadmin porque había actualizado mi versión de Ubuntu de 21.04 a 21.10 y me lo había mochado. Probé varias cosas pero lo que me anduvo fue esto que saqué de este sitio:

fuente: <https://www.bennetrichter.de/en/tutorials/apache2-php8-mariadb-phpmyadmin/>

Despues fui a la web de phpmyadmin y lo descargué y pegué en /var/www/html/ y listo. Anduvo todo ok
