Title: SweetAlert2 con Laravel Livewire: funcionando infalible
Date: 2021-08-22 19:34
Author: Hugovksy
Category: Laravel
Tags: Laravel, livewire, sweetalert2
Slug: sweetalert2-laravel-livewire
Status: published

Después de buscar como un engendro encontré la forma toda emparchada (digo emparchada porque tomé un poco de la instalación oficial y algo que vi acá: <https://laracasts.com/discuss/channels/livewire/livewire-with-sweetalert2?page=1>) de hacer funcionar SweetAlert2 con Livewire en Laravel 8, pero supongo que puede andar para otras versiones. A continuación, los pasos para que te funcione todo como un verdadero programador que se respeta debe hacer:

1\) Instalar SweetAlert2 (obviamente en la carpeta donde estás trabajando con Laravel)  
composer require realrashid/sweet-alert

2\) En el Layout o plantilla principal agregar el elemento que va a estar escuchando:

```
@include('sweetalert::alert')  
@livewireScripts

<script>

    window.addEventListener('swal',function(e){
        Swal.fire(e.detail);
    });
</script>
```

3\) En el componente, donde quiero lanzarlo con mensaje enviar:

```
$this-\>dispatchBrowserEvent('swal', \[  
'title' =\> 'Noticia creada!',  
'timer'=\>3000,  
'icon'=\>'success',  
'toast'=\>true,  
'position'=\>'top-right'  
]);
```

4\) En el .env **FUNDAMENTAL PARA QUE ANDE**, colocar en cualquier lado  
SWEET\_ALERT\_ALWAYS\_LOAD\_JS=true
