Title: Biblioteca EnlaceLibre
Date: 2021-11-29 20:29
Author: Hugovksy
Slug: biblioteca-enlacelibre
Status: published
Category: Proyectos
Summary: Actualmente me encuentro trabajando en un proyecto para la tecnicatura en Software Libre...

Actualmente me encuentro trabajando en un proyecto para la tecnicatura en Software Libre que estoy estudiando en la Universidad Nacional del Litoral. Este trabajo nace de la materia Culturas Digitales Libres y se llama como el título menciona, [Biblioteca EnlaceLibre](https://biblioteca-enlacelibre.duckdns.org/) . La idea es hacer una biblioteca de licencias libres, o sea, que los libros digitales subidos puedan ser descargados, compartidos y modificados libremente. Gracias a la tecnicatura pude encontrar proyectos bellísimos, como puede ser el de [Bibliobox](https://bibliobox.copiona.com/), una idea de biblioteca fuera de línea para descargar materias desde ferias, bibliotecas, etc. Tiene un tutorial muy completo. Lo recomiendo.

Siguiendo con EnlaceLibre, el proyecto está disponible en[GitLab](https://gitlab.com/enlacepilar/biblioteca-enlace-libre.git)para poder colaborar, o ser descargado, modificado y reutilizado. Está en pleno proceso de construcción y estoy trabajando como lo vengo haciendo desde el año pasado con el marco de trabajo Laravel (me encantó y dejé atrás Flask). Hoy por hoy podés:  
-Registrarte con tu usuario.  
-Subir tus propios PDF con tus tapas en JPG o PNG

![Foto Biblio](/images/biblioteca-enlace-libre.jpg)

Añadiendo más detalles técnicos, la tengo montada en una Raspberry Pi 4 en casa y es accesible gracias a Duckdns. Está buena la idea de llevarlo también a la versión fuera del línea, estoy lo voy a ver más adelante.
