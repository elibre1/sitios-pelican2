Title: Instalar Laravel /UI y si no se puede, bueno... Breeze
Date: 2021-10-11 12:56
Author: Hugovksy
Category: Laravel
Tags: breeze, logueo, sistema de logueo en laravel, ui
Slug: instalar-laravel-ui
Status: published

Voy a hacer una lista de pasos para instalar el más sencillo de los componentes de registro que conozco y el que más me gusta UI. También está Breeze y Jetstream pero para empezar está bien con el UI. Ahí vamos:

``` 
1) composer require laravel/ui
2) Elegir boostrap como herramienta:
php artisan ui bootstrap --auth
3) Ejecutar npm install y acto seguido npm run dev 

Si las cosas se complican como me pasó a mí con PHP 8, entonces sí, la solución es decantarte por Breeze

composer require laravel/breeze --dev

php artisan breeze:install

npm install
npm run dev
php artisan migrate

```
