Title: Configurar IP fija en un Ubuntu server, inclusive con wifi
Date: 2021-07-21 20:24
Author: admin_ligero
Category: Python
Slug: ip-fija-ubuntu
Status: published
Imagen: /images/logo1.jpg

Estos son los pasos resumidos para lograr el éxito sin precedentes usando netplan. Nunca más fáciles en el mundo mundial. El secreto son los espacios bien delimitados, ¡cuidado! Abajo pongo una imagen como debería quedarte.

1) cd /etc/netplan

2) editar el archivo .yaml

```
    network:  
    version: 2  
    ethernets:  
    eth0:  
    dhcp4: no  
    addresses: \[192.168.0.20/24\]  
    gateway4: 192.168.0.1  
    nameservers:  
    addresses: \[192.168.0.1\]  
    dhcp6: no

    wifis:  
    wlan0:  
    dhcp4: no  
    addresses: \[192.168.0.25/24\]  
    gateway4: 192.168.0.1  
    nameservers:  
    addresses: \[192.168.0.1\]  
    dhcp6: no  
    access-points:  
    "MiWIFI":  
    password: "MICLAVE"

```

3) Confirmar y apliicar los cambios  
sudo netplan apply

4)Gozar

imagen de muestra:

![Configura-red](/images/configura-red.jpg){:.img-fluid}
