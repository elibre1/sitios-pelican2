Title: Desplegando Laravel en un servidor Ubuntu. Permisos y propietarios
Date: 2021-08-23 17:34
Author: Hugovksy
Tags: Proyectos, Laravel, Despliegue
Slug: despliegue-laravel-permisos
Status: published
Category: Laravel
Summary: Desplegar a manopla un proyecto Laravel - PERMISOS...

Este es el resumen ligero para que pares de sufrir. Con estas claves, desplegar a manopla un proyecto Laravel puede llegar a ser de lo más placentero. A gozarla:

Después de clonar el repositorio con git clone tu'repo.

#### Propietarios y permisos proyecto laravel

1) Configurar como propietario a www-data de la carpeta y asignarle el grupo  

```
    sudo chown -R www-data:www-data /ruta-completa-a-tu-direcctorio-laravel**

```

```
    Ej: /var/www/html/MiProyecto/

```

2) Incluir mi usuario en dicho grupo: 

``` 
    sudo usermod -a -G www-data tu-usuario

```

3) Asignar permisos a los archivos y las carpetas:  
-Para los archivos:

```
    sudo find /var/www/html/MiProyecto/ -type f -exec chmod 644 {} \\;

```

-Para las carpetas:  

```
    sudo find /var/www/html/MiProyecto/ -type d -exec chmod 755 {} \\;

```