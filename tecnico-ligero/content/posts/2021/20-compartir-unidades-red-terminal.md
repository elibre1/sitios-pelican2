Title: Compartir unidades entre 2 Ubuntu server - todo por terminal
Date: 2021-08-30 10:01
Author: Hugovksy
Tags: backups, copias de seguridad, Ubuntu server
Slug: compartir-unidades-red-terminal
Status: published
Category: Copias de Seguridad
Summary: Instalar una unidad de red de un servidor Ubuntu en otro servidor Ubuntu

Con estos pasos vas a poder instalar una unidad de red de un servidor Ubuntu en otro servidor Ubuntu. Si llegaste hasta acá es porque querés hacer estas engendradas como yo, a saber:

-Hacer backups que tenes en un servidor chico a otro más grande pero automáticamente con crontab  
-Copiarte los datos como un verdadero técnico ligero, todo a pura terminal.  
-Simplemente ver si se puede hacer...  
Y muchas otras ideas más que seguramente existen pero que no me vienen a la mente en este momento...  
Bueno, allá vamos con la idea, paso a paso:

1) Instalar samba en el servidor, crear usuario y clave y compartir  

```
    mkdir backup-PC1  
    sudo chmod -R 777 backup-PC1/  
    sudo adduser Tu_Usuario  
    sudo smbpasswd -a Tu_Usuario

```

2) editar sudo nvim /etc/samba/smb.conf  

```
    [backup-PC1]  
    comment = Archivos comprimidos para llevar a PC2  
    path = /home/Tu_Usuario/backup-PC1  
    read only = no  
    browsable = yes

```

***En en el servidor "cliente"***  

3) En la Pc cliente instalar estos 3 paquetes para comunicarse con Samba  

```
    sudo apt install samba-common  
    sudo apt install samba-client  
    sudo apt install cifs-utils

```

4) Listar las unidades compartidas *(con esto vemos si efectivamente existe la unidad compartida)*  

```
    smbclient -L 192.168.0.101 -U tu\_Usuario\_Samba

```

5) Crear la carpeta donde va a montarse la unidad  

```
sudo mkdir /media/backup-de-la-PC1

```

6) Montar unidad  

```
sudo mount -t cifs -o username=***tu_Usuario_Samba***,password=***tu_clave*** //192.168.0.101/backup-PC1 /media/backup-de-la-PC1

```