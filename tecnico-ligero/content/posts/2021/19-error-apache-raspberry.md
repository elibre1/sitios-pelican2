Title: Este error de Apache me colgaba el servidor a cada rato [core:notice] [pid 2099] AH00051: child pid 23918 exit signal Segmentation fault (11), possible coredump in /etc/apache2
Date: 2021-08-23 21:27
Author: Hugovksy
Tags: Apache, GNU-Linux
Slug: error-apache-raspberry
Status: published
Category: Apache
Summary: Me estaba volviendo loco, hacía cualquier cosa y ¡pum! servidor no encontrado...

Me estaba volviendo loco, hacía cualquier cosa y ¡pum! servidor no encontrado. Cabe aclarar que tengo montada esta web en una ***Raspberry Pi 4 de 4gb de ram***. Modesto servidor pero se la banca. El tema es que me puse a buscar en internet, en castellano e inglés, ya me valía lo que sea porque estaba repodrido, y encontré una boludez que decía que modificara el **PHP. ini.** ¡Qué va a funcionar! me dije. Pero sin embargo, no sólo ha mejorado la velocidad de respuesta si no que hasta ahora, cagándola a palos meta carga, no se colgó.

Voy a tratar de repetir los pasos que hice:

1.  Obviamente, primero me conecto por ssh a la consola (los machos que se respetan sólo usamos línea de comandos)
2.  cd /var/log
3.  Ahí es necesario subir los niveles del usuario: sudo su
4.  ya con super usuario, cd apache2
5.  nvim error.log ---- ahí está el detalle de los errores esos, a cada rato.
6.  Estos pasos sólo a modo de comprobación.
7.  Después me fui a /etc/php/7.4/apache2
8.  Ahí sudo nvim php.ini (síii, los machos que se respetan usamos nvim)
9.  Hay que cambiar:  
   max\_input\_time = -1 (estaba por defecto en 60)  
   memory\_limit = 384M (estaba en 128M)  
10. Guardamos y acto seguido: sudo service apache2 reload.

Listo, eso es todo. ¡A disfrutar de un apache (por ahora) sin cuelgues!
