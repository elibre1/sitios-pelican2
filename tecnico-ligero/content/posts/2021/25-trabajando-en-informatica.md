Title: Trabajar en informática no es la mesa de ping pong o los sillones cómodos de la empresa para echar una siesta. Las mentiras y la aceleración en informática. Adónde hay que llegar tan apurado...
Date: 2021-10-26 17:51
Author: Hugovksy
Category: Opiniones
Tags: burnout, opino, personal, Quemado, sindrome del quemado informático, trabajar en informática
Slug: trabajando-en-informatica
Status: published

Me esmeré en el título, a lo Pink Floyd, salvando las distancias, jaja. Pero es el tema es más que cierto. Por estas fechas llevo 11 años trabajando en una oficina del estado del departamento de informática ¿y saben lo que vi crecer en estos años exponencialmente? (y no fue mi sueldo): la velocidad de generar innovaciones. Corto plazo. Inminente. Necesario. Último esfuerzo del año. Y estamos hablando del estado. Ni me quiero imaginar en el sector privado, donde se puede leer del síndrome de *burnout* o casos extremos donde los empleados son monitoreados por inteligencia artifiicial. Me pregunto cuál es el objetivo de todo esto, de estas presiones. En el sector privado donde el capitalismo es más despiadado puede entreverse: maximizar la ganancia. ¿Pero en el público? Cuál es la necesidad de extender la jornada a horarios interminables, no sólo para informática, cosa que esta pandemia ha potenciado... Se vencen los plazos... puras mierdas.

Es una pregunta que me hago siempre, el motivo por el cual los plazos de están acortando a medida que pasa el tiempo. A veces pienso que la gente se "inventa" plazos de manera tal que parezcan ocupados ante sus superiores. Está bien confieso que estoy en las antípodas, yo admiro a tipos como [este](https://computerhoy.com/noticias/apps/este-programador-automatiza-asi-su-trabajo-base-scripts-37481). Estamos de acuerdo que una innovación en una aplicación porque está explotada es necesaria y hay que hacerla, pero a veces viejo no hay nada que innovar, solo hinchar las pelotas por el mero hecho de simular que se hacen cosas nuevas. Eso genera una cadena de presiones que llegan a los que estamos en atención al usuario, o sea, el último eslabón de la cadena, nada agradable, y que detonan poco a poco las ganas de seguir haciendo eso...

El tema o el punto a solucionar para los que trabajamos en esto es:  
1) Aprender a controlar las presiones  
2) Huir.

El segundo punto es el que me trajo a estas líneas, es lo que quiero hacer. Pero el inconveniete es fijar un plan a corto plazo en el que uno pueda hacer lo que ama y no terminar en el clásico meme: "programo por comida".

Confieso que en estos últimos años fui migrando de entusiasta del software libre de medio tiempo a tiempo casi completo. Del arranque dual con Windows a sólo tener software libre (o casi, algunas cosas de los paquetes no son totalmente libres, si nos fijamos en la FSF). Eso acorta un poco el espectro de búsqueda. Una solución en mi caso, el desarrollo personal o emprendimiento por mi cuenta. Una empresa difícil si las hay, pasados los 40, aggiornado a una "contención" o mejor dicho, a la parte beneficiosa del trabajo, que es el sueldo depositdado todos los meses, si bien más humilde comparado con el sector privado, sueldo asegurado al fin. Y una obra social, la que sea, pero también. Entonces decía que correrse de la zona de confort es complicado, así es, pero lo principal y positivo es que he tomado la decisión de largarme. En un mes o cuando sea, pero voy para ese lado. Todavía sigo pensando el proyecto a corto plazo, pero el que avisa no traiciona...
