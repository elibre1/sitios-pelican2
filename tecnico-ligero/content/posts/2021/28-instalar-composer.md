Title: Instalar Composer en Debian y derivados, actualizado y sin morir en el intento buscando sitios que te hacen un preámbulo interminable.
Date: 2021-12-11 10:16
Author: Hugovksy
Category: Composer
Tags: composer
Slug: instalar-composer
Status: published

Francamente, los pasos son bien sencillos, pero me costó enterarme por los adornos de la mayoría de los sitios web:

1.  Descargamos el setup  
   
```
   curl -sS https://getcomposer.org/installer -o /tmp/composer-setup.php

```

2.  Obtenemos el último hash de la pag. de Composer:  
  
```
   HASH=curl -sS https://composer.github.io/installer.sig

```

3. Si querés, verificás el valor:  

```
   echo $HASH

```

   Te tiene que salir algo parecido a esto: 

```
      e0012edf3e80b6978849f5eff0d4b4e4c79ff1609dd1e613307e16318854d24ae64f26d17af3ef0bf7cfb710ca74755a

```

4.  Ahora ejecutar la siguiente sentencia PHP  
   
```
   php -r “if (hash_file(‘SHA384’, ‘/tmp/composer-setup.php’) === ‘$HASH’) { echo ‘Installer verified’; } else { echo ‘Installer corrupt’; unlink(‘composer-setup.php’); } echo PHP_EOL;”

```
   
   Tiene que salirte "Installer verified"

5.  Para instalar composer globalmente. ejecutar lo siguiente: 
   
```
   sudo php /tmp/composer-setup.php –install-dir=/usr/local/bin –filename=composer

```
   
6.  Finalmente, tipeando en la consola ***composer*** obtenemos la versión y un menú de ayuda ligero, como me gusta a mí.  

Fuente:  

<https://www.digitalocean.com/community/tutorials/how-to-install-and-use-composer-on-ubuntu-20-04>
