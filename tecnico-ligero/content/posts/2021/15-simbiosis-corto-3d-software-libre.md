Title: Simbiosis - Corto 3d hecho con software libre en 2009
Date: 2021-08-02 18:21
Author: Hugovksy
Category: Proyectos
Tags: 3d, Blender, Kden Live, LMMS, Software Libre
Slug: simbiosis-corto-3d-hecho-con-software-libre-en-2009
Status: published

Los derroteros de un técnico ligero me llevaron hace poco más de 10 años por el mundillo de la animación en tres dimensiones. Estudié una tecnicatura en gráfica y animación digital en la Universidad Nacional de Litoral con software privativo, pero que en una de sus materias animaba la utilización de software libre. Si bien conocía muy linux un poco (tuve mis experiencias allá por el 2000 con Corel Linux) nada sabía de su filosofía y esta materia me ayudó a comprender y abrir la mente. Aaaabre tu mente diría Kuato de ¨El vengador del futuro". Fue así que me lancé a hacer un corto rudimentario íntegramente hecho con herramientas libres. He aquí aquel resultado:

<div class="section d-flex justify-content-center embed-responsive embed-responsive-4by3">
  <video class="embed-responsive-item" controls autoplay loop muted>
        <source src="/images/Simbiosis.mp4" type=video/mp4>
        Tu navegador no soporta la resproducción de este video
      </video>
</div>