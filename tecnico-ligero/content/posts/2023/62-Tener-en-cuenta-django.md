Title: Cosas a tener en cuenta en Django 
Date: 2023-09-29 10:26
Autor: Hugovksy 
Slug: tener-en-cuenta-django
Lang: es
Tags: django 
Category: Django
Summary: Este es un listado que me voy a ir a anotando de cosas que sería bueno tener presente cuando uno empieza a profundizar en algunas cosas más complicadas de Django

Por ejemplo, si queremos trabajar con los templates en cada carpeta por separado, pero deseamos que la ruta se igual para todas las apps, sin importar donde te encuentres, en settings.py hacemos lo siguiente:

```
os.path.join(BASE_DIR / 'altas/templates'),
os.path.join(BASE_DIR / 'reseteos/templates'),
os.path.join(BASE_DIR / 'roles/templates'),
os.path.join(BASE_DIR / 'subrogancias/templates'),
os.path.join(BASE_DIR / 'ayuda_otros_formus/templates'),
os.path.join(BASE_DIR / 'partidos_y_localidades/templates'),
os.path.join(BASE_DIR / 'internos_disi/templates'),
```

Entonces llamo a los templates desde donde quiera.

Otra cosa
Si voy a cambiar de Proyecto pero quiero usar la misma base. Podemos esto:
Inspeccionar la base y guardarla como el modelo deseado desde la consola:

```
python manage.py instpectdb --database internos_base >> modelos-internos.py
```

Esto va a exportar al archivo modelos-internos las tablas de esa base para poder trabajar traniquilos. Después desde el view deseado importamos el modelo  y la clase.


***26-11-23***

Otra cosa que me di cuenta cuando levantaba un sitio que estaba bajo un proxy_pass en nginx:

Me daba siempre error de CSRF

```
server
{
    listen 82;
    server_name 'nombre del sitio';

    location / {
        include proxy_params;
        proxy_pass http://192.168.0.145:5001/;
    }
}
```

Estaba todo bien condigurado y sin embargo no arrancaba.

hasta que di con una publicación (no recuedo dónde) que me decía que a partir de las versiones 4 de Django, hay que agregar en el settings.py esta sentencia para que ande con proxy_pass:

```
CSRF_TRUSTED_ORIGINS = ['https://tu-sitio.com.ar']

```

Es importante ponerle el http y y si tenes ssl, el https porque si no a mi no me anduvo.

<hr>

Lo voy a ir modificando a medida que quiera recordar algunas cosas mas... :P
