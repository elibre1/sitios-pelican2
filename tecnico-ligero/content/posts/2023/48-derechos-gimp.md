Title: Resolver permiso denegado en GIMP
Date: 2023-01-11 09:17
Autor: Hugovksy 
Slug: Pasos para resolver permiso denegado en GIMP
Lang: es
Tags: GIMP, permisos, GNU/Linux
Category: Gimp
Summary: Resolver permiso denegado Gimp
Imagen: /images/logo1.jpg


Estos días por cuestiones técnicas volví a una versión de Ubuntu en la que le instalé Gimp llevándome la sorpresa que al querer abrir de un disco esclavo una imagen para editar me aparecía ***permiso denegado*** Buscando en los foros de internet pude resolverlo simplemente tipeando esto en la consola:


```
snap connect gimp:removable-media :removable-media

```



