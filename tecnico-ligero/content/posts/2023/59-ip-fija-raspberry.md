Title: Configurar IP Fija en una Raspberry  
Date: 2023-07-22 11:08
Autor: Hugovksy 
Slug: ip-fija-raspberry
Lang: es
Tags: Raspberry, IP, Consola, GNU/Linux
Category: Raspberry
Summary: Me anoto (y para quien le sirva) los pasos para poner la ip fija en una Raspberry Pi con Raspbian

Es bueno recurrir a un sitio con los pasos puntuales para poner la IP fija de tu dispositivo, sobre todo cuando te cambian el módem y sos un fanático del autoalojamiento web, cuna de la independencia tecnológica y la resistencia anti corporativa (¡ahhhhh recontra anarquista me salió!). 

La cuestión es que desde el panel nunca me anduvo, hay que meter mano en la consola y no es /etc/netplan como en Ubuntu o en algunas distribuciones Debian
(no sé si en todas porque antes también configuraba con network/interfaces y francamente no sé si cambió para todos o qué).

Aquí los pasos:

* Abrimos consola

* Editamos como verdaderos técnicos que se respetan con Neovim y no con Nano como ofrece la fuente que cito abajo :P

```

sudo nvim /etc/dhcpcd.conf

```

* En  mi caso tengo la raspberry por Wifi, así que vemos lo siguiente:

```
interface wlan0
static ip_address=192.168.0.99/24
static routers=192.168.0.1
static domain_name_servers=1.1.1.1
static domain_search=



```

Donde dice 99 le ponés la IP que más te guste. Después Tenés que apuntar desde el ruter por Port Forwarding a la que asignaste (pero eso, eso es otra historia amigo).

Fuente [raspberry para novatos](https://raspberryparanovatos.com/tutoriales/asignar-ip-fija-raspberry-pi/)
