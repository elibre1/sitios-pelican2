Title: Viendo el mar en Mar de Ajó 
Date: 2023-04-01 17:16
Autor: Hugovksy 
Slug: mar-mar-de-ajo
Lang: es
Tags: Fotos, Paisajes
Category: Fotos
Summary: Un paisaje de marzo en Mar de Ajó, foto y nada más...
Imagen: /images/Mar-de-ajo-19-03-2023_thumb.jpg


<a href="/images/Mar-de-ajo-19-03-2023.jpg" target="_blank"><img src="/images/Mar-de-ajo-19-03-2023_thumb.jpg"/></a><br>


Una mañana de marzo me fui a ver el mar, nada nostágico. En realidad creo que era a la tarde, porque a la mañana llovió. Después jugamos a la pelota en la playa y hasta me metí en el mar...
