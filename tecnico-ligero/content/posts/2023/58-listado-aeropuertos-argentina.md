Title: Listado de aeródromos de Argentina 
Date: 2023-06-14 20:58
Autor: Hugovksy 
Slug: listado-aerodromos-argentina
Lang: es
Tags: Script, Aeródromos, Python
Category: Scripts
Summary: Este es un script que busca en una base de datos SQLite el listado de aeródromos de Argentina, provisto por la página del Gobierno Nacional

Quería (lo intenté y fallé) hacer un script para buscar vuelos de Argentina y sus precios y me dije: por dónde empiezo; bueno, primero hemos de conocer todos los IATA, que no sé las siglas pero vendrían a ser las definiciones internacionales de los aeródromos. Este pequeño archivo hecho en Python, como el resto de las cosas que hago, te permite buscar por Lugar o Provincia y consta de 3 columnas: 

* IATA
* Denominación
* Provincia



[Descargá el script](https://gitlab.com/enlacepilar/scripts-de-python/-/raw/main/17-consulta-aerodromos-Argentina.py?inline=false)

[Descargá la base](https://gitlab.com/enlacepilar/scripts-de-python/-/raw/main/aerodromos.db?inline=false)





Disponible en [El camino del script](https://gitlab.com/enlacepilar/scripts-de-python)
