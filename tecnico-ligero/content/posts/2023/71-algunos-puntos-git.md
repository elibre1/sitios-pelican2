Title: Algunos puntos para GIT
Date: 2023-12-24 14:58:47.284148
Autor: Hugovksy 
Slug: algunos-puntos-git
Lang: es
Tags: Git, GNU/Linux
Category: GIT
Summary: Algunas indicaciones que me guardé en un archivo de texto para configurar, desconfigurar y guardar usuario y correo en Git  
Imagen: /images/02-mini.jpg




Estas indicaciones las tenía guardadas en un archivo de texto y como soy adicto a la reinstalación de Gnu/Linux, muchas veces no me acuerdo dónde quedaba éste. Así que me dije, qué tal si lo subo a mi página y me dejo de boludeces...

<img src='/images/logo-git.jpg' alt='foto' class='img-fluid text-center'>

1) Desloguea credenciales
```
git config --global --unset user.name

git config --global --unset user.email

git config --global --unset credential.helper

git config --unset credential.helper store

```

2) Configurar usuario global

```
git config --global user.name "enlacepilar"

git config --global user.email "enlacepilar@protonmail.com"

```

3) Persistir usuario y clave

```
git config --global credential.helper store

```

En cualquier repo clonado a Gitlab u otro escribir:

```

git pull

```