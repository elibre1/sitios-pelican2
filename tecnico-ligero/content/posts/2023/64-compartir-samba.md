Title: Compartir archivos por Samba. Guía para engendros (como yo) 
Date: 2023-11-04 20:57
Autor: Hugovksy 
Slug: samba-para-engendros
Lang: es
Tags: samba, compartir, gnu-linux, terminal
Category: Samba
Summary: Este es un apunte para que cuando vuelva a reinstalar todo como me pasa bastante seguido por mis propias locuras, tenga al alcance un documento y no tener que andar puteando para ver los pasos...  
Imagen: /images/fotoNASA-7134.jpg



Primero y principal, claro, instalamos Samba en variantes de Debian (Ubuntu, Xubuntu, etc) con todas las mierdecillas para evitar inconvenientes, inclusive si querés navegar por cualquier plataforma (SO) como si querés compartir algo:

```

sudo apt install samba samba-client cifs-utils samba-common 

```

Después configuramos en la siguiente dirección, con tu editor favorito (yo uso la terminal y Neovim, porque soy un técnico que se respeta):

```
sudo nvim /etc/samba/smb.conf

```
Acto seguido agregamos la carpeta (muy windors)/directorio (bien GNU/Linux):

```
[compartida Hugovsky]
        comment = Archivos compartidos
        browseable = yes
        path = /home/hugovsky/Documentos/compartida-Hugovsky/
        guest ok = no
        read only = no
        create mask = 775
```

Despues, y esto es mucho uy importante, agregamos al usuario que queramos hacer engendro propietario para acceder a toda esta mierdecilla:

```
sudo smbpasswd -a hugovksy
```

Y le ponemos clave. Por último reiniciamos toda esta cosilla:

```
sudo service smbd restart
```

Y comprobamos si realmente ´tamo activo:

```
sudo service smbd status
```

