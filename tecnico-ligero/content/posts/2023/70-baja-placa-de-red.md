Title: Dar de baja una placa de red en GNU-Linux
Date: 2023-12-19 07:22:15.786746
Autor: Hugovksy 
Slug: baja-placa-de-red
Lang: es
Tags: Redes, Xubuntu, Gnu-Linux, Placas
Category: Redes
Summary: Una sentencia para dar de baja una de las placas de red del sistema (Debian y derivados)  
Imagen: /images/vuelta-de-obligado-mini.jpg


Tengo una notebook viejita, cuando digo viejita quiero decir anterior a 2010 y la reviví con 2 gb de Ram más y un disco sólido. Además para captar mejor señal le puse una de esas placas USB con doble antena para que reciba mejor el enlace de red Wi-Fi. El tema es que a veces por diversos motivos pongo la clave en la otra placa (pruebas, etc) y después me queda. Entonces me digo: por qué no la vuelo a la mierda. Es por ello que di con este sitio que comparto a continuación que me dio la posibilidad de que con este comando la demos de baja. A saber:
Primero que nada hemos de ver qué placasa tenemos. Hay muchos comandos para eso, ip ad, ip addr show, ifconfig. A mí me gusta el último pero como no viene instalado en las últimas (varios años ha) versiones de Ubuntu/Xubuntu la instalamos:

```
sudo apt install net-tools
```
Ahora si, tipeamos el comando:
```
ifconfig
```
Nos tendría que salir algo parecido a esto. Fijente en las que dice W, ya que las eth son generalmente las de cable.

<img src='/images/consola-redes01.jpg' alt='foto' class='img-fluid text-center'>

Y ahora sí, identificamos la que queremos deshabilitar, en mi caso la que no tiene IP

```
sudo ifconfig wlp5s0 down
```

No olvidar hacerlo con derechos de admin porque si no no vas a poder.

fuente:

<a href='https://cambiatealinux.com/activar-y-desactivar-redes-en-ubuntu/' target='_blank'>cambiatealinux</a>
