Title: Piedra, papel o tijera violento 
Date: 2023-06-14 10:09
Autor: Hugovksy 
Slug: pierdra-papel-tijera
Lang: es
Tags: Script, juego, Python
Category: Scripts
Summary: Vamos a jugar un poco al piedra, papel o tijera. Ojo: al semi bot este no le gusta perder, ni que te vayas, solo humillarte.
Imagen: /images/ppt-violento.jpg
 
Cansado ya de los bondadosos bots (o semi bots) que responden amablemente en qué podemos ayudarle, señor, he aquí uno que te juega Pierdra, Papel o Tijera y te agita para cagarte a tiros, machetazos o putear incansablemente si pierte, gana, o lo que sea. 

De todos modos, como es software libre, lo podés modificar para que te diga *"oh he perdido, que triste estoy"*, sólo tenés que cargarlo en las listas, tan sencillo como eso. A por él, chaparrito, es el Nro 20 de ***El camino del script de un servidor***, una colección de scripts sin mucho más sentido que pasar el rato como buen técnico ligero que soy.

```
#Piedra Papel o Tijera con PC VIOLENTA

import random
import time

puteadas_gana = ["¡Te cague a palos!", "Tomaaaaa wachooo", "Comeeee cagon",
            "¡Volvi a ganar hijo de re mil putas!"]

puteadas_pierde = ["Perdii la concha de su madre!", "Te voy a matar, empeza a correr logi",
                   "Voy a soltar los perror hijo de un camion lleno de putas",
                   "Voy buscando el arma asi te cago a tiros"]


sortea = ["Piedra", "Papel", "Tijera"]


def consulta():
    pregunta = input ("¿Seguis jugando, nenita? S/N\n")
    if pregunta == "S":
        print ("Asi se habla...\n\n")
        jugada()
    elif pregunta == "N":
        print ("Chau caquita, no vengas mas por aca...")
    else:
        print ("¿¿Sabes escribir?? ¡¡¡S o N!!!")
        consulta()
               



def evalua(pc,hm):
    #SI GANA
    if pc == "Piedra" and hm == "Tijera":
        print (random.choice(puteadas_gana))
        print ("\n¿Ya viene mami a buscarte, bebe? O seguimos\n")
        time.sleep(2)
    elif pc =="Papel" and hm == "Piedra":
        print (random.choice(puteadas_gana))
        print ("\n¿Queres ir llorando a casa?")
        time.sleep(2)
    elif pc =="Tijera" and hm == "Papel":
        print (random.choice(puteadas_gana))
        print ("\n¿Queres seguir siendo vapuleado, perdedor?\n")
        time.sleep(2)
    #SI EMPATA:   
    elif pc == hm:
        print ("\nEmpatamos. Maldito, tuviste suerte.\n")
        print ("\nDesempatemos como verdaderos guerreros, a morir...\n")
    #SI PIERDE:
    else:
        print ("\n"+random.choice(puteadas_pierde))
        print ("Dame la revancha o te cago a machetazos...\n")
        time.sleep(2)
    consulta()
        
        
        



def jugada():
    
    print ("¡¡PIEDRA PAPEL O TIJERA VIOLENTO!!")
    print ("Preparate para morir")
    time.sleep(3)
    print ("¡Agita tus manos, humano!")
    time.sleep(3)

    roboto = random.choice(sortea)
    humano = random.choice(sortea)

    print ("""ROBOTO: """+roboto+
           """\n\nHUMANO(vos): """+humano)

    evalua(roboto,humano)

jugada()

```

[¡Descargá Piedra, Papel o Tijera Violento!](https://gitlab.com/enlacepilar/scripts-de-python/-/raw/main/20-piedra-papel-tijera-violento.py?inline=false)

Disponible en [El camino del script](https://gitlab.com/enlacepilar/scripts-de-python)
