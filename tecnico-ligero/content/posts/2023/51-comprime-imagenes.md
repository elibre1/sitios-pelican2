Title: Script de Python para comprimir imágenes 
Date: 2023-01-28 20:43
Autor: Hugovksy 
Slug: comprimir-imagenes-python
Lang: es
Tags: Python, Pillow, Fotos, Comprimir
Category: Python
Summary: Un script para reducir el peso de las imágenes. Si son PNG las pasa a JPG. En caso de que el PNG tenga canal Alfa, coniverte con fondo negro.
Imagen: /images/miniaturas-prueba/20220506_111808_thumb.jpg


En alguna oportunidad es necesario reducir el peso de las imágenes y esa empresa puede verse abandonada cuando se trata de cierta cantidad de imágenes. O por decirlo de otra manera, mandamos todo al carajo y pasamos a otra cosa por tedioso, que queden las imágenes con el peso que tiene y las mil horas de carga, pero no, no: debemos luchar constra esos demonios. Es ahí donde un script como este, que reduce en masa el peso de los archivos JPG y PNG.  Si son PNG las pasa a JPG. En caso de que el PNG tenga canal Alfa (sin fondo), coniverte con fondo negro. 

Veamos un poco el código a ver de qué se trata: 

```

import os
from PIL import Image

cwd = os.getcwd()

print (cwd)
for archivo in os.listdir(cwd):
    if archivo.endswith('.jpg') or archivo.endswith('.png'):
        im = Image.open(archivo)
        x, y = im.size
        nombre_archivo = archivo.split(".")[0]
        extension_archivo = archivo.split(".")[1]
        print ("El ancho de la imagen es: "+str(x))
        print ("El alto de la imagen es: "+str(y))
        print ("=========================")
        
        if extension_archivo == "png":
            print ("Es PNG. Convirtiendo a JPG \n\n")
            im = im.convert("RGB")
            im.save(cwd+"/"+nombre_archivo+"-imagen_nueva.jpg","JPEG", quality=30, optimize=True) 

        else:
            im.save(cwd+"/"+nombre_archivo+"-imagen_nueva.jpg",quality=30, optimize=True) 
        


``` 

Primero, cabe aclarar, necesitamos el módulo Pillow, si no tenemos hay que instalarlo:

```
pip install pillow

```

Después examinamos en qué ruta estamos parados y examinamos el directorio con el **FOR**. Si los archivos terminan con *JPG* o *PNG* te muestra el ancho y alto de la misma y posteriormente, te corta la misma por un lado con su nombre y por el otro, con la extensión. Si llegase a ser la extensión *PNG*, lo que hace es convertirla a RGB y luego la graba como JPG, si no continúa el bucle y realiza ese último proceso.


El script se encuentra disponible también en el repo que armé (se llama ***09-comprimir imagenes.py***):

[Descargar Script](https://gitlab.com/enlacepilar/scripts-de-python/-/raw/main/09-comprimir_imagenes.py?inline=false)

[Mis Scripts Python](https://gitlab.com/enlacepilar/scripts-de-python){:target="_blank"}



