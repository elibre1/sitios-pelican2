Title: ¡Automatiza, chico, automatiza todo!
Date: 2023-11-16 13:07
Autor: Hugovksy 
Slug: automatizado-combinando-scripts
Lang: es
Tags: Scripts, Python, Terminal, Copias
Category: Automatización
Summary: Como ese loco (ficticio o no) que automatizó su vida en el trabajo, pero menos garca y más para el lado del bien, me hago algunas cosillas combinadas, por ejemplo, para publicar acá   
Imagen: /images/vuelta-de-obligado-mini.jpg

El punto es que quiero hacer más amena la publicación, y me ayudo con el script número 12 de [El camino del Script](https://gitlab.com/enlacepilar/scripts-de-python/-/blob/main/12-Miniatura-una-imagen.py?ref_type=heads) que reduce cualquier imagen un poco para que no sea un masacote pesado que no vuele la página.

Después escribo en un archivo markdown (extensión MD) todo lo que quiero poner y listo. Hasta ahí bien. 

El tema surge que quiero trabajar local con esto y publicar en la raspberry por ssh con comandos y todos, a saber:


**ATENCION - ESTOS SERÍAN LOS PASOS, NO EL SCRIPT SH**

```
ssh -p0000 usuario@192.X.X.X
cd Documentos/sitios-pelican/
source envPeli/bin/activate
cd tecnico-ligero/
pelican content
deactivate 
exit

```

Donde 0000 sería el puerto (por defecto es el 22) las X corresponden a la unidad de red.
Para acceder por ssh sin claves, deberías ver [esto antes](https://tecnico-ligero.enlacepilar.com.ar/posts/2022/acceder-por-ssh-sin-claves.html)



El script de extensión sh (o sea, subidor.sh, por ejemplo) está perfecto pero antes faltaría lo esencial, que es copiar el md y la foto a la carpeta content/posts de Pelican en este caso. 
Por cierto Pelican es un generador de sitios estáticos en los cuales está basado este sitio, si querés más info sobre ello deberías [ir acá](https://getpelican.com/)

Bien, pero antes de nada, ¿cómo copiamos a la carpeta deseada? Con el maravilloso comando de GNU/Linux scp

```
scp -P0000 66-copias-automatizadas.md usuario@192.X.X.X:/home/usuario/Documentos/sitios-pelican/tecnico-ligero/content/posts/2023/

```

Acordate, 0000 es blabla y X lo otro.

Voy a probar si funciona. Ya vengo.

¡Anduvo! Ahora la foto:


```

scp -P0000 mi-foto.jpg usuario@192.X.X.X:/home/usuario/Documentos/sitios-pelican/tecnico-ligero/content/images/

```

Por último ejecutamos el script de arriba. y publicamos. Obviamente con más confianza esto se puede generar en un solo script.

veamos el script final, por ejemplo subidor.sh:

```

scp -P000 66-copias-automatizadas.md enlacepilar@192.X.X.X:/home/usuario/Documentos/sitios-pelican/tecnico-ligero/content/posts/2023/
scp -P000 tu-imagen.jpg enlacepilar@192.X.X.X:/home/usuario/Documentos/sitios-pelican/tecnico-ligero/content/images/
ssh -p000 usuario@192.X.X.X "cd /home/usuario/Documentos/sitios-pelican/ && source envPeli/bin/activate && cd tecnico-ligero/ && pelican content && deactivate"

```



Le damos para que se ejecute como script:

```

sudo chmod +X subidor.sh

```

y cambiamos los derechos: 

```
sudo chmod 775 subidor.sh

```

Por último ejecutamos:

```
./subidor.sh

```


¡Y a gozar!
