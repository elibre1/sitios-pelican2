Title: Cargador de libros en una base sqlite 
Date: 2023-05-23 17:44
Autor: Hugovksy 
Slug: cargador-libros
Lang: es
Tags: Script, cargador,libros, Python
Category: Scripts
Summary: Script de Python que permite cargar títulos de libros, autorres, generos y año.
Imagen: /images/miniaturas-prueba/20220503_100557_thumb.jpg

**Carga-Ocrux script de rescate** 
Salí a la búsqueda por el mero hecho de ser un nostálgico, de un antiguo script que había hecho para guardar en una base el archivo histórico de ediciones de la editorial de mi viejo, desde 1985 hasta 2012. Este sirve para cargar libros por año, titulo,autor, género y poder agregar un breve comentario. Lo que corregí gracias a esta publicación de [stackoverflow](https://stackoverflow.com/questions/40645216/check-if-sql-table-exists-in-python) es generar la tabla si no existe. La verifica y te permite continuar, antes tenía que crearla a mano. Esos tiempos terminaron :)

Vamos a verlo:

```

import sqlite3

con = sqlite3.connect("ocrux.db")
cursor=con.cursor()


def salida():
    print ('')
    sal = input ("carga mas (enter) (X para salir)")

    if sal == "X":
        con.close()
        print ('')
        print ('Cierro conexión')
        print ("Adiosin")
        
    else:
        carga()


############### Función carga ###################  
def carga():
    #ant=("")# acá voy a almacenar el ultimo ID
    #datos = cursor.execute(" SELECT * FROM libros WHERE ID = (SELECT MAX(ID) FROM libros);")
    #for i in datos:
    #    ant=i[0]

    #id1=ant+1 #al id le sumo 1 de la variable "ant"
    año=input("Año de la edición: ")
    titulo=input('titulo: ')
    genero=input('genero: ')
    autor=input('autor: ')
    com =input('fecha de publicación / comentario: ')

    con.execute("INSERT INTO libros(año,titulo,genero,autor,comentario) VALUES (?,?,?,?,?)",(año,titulo,genero,autor,com))

    print ("datos cargados")   
    con.commit()

    ### imprime los datos cargados
    datos2 = cursor.execute(" SELECT * FROM libros WHERE ID = (SELECT MAX(ID) FROM libros);")
    for i in datos2:
        print (i)
    salida()
############### Fin de función carga ###################


def crear_tabla():
    cursor.execute( """CREATE TABLE libros
            (id INTEGER PRIMARY KEY AUTOINCREMENT,
            año INT (10),
            titulo VARCHAR (255),
            genero VARCHAR (255),
            autor VARCHAR (255),
            comentario VARCHAR (255)
            );
    """)
    carga()


# Verifica si la tabla existe
print('Verificamos si la tabla existe en la base:')
verifica = cursor.execute(
  """SELECT name FROM sqlite_master WHERE type='table' AND name='libros';""").fetchall()
 
if verifica == []:
    print('No se encontró la tabla. La creamos...')
    crear_tabla()
else:
    print('¡Tabla libros encontrada! Continuamos.')
    carga()


```
[Descargá el script cargador](https://gitlab.com/enlacepilar/scripts-de-python/-/raw/main/14-carga-ocrux.py?inline=false)

Disponible en [El camino del script](https://gitlab.com/enlacepilar/scripts-de-python)
