Title: Tema propio Pelican 
Date: 2023-01-23 11:58
Autor: Hugovksy 
Slug: Construyendo un tema propio para el sitio estático Pelican
Lang: es
Tags: Pelican, Temas, construir
Category: Pelican
Summary: Construyendo un tema propio para el sitio estático Pelican
Imagen: /images/logo-pelican.svg


<img src="/images/logo-pelican.svg" alt="Logo Pelican" width="300px">

# Tema Geliba para Pelican sitios estaticos 



## ¿De qué se trata?

Es un tema creado desde cero para el generador de sitios estáticos Pelican. Pero como nada nace de la nada, claro está, basé mis configuraciones en los temas [brutalist](https://github.com/mc-buckets/brutalist/tree/de551620221ec3f1958250adfaffbbc81e9b748c) y [bootlex](https://github.com/getpelican/pelican-themes/tree/master/bootlex), los cuales se encuentran en el repositorio oficial de Pelican Themes.

## ¿Qué hay en este tema?

* Hay un archivo "base" que contiene los accesos a Boostrap 4, Animatecss y un CSS propio para los colores y las fuentes. El H1 tiene la tipografía fanzine-title y los H2 supercomputer. Tiene un ***include*** a un archivo de barra superior, para que de esta forma se encuentre todo más ordenado. En el pie o ***footer*** está el año corriente y alusión al autor que se encuentre configurado en el Pelicanconf de tu sitio.

* La barra superior tiene un acceso en **images/logo.png**, esto quiere decir que si vos ponés una imagen con ese nombre en esta carpeta, dentro de tu ***content***, vas a tener el logo funcional.

* Siguiendo con la barra superior, las páginas que agregues van a estar representadas como enlaces allí (fijate en las capturas para mejor guía). Después tenés un menú con accesos al archivo, las etiquetas del blog y las categorías. Si no te gusta o no te sirve, lo podés eliminar de la barra y listo. :)

* En la página principal lo que quise hacer es traer todos los artículos escritos con el formato ***card*** de Bootstrap. La imagen del costado la podés traer de la siguiente manera (ejemplo en markdown): 

```

Title: Desarrollando una web a los ponchazos con Flask
Date: 2020-06-12 20:18
Author: Hugovksy
Category: Flask
Slug: web-a-los-ponchazos-flask
Status: published
Imagen: /images/fotoNASA-7134.jpg

Arranco esta primera publicación con el proyecto que vengo realizando sobre la web de Ocruxaves que decidí migrar de un [blog](http://hugo-enrique-boulocq.blogspot.com/) hacia [esta](https://ocruxaves.com.ar/) (espero que siga vigente cuando hagas click). La idea inicial era trabajar en Python y no moverme de eso, agregarle una base de datos en Mysql. En el camino descubrí que eso (montar la web) tenía que hacerlo con un Framework enorme que había oído nombrar que se llama Django o con las posiblidades más "ligeras", como son Flask o Bottle. Probé las 2 pero me decanté por la primera, más que nada por la información para poder levantarla (había más).

```

O sea, cuando escribas tu artículo agregá la ruta a tu imagen para que la veas representada en tu página principal, si no el mismo sitio te proveerá una genérica.


* Las fechas de los artículos están configuradas en español.


[Repositorio del tema](https://gitlab.com/enlacepilar/tema-geliba-para-pelican-sitios-estaticos){:target="_blank"}

Por el momento eso es todo, cualquier cosa me escribís a:

[enlacepilar](mailto:enlacepilar@protonmail.com)




