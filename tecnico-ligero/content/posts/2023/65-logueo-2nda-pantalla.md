Title: Configurar pantalla de logueo en la pantalla que quieras  (probado en Ubuntu)
Date: 2023-11-13 20:57
Autor: Hugovksy 
Slug: dos-monitores-ubuntu
Lang: es
Tags: Logueo, Ubuntu, gnu-linux, monitores
Category: Monitores
Summary: Comandos infalibres para tener la pantalla de logueo, si tenés 2 monitores, en la que quieras. Probado en Ubuntu.  
Imagen: /images/fotoNASA-7134.jpg

Este apunte parte de esta base:

* Tenés Ubuntu instalado

* Tenés 2 monitores, o uno y una tele, etc.

* Querés que la pantalla de logueo esté donde se te antoje

Para eso vamos a ir a la configuración, monitores y seleccionar como pantalla primaria donde querés loguearte.

Acto seguido abrimos la terminal y copiamos lo siguiente (copiamos el archivo de configuración monitors.xml al directorio que dice ahí):


```
sudo cp ~/.config/monitors.xml ~gdm/.config/

```

Si por algún motivo queremos verificar y/o editar esto, editamos con nuestro editor favborito:

```

sudo nvim ~gdm/.config/monitors.xml

```

Reiniciamos y comprobamos. Si no funcionar tiramos todo a la mierda y nos vamos a leer una novela.
