Title: Probando Etherpad: escritura colaborativa. Instalando en Raspberry 
Date: 2023-03-31 20:09
Autor: Hugovksy 
Slug: instalando-etherpad-en-raspberry
Lang: es
Tags: etherpad, colaborativo, instalar
Category: Etherpad
Summary: Probando Etherpad para escritura colaborativa, instalando en Raspberry y accediendo desde el exterior.


Como habrán podido leer en este blog ligero, si es que llegaron a hacerlo, todo lo que puedo probar y colgar en la red se encuentra autohosteado en una Raspberry Pi hogareña, porque me gusta promover eso mismo, el autoalojamiento de páginas web, el software libre y la lucha contra la obsolescencia.

El tema que nos trae por esta publicación es Etherpad, una herramienta de escritura colaborativa, que me habían mencionando en la facultad hace años pero que realmente le di bola por estos días, cuando tuve que presentar un trabajo para corregir para un profe amigo que me refrescó la memoria y me lo recomendó. Entonces me dije, por qué no colgarla de la Raspberry, cómo que no. 

Después de haber fallado intentando dockerizar, probé lo que me quedó a la mano.
El proceso es bastante sencillo. Debemos tener instalado curl en Raspbian anantes que nada y tipear(copiar) linea a linea lo siguiente:

```
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -

sudo apt install -y nodejs

git clone --branch master https://github.com/ether/etherpad-lite.git &&
cd etherpad-lite &&
src/bin/run.sh      

``` 

Esto nos va a dejar corriendo una ventana donde nos indicará que podremos acceder por el puerto 9001 desde nuestro navegador. En principio si viste otras publicaciones, vas a poder acceder eventualmente desde la lan con la ip de tu Raspberry o desde afuera si tenés todo configurado para poder acceder y l puerto 9001 abierto.

También podemos ir a [duckdns.org](https://www.duckdns.org/){:target="_blank"} y configurar el acceso por nombre como se puede ver en esta imagen, sin necesidad de apache por medio.

<img src="/images/etherpad.png" alt="Etherpad" class="img-fluid">

[Repo Etherpad](https://github.com/ether/etherpad-lite){:target="_blank"}



