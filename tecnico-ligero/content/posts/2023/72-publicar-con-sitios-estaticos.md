Title: El camino de la publicación con un sitio estático
Date: 2023-12-25 08:51:25.231605
Autor: Hugovksy 
Slug: publicar-con-sitios-estaticos
Lang: es
Tags: sitios estáticos, scripts, python, terminal, remoto
Category: Sitios Estáticos
Summary: Explico en esta publicación el derrotero de un técnico ligero que termina (o sea yo) publicando de esta manera. El detalle de mis pasos para publicar con el generador de sitios estáticos Pelican  
Imagen: /images/v-obligado01-mini.jpg

La motivación de escribir en un sitio estático, es decir, volver a las raíces de la web y combatir la obsolescencia programada a veces no es tan fácil para el técnico ligero.

En publicaciones anteriores que no tenés por qué leer, tampoco es que este sitio destila cantidades ingentes de visitantes así que hago un resumen, describía cómo con un script hacía los pasos previos para publicar con el generador de sitios estáticos Pelican de Python.
Vamos a ver algunos detalles para aclarar mi ejemplo, que quizás le pueda servir a alguien más.

<div class='text-center'>
<img src='/images/v-obligado02-mini.jpg' alt='foto' class='img-fluid'>
</div>
***Las fotos las pongo porque me gusta, además de que tienen que ver con lugares que fui visitando***


* Tengo una Raspberry Pi 4 con Ip "casi" fija en casa.
* Tengo Nginx como administrador de peticiones web en el puerto 80 y 443 (ssl)
* Tengo una carpeta en Documentos que se llama ***sitios-estáticos*** donde guardo las publicaciones de este modo: **/tecnico-ligero/content/posts/2023/72-esta-publicacion.md** por ejemplo.
* Dentro de ***sitios-estáticos*** tengo una carpeta llamada **envPelican** que aisla el contenido de los complementos que necesite de Python para poder editar, como el caso más puntual: Pelican
* Pelican te genera esta carpeta content a través de una serie de preguntas, dentro tenes que crear la carpeta ***images***, ***posts*** y ***pages***, donde images es para todas las fotos que subas, posts para las publicaciones periódicas y pages para las páginas de presentación con algún contenido. 
* En el sitio que generaste, en este caso tecnico-ligero, en la raíz tenés un archivo de configuración donde le podés indicar la salida del sitio web generado, es propio de Pelican y hay varios tutoriales de la configuración de este archivo, de hecho <a href="https://docs.getpelican.com/en/stable/" target="_blank">acá</a>
* Después a escribir en formato Markdown, que es el que me gusta o Restructuredtext, que no.
* **pelican content** es la sentencia que te publica todo y genera el sitio.

##Hasta acá muy lindo pero ¿qué tiene que haber en ese markdown?

Tendría que estar, en principio esto:

```
Title: El camino de la publicación con un sitio estático
Date: 2023-12-25 08:51:25.231605
Autor: Hugovksy 
Slug: publicar-con-sitios-estaticos
Lang: es
Tags: sitios estáticos, scripts, python, terminal, remoto
Category: Sitios Estáticos
Summary: Explico en esta publicación el derrotero de un técnico ligero que termina (o sea yo) publicando de esta manera. El detalle de mis pasos para publicar con el generador de sitios estáticos Pelican  
Imagen: /images/v-obligado01-mini.jpg

```

Y abajo te ponés a escribir con formato markdown (te podés ver un tutorial, no es difícil y es combinable con HTML).

Generar esto se me hizo tedioso cada vez que escribía, clonar el archivo, copiar todo al servidor con scp desde la terminal o publicar directamente desde allí... un embole. 

He ahí que las tareas repetitivas al técnico ligero no le gustan para nada y aprovecho para enunciar el lema que ha de llevarte al paraíso:

***Automatizar todo***

* Por eso me hice un script de Python que te va haciendo unas preguntas, las del encabezado del archivo.md
* Además de eso, una pequeña función para reducir las fotos sacadas de **El camino del script** al tamaño que yo quiera.
* Por último, un archivo sh para después publicar directamente en el sitio, que sube todas las fotos y el archivo md, después lo publica automáticamente
* Eso quiere decir que sólo me deja el espacio para lo que no se puede automatizar, que es escribir. 

Próximamente comparto todo el contenido del script y los módulos. Arrevuaaaaa


<div class='text-center'>
<img src='/images/v-obligado03-mini.jpg' alt='foto' class='img-fluid'>
</div>
***Otra foto de Vuelta de Obligado***
