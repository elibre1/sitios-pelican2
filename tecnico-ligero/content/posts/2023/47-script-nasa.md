Title: Script para bajar fotos de la Nasa
Date: 2023-01-10 18:56
Autor: Hugovksy 
Slug: script para bajar fotos de la Nasa
Lang: es
Tags: script, fotos, Nasa, Python
Category: python
Summary: Acceder por SSH sin claves
Imagen: /images/fotoNASA-7134.jpg


Estaba buscando cómo conectarme a una api y ver qué onda, lo que me llevó a dar con una web llamada **Recursos Python** donde explicaba cómo bajar la foto del día de la Nasa. Era muy interesante, pero me guardaba una sola foto y con algunas muy muy simples modificaciones como me gusta a mí, lo que hice fue que cambiara el nombre y generara un número aleatorio para que no se pisara. Además, tiré un rango de fechas para que buscara, por ejemplo, entre el 2005 y el 2022. 

Obvio me faltan algunas cosas por modificarle, como por ejemplo que se pueda ver la foto bajada en un navegador, cosa que he hecho en otra ocasión, pero como no lo ***sentí*** en este momento, lo dejamos quizás para más adelante.

```
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
import datetime
import random

fecha=datetime.date(random.randint(2005,2022), random.randint(1,12),random.randint(1,28))

numero = random.randint(0,10000)
params = {
    "api_key": "to5gfAyIUfqz7pKwfl1zjRYh06ElbcZXszhmk1Qg",
    "date": fecha
}
#r = requests.get("https://api.nasa.gov/planetary/apod", params=params)
r = requests.get("https://api.nasa.gov/planetary/apod", params=params)
if r.status_code == 200:
    results = r.json()
    url = results["url"]
    # Si es una imagen guardar el archivo.
    if results["media_type"] == "image":
        with open("fotoNASA-"+str(numero)+".jpg", "wb") as f:
            f.write(requests.get(url).content)
        print ("La imagen se guardó corretamente")
    else:
        print(url)
else:
    print("No se pudo obtener la imagen.")


```


Un ejemplo de lo que se puede bajar:

![Foto Nasa](/images/fotoNASA-7134.jpg){:.img-fluid}


Fuente:
[Recursos Python](https://recursospython.com/guias-y-manuales/imagenes-satelitales-nasa-apod/){:target="_blank"}


Si sos vago y no querés copiar, acá está el script para descargar (botón derecho, guardar como, -todo tengo que decirte, todo :P):

[Script-Nasa](/images/documentos/04-Fotos-NASA.py)

[Y directo del repo](https://gitlab.com/enlacepilar/scripts-de-python/-/raw/main/04-Fotos-NASA.py?inline=false)


