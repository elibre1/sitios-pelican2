Title: Generador de blogs idenpendientes y sencillos Elibre 
Date: 2023-06-06 08:51
Autor: Hugovksy 
Slug: elibre-generador-de-blogs
Lang: es
Tags: Script, blog,generador, Python
Category: Proyectos
Summary: Estoy trabajando en un generador de blogs libre, tengo el mio para que veas como funciona. En este caso lo uso para Gifs animados y fotos.
Imagen: /images/miniaturas-prueba/20220503_100557_thumb.jpg

**Elibre - Un Generador de Blogs Libre pensado para fotos y Gifs Animados** 
Y por qué no, para escribir también con alguna fotillo de muestra. El tema era el siguiente, cuando quiero publicar por acá, sobre todo fotos, se me hacía medio complicado, además de que acá me dedico a las memorias técnicas y sólo técnicas para la posteridad. Pero este no es el tema que nos atañe ahora, sino que estaba buscando volver a los scripts pero no puedo evitar la web y quería hacer Gifs animados, esa es la cruda realidad, sólo gifs animados y publicarlos, porque estoy viendo esos sitios locos que no tienen una sola publicidad y yo quiero lo mismo. Sin irme demasiado por las ramas vamos a ver de qué se trata.

Acá algo del readme:

## Blog de Elibre
Es un blog realizado con Python con Flask (inicialmente con Bottle pero tuve problemas con las imágenes) y almacenando las entradas en SQLite. La idea es poder escribir sin complicaciones en el script 15, darle una imagen si tuviera y subir por FTP todo a un lugar en mi Raspeberry de casa. Este proyecto se bifurca del tedio de:
* No querer tener Wordpress por todo lo que ello implica.
* Probar algo mejor (más sencillo para vagos como yo) que los sitios estáticos de Pelican, en donde vengo publicando hace un tiempo.
* Scriptear, que es lo que más me gusta de la informática.
* Sacar algunas fotos y hacer gifs animados.

## ¿Qué más?

Se puede clonar y adaptar, ¡¡es Software Libre papá!!

Al día de hoy podés hacer lo siguiente:

* Crear publicaciones desde cualquier dispositivo desde la ruta /crear

* Crear una presentación en el sector presentación a través del script "querés-crearte una presentación"

* Subir gif animados

* Subir fotos y si son JPG te las reduce para más placeeeer, diría Homero.

* Si no tenés fotos y querés subir igual algo, reemplazá en la carpeta "/static/imagenes" la foto "sin-imagen.webp" porque te la inserta por defecto :)

* Podés publicar desde el script de python o desde la web, en cuanlquier lado y desde cualquier dispositivo.

Acá el repositorio para que te lo clones
[Repo ELibre](https://gitlab.com/enlacepilar/elibre-blog)


Acá el sitio funcionando (me autopublicito):

[sitio Elibre](https://elibre.enlacepilar.com.ar/)
