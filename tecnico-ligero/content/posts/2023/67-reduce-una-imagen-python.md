Title: Script reductor de imagenes a la mitad
Date: 2023-11-16 19:29:32.997136
Autor: Hugovksy 
Slug: reduce-una-imagen-python
Lang: es
Tags: Reduce, Scripts, Python, Imagenes
Category: Scripts
Summary: Basándome en el script *02-genera-thumnails.py* creé el *27-Reduce-una-imagen-a-la-mitad.py*, que pide el nombre de una foto y la reduce a la mitad. Se puede mejorar si la imagen es muy pesada y reducirla 3 veces, 4, etc. Es sólo el principio y se puede mejorar :) Pero... ¿De qué estoy hablando? Leé mas y enterate  
Imagen: /images/cadenas01-mini.jpg


Me basé en el script mencionado de  [El camino del Script](https://gitlab.com/enlacepilar/scripts-de-python/-/blob/main/12-Miniatura-una-imagen.py?ref_type=heads){:target="_blank"}. Parece que te lo estoy vendiendo pero no es así, de ninguna manera :P


¿Qué es ***El camino del script***?
[Enterate acá](https://tecnico-ligero.enlacepilar.com.ar/pages/el-camino-del-script.html){:target="_blank"}
```

from PIL import Image

imagen = input ("Ingrese nombre de la imagen: ") 

img = Image.open(imagen)
width, height = img.size
print ("La imagen tiene un ancho original de: "+str(width))
print ("La imagen tiene un alto original de: "+str(height))
input("Reduciendo a la mitad... (enter)")
img.thumbnail((width/2, height/2), Image.ANTIALIAS)
nom = imagen.split ('.')[0]
tnom = nom + "-mini.jpg"
img.save(tnom, "JPEG")
print ("¡Imagen reducida!")

```
Esta es una posibilidad si sabemos que la imagen a la mitad va a servir, en el otro script elegía arbitrariamiente un formato y a la mierda. 

