Title: Configurar como Mono la salida en Ubuntu con un simple script
Date: 2023-11-17 19:42:23.433865
Autor: Hugovksy 
Slug: salida-mono-ubuntu
Lang: es
Tags: Terminal, Gnu-Linux, Sonido
Category: Sonido
Summary: Esto pasa cuando tenés unos parlantes en la compu que suenan lindo, pero que los cables andan para la mierda, entonces buscamos una solución ligera...  
Imagen: /images/mate_y_naturaleza-mini.jpg

El tema que nos trae esta tarde por aquí es el siguiente. Como bien se ha mencionado en la descripción, tengo un equipo que suena bastante decente, de esos antiguos *home theater* con el bajo con cable, un regulador... Ahora muestro una imagen mejor:


<img src="/images/parlantes.jpg" alt="parlantes" class="img-fluid">

No es compo para escuchar ***Riders on the Storm*** de un lado solo. O te perdés a Manzarek o te perdes a Robbie... definitivamente no.
Es por ello que buscando desesperadamente en Internet econtré este código que te hace el mono, vieja:

```
pacmd load-module module-remap-sink sink_name=mono master=$(pacmd list-sinks | grep -m 1 -oP 'name:\s<\K.*(?=>)') channels=2 channel_map=mono,mono
```
Esto lo guardamos como un archivo **.sh** le damos los derechos (sudo chmod +X y 775)y ahora viene lo mejor, lo cargamos al inicio de la sesión :)
Para ello tipeamos *Aplicaciones al incio*, cargamos la ruta del script y como digo siempre: ¡A gozar!. Todo en mono, claro.
