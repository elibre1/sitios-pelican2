Title: Mostrar imagen con Pillow y Tkinter en Python
Date: 2023-01-24 10:14
Autor: Hugovksy 
Slug: mostrar-imagen-pil-tk
Lang: es
Tags: Python, Pillow, Tkinter
Category: Python
Summary: Mostrando una imagen con Pillow y Tkinter en Python (sin programas externos)
Imagen: /images/muestra-imagen-PIL.jpg


Sigo probando el módulo Pillow de Python, que me parece fantástico. Estaba buscando mostrar una imagen con el módulo mencionado, pero no con el visualizador de fotos nativo del sistema operativo donde me encontrase, si no dentro de Python. Una alternativa interesante que encontré fue sumando Tkinter. 

El código es bien sencillo:

```

from PIL import Image, ImageTk 
import tkinter as tk

caja = tk.Tk()
caja.title("Muestra imagen")
caja.geometry('400x280')
imagen = Image.open("raspberry.png")

tamano_imagen_deseado=(398,278)

salida = imagen.resize(tamano_imagen_deseado)

tkimagen = ImageTk.PhotoImage(salida)
tk.Label(caja, image=tkimagen).pack()
caja.mainloop()


```

Entonces, importamos el método Image e InageTK de Pillow (PIL) y Tkinter como tk.
Le damos forma a la caja de salida de 400x280 y a la imagen de 398x278. Luego reconvertimos a ese tamaño (tkimagen = ImageTk.PhotoImage(salida) y finalmente la mostramos.

Cabe aclarar que puse **raspberry.png** como imagen alusiva. En el caso de usarlo hay que poner la ruta correspondiente si no se encuentra en la misma raíz que el script.

Acá abajo, una captura del resultado de mi Raspberry:

![Imagen Pillow](/images/muestra-imagen-PIL.jpg){:.img-fluid}

El script se encuentra disponible también en el repo que armé (se llama ***07-abrir-imagenes-pillow.py***):
[Descargar Script](https://gitlab.com/enlacepilar/scripts-de-python/-/raw/main/07-abrir-imagenes-pillow.py?inline=false)


[Mis Scripts Python](https://gitlab.com/enlacepilar/scripts-de-python){:target="_blank"}



