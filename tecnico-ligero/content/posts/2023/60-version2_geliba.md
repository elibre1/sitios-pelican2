Title: Nueva versión de GeLiBA, script generador de bibliotecas  
Date: 2023-08-27 10:58
Autor: Hugovksy 
Slug: version2-geliba
Lang: es
Tags: geliba, scripts, python
Category: Scripts
Summary: Nueva versión del script generador de bibliotecas libras GeLiBA, con modificación estructural, de diseño final, realizado para el Trabajo Final de la Tecnicatura en Software Libre de la Universidad Nacional del Litoral. ¿Más info? Ingresá...

Un técnico ligero que no se encuentra constantemente en el derrotero de la búsqueda espiritual a través de los scripts, disculpen, señores y señoras, no es un verdadero técnico ligero, un guerrero del script, un activista de la variable, un aikidoca de los condicionales.

Es así que el código me ha llevado hasta aquí, una nueva versión del incansable script creador de bibliotecas simples y automatizadas, preparadas especialmente para redes de área local y sin internet, donde se puedan ofrecer documentos para descargar y llevarte. Siempre pregonando las licencias libres, claro está, porque GeLiBA es Software Libre, demás está decir.

**¿Un ejemplo?**

Dejemos por un momento nuestra postura beneficiosa de tenerlo casi todo, ser ricos digamos, porque tenemos trabajo, tenemos para comer, un techo que nos cobije y poder darnos 2 duchas calientes todos los días, como diría Darín.

Imaginemos por un momento una escuela rural, alejada de todo, donde los proveedores de internet no llegan porque no es negocio para ellos y por ende, les quita acceso a la cultura. Un pequeño, pequeñísimo aporte que hace GeLiBA es poder generar un sitio web con el listado, por ejemplo, de libros generado por un maestro previamente, llevarlo en un pendrive y cargarlo en esa PC sin conexión, capaz se puede conseguir un router y hacer una lan, y si no hay, de ouede usar simplemente como PC de consulta y que permita descargar y llevarse los documentos a los alumnos de una manera más simple. Esta es una de las tantas ideas que pueden llegar a surgir.

Claro está que disto enormemente de solucionar algo con este ejemplo citado, pero se pretende aportar cultura de una manera, digamos, sencilla, generando un sitio para poder descargar documentación, libros, manuales y cualquier archivo de texto en formato PDF, ODT y TXT.


¡Para más info entrá al repo!

  [Repositorio GeLiBA](https://gitlab.com/enlacepilar/generador-libre-de-bibliotecas-automatizadas){:target="_blank"}

