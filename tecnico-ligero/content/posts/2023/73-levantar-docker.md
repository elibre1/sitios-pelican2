Title: Crear una imagen Docker y levantar un contenedor
Date: 2023-12-25 11:50:21.138398
Autor: Hugovksy 
Slug: sentencias-docker
Lang: es
Tags: Docker, Dockerfile
Category: Docker
Summary: Sentencias para no olvidarme. Cómo creo una imagen en base a un Dockerfile y lo levanto en un puerto determinado.  
Imagen: /images/vuelta-de-obligado-mini.jpg


###Este es un resumen, mucho, mucho más acotado que se centra en levantar el contenedor de <a href='https://tecnico-ligero.enlacepilar.com.ar/posts/2023/desplegar-django-docker.html' target='_blank'>esta publicación</a>



Partimos de un Dockerfile, que en caso de este proyecto en Django el cual me baso, sería más o menos así:

```
# Desde la imagen
FROM python:3.10

# Quién lo mantiene
MAINTAINER Hugovsky

#agregar el proyecto a usr/src/app folder
ADD . /usr/src/app

#Directorio de trabajo
WORKDIR /usr/src/app

COPY requerimientos.txt ./

# Instalar con pip los requerimientos:
RUN pip install --no-cache-dir -r requerimientos.txt

#Exponer puerto y ejecutar gunicorn
CMD ["gunicorn", "disiformu.wsgi:application", "--bind", "0.0.0.0:5010", "--workers", "3"]

EXPOSE 5010
```

Ahora sí, tenemos el Dockerfile, vamos a crear la imagen:

```
docker build --tag disiformu_v5:latest .
```


Y levantamos el contenedor:

```
docker run --name disiformu_v5 --restart=always -d -p 5010:5010 disiformu_v5:latest
```


Hay una herramienta muy insteresante que se llama  <a href='https://www.portainer.io/' target='_blank'>Portainer</a> que sirve para manejar desde la web todos los contenedores, pero eso amerita otra publicación :P

