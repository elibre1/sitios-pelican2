Title: Miniatura de una imagen 
Date: 2023-04-01 17:44
Autor: Hugovksy 
Slug: miniatura-de-una-imagen
Lang: es
Tags: Script, miniaturas, imagenes, Python
Category: Scripts
Summary: Script de Python para generar una sola minuatura a partir de una sola imagen.
Imagen: /images/Mar-de-ajo-19-03-2023_thumb.jpg

Para la publicación anterior precisaba generar solo una miniatura y que además me crear los enlaces a la miniatura para la publicación y otro para la foto original. 

Partí del script 02-genera-thumnails.py de mi [colección de scripts](https://gitlab.com/enlacepilar/scripts-de-python){:target="_blank"} que me estoy haciendo (libres claramente). Este lee toda una carpeta, y este no era el caso, necesitaba uno solito. Veamos el código:

```
from PIL import Image

print ("Reduce tu imagen")
print ("===============")

archivo_original = input ("Escribí el nombre de la imagen: ")


if archivo_original.endswith('.jpg'):
    im = Image.open(archivo_original)
    im.thumbnail((500,375), Image.ANTIALIAS)
    nom = archivo_original.split ('.')[0]
    miniatura = nom + "_thumb.jpg"
    print(im.size)
    im.save(miniatura, "JPEG")
    print ("Imagen convertida a 500x375")
    print ("HTML para pegar en la carpeta images images")
    print ('(<a href="/images/%s" target="_blank"><img src="/images/%s"/></a><br>' % (archivo_original, miniatura))

``` 

* Como se puede ver, es simple. Importamos Image de PIL y le pedimos al usuario que pegue o tipee el nombre de la foto (en este caso JPG)

* Convertimos a 500x375

* Cortamos la extensión del archivo para poder generarle a la miniatura un nuevo nombre.

* La guardamos y finalmente mostramos por pantalla el código para pegar en nuestra publicación MarkDown.

[Descargá el script](https://gitlab.com/enlacepilar/scripts-de-python/-/raw/main/12-Miniatura-una-imagen.py?inline=false)
