Title: Mejorando la reducción de imágenes
Date: 2023-11-19 12:22:59.446123
Autor: Hugovksy 
Slug: reduce-imagen2
Lang: es
Tags: Scripts, Imágenes, Python
Category: Scripts
Summary: Estoy mejorando un poco el script en el que venía trabajando, no me cerraba del todo que dividera por 2 en todos los casos...  
Imagen: /images/vuelta-de-obligado-mini.jpg


En este script mejor pregunto qué ancho querés que tenga la imagen, obvio te muestra primero qué pixeles tiene la original. Todo esto en un script de Python ayudándonos del complemento Pillow

```
from PIL import Image

imagen = input ("Ingrese nombre de la imagen: ") 

img = Image.open(imagen)
ancho, alto = img.size

print ("La imagen tiene un ancho original de: "+str(ancho))
print ("La imagen tiene un alto original de: "+str(alto))

ancho = input ("ingresar ancho en pixeles, menor a 500 y proporcional: ")
alto = input ("ingresar alto proporcional: ")

    
input("Reduciendo... (enter)")
img.thumbnail((int(ancho), int(alto)), Image.ANTIALIAS)

nom = imagen.split ('.')[0]
mini = nom + "-mini.jpg"
img.save(mini, "JPEG")
print ("¡Imagen reducida!")

``` 
<img src='/images/sin-cadenas-mini.jpg' alt='foto' class='img-fluid text-center'>
