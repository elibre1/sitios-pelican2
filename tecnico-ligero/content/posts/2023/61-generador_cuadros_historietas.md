Title: Generador de cuadros de historietas con un Script de Python  
Date: 2023-09-21 18:18
Autor: Hugovksy 
Slug: generador-cuadros-historietas
Lang: es
Tags: historietas, scripts, python
Category: Scripts
Summary: Un técnico ligero al que le guste dibujar y que se jacte de ello no debe dejar pasar por su vagancia este script para generar cuadros de historietas, con un script de Python y de esta manera por ligero, evitar dibujar los rectángulos... Leé más si no te da fiaca...
Imagen: /images/fotoNASA-7134.jpg
Modified: 2023-09-25 12:30

Estoy haciendo un curso de historietas para rememorar aquellos años de preadolescencia donde inventaba y plasmaba en papel mis propias aventuras, que no eran más que viles copias de los super héroes de turno, a saber: tenía uno que se llamaba "El Pegadizo", que trepaba las paredes y tiraba... pegameno de las manos. Claramente nada que ver con el hombre araña. Después había creado otro que se vestía igual que súperman, pero tenía antifaz, un error que corregí porque nadie en el mundo mundial cree que Clark Kent oculte bien su identidad.

En fin, vamos a por ello. Como soy altamente ligero y es verdad lo del curso de historietas (está al día de la fecha en la página de [Conectar Igualdad](https://formacion.conectarigualdad.edu.ar/)) quería empezar a dibujar en cuadros, pero me dije, para qué están los scripts de Python si no es para automatizar la vida. Y fue así como me serví del bueno del complemento que crea PDFs: FPDF y con algunos retoques con el otro complemento random hice la magia. Vamos a ver el detalle:

Bueno basta de divagues existencialistas.
Primero que nada. Para crear un PDF en Python usaremos la biblioteca PyPDF. Para instalar este complemento de Python si no es que no lo tenés, en la consola tipeamos lo siguiente:

```

pip install fpdf

```

El código: 

```

from fpdf import FPDF
import random

total = 170
altura_inicial = 20

cuadro1_a = random.randrange (30, 130)
cuadro1_b = total - cuadro1_a
espacio1 = cuadro1_a + 20
alto1 = random.randrange (50, 90)

distancia_con_1 = alto1 + altura_inicial + 5
cuadro2_a = random.randrange (30, 130)
cuadro2_b = total - cuadro2_a
espacio2 = cuadro2_a + 20
alto2 = random.randrange (50, 90)

distancia_con_2 = distancia_con_1 + alto2 + 5 
cuadro3_a = random.randrange (30, 130)
cuadro3_b = total - cuadro3_a
espacio3 = cuadro3_a + 20
alto3 = random.randrange (50, 90)



pdf = FPDF()
pdf.add_page()
pdf.set_font('Arial', 'B', 16)

#FILA 1 de cuadros
pdf.rect(x = 15, y = altura_inicial, w = cuadro1_a, h = alto1, style = '')
pdf.rect(x = espacio1, y = altura_inicial, w = cuadro1_b, h = alto1, style = '')

#FILA 2 de cuadros
pdf.rect(x = 15, y = distancia_con_1, w = cuadro2_a, h = alto2, style = '')
pdf.rect(x = espacio2, y = distancia_con_1, w = cuadro2_b, h = alto2, style = '')

#FILA 3 de cuadros
pdf.rect(x = 15, y = distancia_con_2, w = cuadro3_a, h = alto3, style = '')
pdf.rect(x = espacio3, y = distancia_con_2, w = cuadro3_b, h = alto3, style = '')

pdf.output('generador_de_historietas.pdf', 'F')


```


He aquí un ejemplo de lo que genera:

!["Ejemplo Cuadro"](../../images/cuadros_historietas.jpg)


Aquí el script para descargar:

["Script Generador de Cuadros"](https://gitlab.com/enlacepilar/scripts-de-python/-/raw/main/23-generador_de_%20cuadros_historietas.py?ref_type=heads&inline=false)

Aquí un ejemplo en PDF de lo que genera:

["Ejemplo cuadros en PDF"](https://gitlab.com/enlacepilar/scripts-de-python/-/raw/main/generador_de_historietas.pdf?ref_type=heads&inline=false)

De más está decir que esto puede modificarse a tu gusto, quitar líneas, filas, cuadros, etc. ¡A dibujar!

```
25-09-2023:
```


Estuve haciendo unas modificaciones sustanciales que están reflejadas en el script número 24 de esta serie para la posteridad, de valor casi de oferta.

Se trata de lo siguiente: he creado una función (suena lógico) para que te pregunte el número de páginas que querés crear con los cuadros aleatorios. De este modo estás en el trabajo y le usas la impresora al jefe y le mandás, ponele, 50 páginas de cuadritos. Eso sí hacelo a la mañana bien temprano para que no te vean. Y a dibujar sin los cánones establecidos, lo que salga, que todo es arte. Dejenme de joder con que no puedo hacer lo que quiero en el dibujo, jajajaj re enojado el flaco.

["Script Generador de Cuadros"](https://gitlab.com/enlacepilar/scripts-de-python/-/raw/main/24-generador_de_%20cuadros_historietas-con%20paginas-.py?ref_type=heads&inline=false)


