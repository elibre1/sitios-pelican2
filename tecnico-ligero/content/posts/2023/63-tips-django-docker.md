Title: Tip para Desplegar con Dockerfile un sitio de Django con base externa
Date: 2023-10-02 19:13
Autor: Hugovksy 
Slug: desplegar-django-docker
Lang: es
Tags: django, docker, puertos
Category: Django
Summary: Estuve renegando miles de horas para poder desplegar un sitio web con Django a través de un Dockerfile y un Nginx con proxy_pass. Aquí algunas notillas interesantes... 
Imagen: /images/fotoNASA-7134.jpg

Cuando vas a desplegar Django tenés 2 caminos posibles (o por lo menos eso es lo que descrubrí hasta ahora yo): 

1) /var/www/html/tu-sitio, creando un servicio para que levante Gunicorn, exportando el static fuera de la carpeta y toda esa sarasa que tengo en otra publicación 

2) Dockerizar y olvidarte de toda esa mierda de arriba.

Vamos por el 2 en este caso, del lado amable. 
No fue fácil llegar a verlo reflejado con dominio y todo, puteé como un condenado.

* Clonamos nuestro repo. Has ahí igual que como si desplegáramos sin dockerizar.

```

git clone nombre-del-repo.git

```

* Asegurarse de que los requerimientos y las direcciones a la base apunten correctamente, es decir preferentemente afuera del futuro contenedor.

En mi caso poseo un archivo ***.env*** que oficia de guardador de claves en el repo de django. Si te clonás este repo, tenés uno de muestra en la carpeta elibre:


[Repo elibre -django](https://gitlab.com/enlacepilar/elibre-generador-de-blogs-en-django-esta-vez.git)


* Si es necesario hay que darle derechos de acceso a la base mysql

```
sudo mysql
CREATE user 'usuario'@'%' identified by 'clave';
GRANT all privileges on base_de_datos.* to 'usuario'@'%';
FLLUSH privileges;

```

* Creamos nuestro Dockerfile en la raiz del proyecto:

```

# set the base image 
FROM python:3.10

# File Author / Maintainer
MAINTAINER Hugue

#add project files to the usr/src/app folder
ADD . /usr/src/app

#set directoty where CMD will execute 
WORKDIR /usr/src/app

COPY requerimientos.txt ./

# Get pip to download and install requirements:
RUN pip install --no-cache-dir -r requerimientos.txt

CMD ["gunicorn", "nombre_de_la_app_django.wsgi:application", "--bind", "0.0.0.0:5001", "--workers", "3"]

EXPOSE 5001

```

* Creamos el contenedor (debemos posicionarnos en la raiz del proyecto, donde creamos el Dockerfile):

```
docker build --tag nombre_de_la_imagen:latest .

```

* Levantamos el contenedor (basado en la imagen que acabamos de crear con el Dockerfile):

```
docker run --name nombre-contenedor --restart=always -d -p 5001:5001 nombre_de_la_imagen:latest
```

* Creamos el archivo para nginx en /etc/nginx/sites-available/nombre_de_archivo:

```
server
{
	listen 82;
	server_name 'nombre del sitio';

	location / {
		include proxy_params;
		proxy_pass http://192.168.0.145:5001/;
	}
}

```


* Creamos en enlace simbolico:

```
sudo ln -s /etc/nginx/sites-available/nombre_de_archivo /etc/nginx/sites-enabled/
```

* Reiniciamos nginx

```
sudo service nginx reload

```

* Verificamos si Nginx está funcionando bien y no hay errores:

```
sudo service nginx status

```

Si llegase a haber errores te los va a marca y aparece un error tremendo **failed** XD


* Probamos si levanta en el puerto 82 como en el ejemplo, que va a derivar a la 192.168.0.145 con puerto 5001, que es el del contenedor corriendo.

En la barra de direcciones de una lan:
```
192.168.0.145:5001
```

Todo debería estar funcionando de pelos.

Después podemos poner un dominio, usar certbot y esas cosas. Pero eso, amigo, eso es otra historia.

