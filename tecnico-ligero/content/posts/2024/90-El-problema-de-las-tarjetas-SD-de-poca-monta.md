Title: El problema de las tarjetas SD de poca monta
Date: 2024-10-14 20:27:10.658235
Autor: Hugovksy 
Slug: El-problema-de-las-tarjetas-SD-de-poca-monta
Lang: es
Tags: Tarjetas, SD, Poca monta
Category: Tarjetas
Summary: Nada más lejos de un buen resultado que comprar una tarjeta SD barata o las peripecias del técnico ligero lidiando con una SD de porquería  
Imagen: /images/SanBer12-24-mini.jpg
    

El tema es nos acaece en esta oportunidad es no mezquinar dinero con las tarjetas SD porque a la larga (mejor dicho, a la corta) dan problemas. 
Sobre todo si el técnico ligero quiere cambiar por una de mayor espacio en la Raspberry Pi Zero 2W, que tenía una humilde de 16 GB de la generación de mi abuelita.
Es por ello que me dispuse a comprar una y le dije al tendero/a: "Deme una de 32". A lo que éste respondió: la buena te sale 17 y la punga, 8.5. Venga la trucha.

Me fui a casa y ya empezaron los problemas. No arrancaba la imagen del Raspbian, no formateaba decentemente. Probé todo, inclusive el windows del trabajo, ya un caso extremo para un amante del software libre, al menos en mi vida privada.
Cuestión es que hube de cambiarla y el tendero/a me recomendó pasarme a una de marca, a lo que me rehusé dignamente. Voy a darle una oportunidad a otra... de la misma marca.
Al principio, nuevamente en casa, todo bien. Me permitió instalar pero luego de algunos procesos el sistema empezó a colapsar y terminó nuevamente por no arrancar.
Moraleja: equipo que gana no se toca y dejen todo como está. La SD la usan para el celu o para guardar música y otro día compramos una como la gente.


<div class='text-center'><img src='/images/SanBer12-24-mini.jpg' alt='foto' class='img-fluid'></div>