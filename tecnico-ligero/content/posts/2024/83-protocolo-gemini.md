Title: Probando las cápsulas Gemini
Date: 2024-06-11 17:39
Autor: Hugovksy 
Slug: protocolo-gemini
Lang: es
Tags: Protocolo, Gemini
Category: Gemini
Summary: Experimentando con los sitios a los cuales se acceden a través del protocolo Gemini, y mi propia cápsula...  
Imagen: theme/images/sin-imagen.png

Hace rato, quiás desde que comencé la Tecnicatura en Software Libre, donde profundicé un poco mi intención de inclinarme hacia el lado del softwaree que cumpla con las 4 libertades, que empecé a frecuentar sitios web no tan, digamos, engordados, y poco comunes. 


La realidad y sin tanto preámbulo que me cansó es que no me gustan las páginas llenas de publicidad, no se puede leer nada, no me quiero suscribir, odio que lo único que quieran es hacer dinero. Los contenidos han perdido calidad, se han transformado en muchos casos en meros rellenos de las publicidades y de esas mierdecillas atómicas sin sentido: ventanas, suscripciones, y GAFAM, claro, siempre las empresas detrás sitiándolo todo.

Buscando sitios más minimalistas di con [texto-plano.xyz](https://texto-plano.xyz), y tras esto con lso hoyos Gopher, cosa que ni siquiera había tenido oportunidad de navegar, aunque de oídas había escuchado el protocolo, pero no mucho más... luego con los clubes Tilde, consolas locas que va, tampoco pienso poner enlaces de todo, se van a cagar, investiguen, esto es un sitio ligero, luego con el protocolo Gemini, que vendría a ser como la evolución del Gopher, pero más seguro y más actual. Lo hizo un chango que seguro amaba Gopher pero quiso crear algo ahora.

La cuestión con toda esa cosa es que me inscribí en cuanto tilde club encontré y si bien algunos me dieron instrucciones de acceso, nunca pude acceder porque soy un engendro y porque tampoco le pusieron mucho entusiasmo para ayudarme, así que me dije: Hugo, es hora de hacer tu propia cápsula Gemini en tu Raspberry multipropósitos... pero que nunca pudo ser PC diaria, eso no, pero es otra historia (maldito Hdmi)

Entonces fue que me puse a buscar navegadores primero, para ver qué diferencia había... y cuál va a haber, que en vez de poner Https:// ponés Gemini:// y los sitios terminan en extensión gmi. El único que usé fue el navegador Lagrange, es un appimage, como un portable de windows, pero del lado debian.

Después de eso, claro, cómo monto un servidor. Había uno que era molly-brown, como la del titanic, pero ese no lo pude instalar en la Raspberry asi que busqué algo que pudiera manejar, Python, que es fácil y ligero, como un servidor, pero para aprender, no que yo soy fácil, querés pelear, queés pelear? peazo de gato, peazo e'bigote?

Di con uno llamado Jeforce. Tiene un repo en github o se instala

```
pip install jetforce
```

Después registras tu subdominio, yo tengo el sitio en nic.ar y los Dns con dnsafraid, puedo crear todos los que quiera. Después con Nginx tenés que apuntar al puerto local 1965, que es el que usa gemini. Lo puse como decía el tuto en /var/gemini y le di derechos.
Tengo certbot instalado, porque necesita que los sitios sí o sí sean seguros, así que lo aseguré 

```
sudo certbot --nginx
```

Seleccionás el tuyo y ya. Ahora con esas direcciones locales que te da certbot, las copiás y pegas en el servicio jetforce, lo habilitás y después lo iniciás.

No me anduvo de una, desde ya... puteé un poco pero después probé sacando una cosa acá, otra allá y anduvo.
Ahora tengo una muestra pedorra pero que anda, hasta que le ponga contenido en 

```
gemini://tecnico-ligero.enlacepilar.com.ar
```
Que es este mismo sitio, claro. No sé si lo voy a replicar o le voy a agregar contenido un tanto diferente, o un poco de ambos.

El lenguaje de Gemini o GemText es parecido al Markdown pero más sencillo

=> para enlaces 
el > para citas

[En este sitio hay una wiki con lo elemental](https://communitywiki.org/wiki/GemText)


almohadillas para titulos. No pongo porque estoy escribiendo esto en Markdown. Obviamente se debe poder poner si ponés algo pero ahora no tengo ganas de buscar. Ah también en el hoyo Gopher de Peron leí unos fanzines muy locos que me hizo acordar cuando tenía la XT y quería hacer un juego en Basic. 
