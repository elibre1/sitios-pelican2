Title: El que camino este camino, viendo el camino que camina. No puedo ser otro más que yo.
Date: 2024-02-13 11:54:20.070398
Autor: Hugovksy 
Slug: sigo-mi-camino
Lang: es
Tags: Escribo, Mi Camino
Category: Escribo
Summary: Un ensayo del camino donde quiera que éste se encuentre o unos laberintos o sueños que qué son, son sueños o vivencias de qué escape de mi alma...   
Imagen: /images/vuelta-de-obligado-mini.jpg



<img src='/images/01-futalaufquen-mini.jpg' alt='Foto del interior' srcset='' class='img-fluid'>

En donde veo que está reflejado algún vestigio de otra personalidad es cuando sueño y vuelvo a la vida completamente contrariado. Extraño de mi ser diría yo. Vivo cosas que de verdad parecen salidas del inconsciente de un trastornado.
Pasa a veces cuando menos lo espero, como diría Elige tu Propia Aventura, algunos son mundos de fantasía, otros el mismísimo infierno... En este caso son caminos que me derivan infinitamente. Pero cuando vuelvo a despertar, en el presente
de una cotidianeidad más, a pesar de la contrariedad todo se clarifica o al menos esto es, decidir con más ímpetu que mi camino es un decisión ferviente no apta para blandengues o inclonclusos eternos. Es algo, cómo decirlo, algo que se tiene que hacer
sin dar mayores vueltas y con fervor. Algo tengo en claro: no vine a este mundo a hacer plata, como bien dijo Urquía, el poeta sanfernandino, en alguna de sus frases. Este camino de tecnología antiobsolescente y literatura y embrollo marcial
y antenas y ensayos sobre minorías aborrecidas y despreciadas y literatura fantástica (pero bien escrita por favor) es algo de lo que sale algo pero no lo puedo clarificar con palabras, sólo soy consciente de que tengo que caminar y caminar.
