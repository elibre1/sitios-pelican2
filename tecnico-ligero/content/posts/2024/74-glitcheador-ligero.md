Title: Glitcheado Ligero
Date: 2024-01-10 12:50:17.835319
Autor: Hugovksy 
Slug: glitcheador-ligero
Lang: es
Tags: Glitch, Python, Scripts, Desctructor
Category: Glitch
Summary: Un breve paseo por el destructor de imágenes ligero que estuve creando para eso mismo, generar fallos en mis fotos por puro arte digamos...  
Imagen: /images/glitched_01-en_8-mini.jpg


Un técnico ligero incansable es quien siempre tiene la mente ocupada en código y cómo hacer esas bolucedes que en vacaciones se pueden programar. He ahí el porqué de las vacaciones.

En este caso que nos ocupa lo que estuve puliendo o dando forma es al glitcheador ligero, pero... ¿de qué estoy hablando concretamente? 
Y para ello debemos partir del principio: ¿Qué sería la técnica Gltich?
Para ello podemos remitirnos a [Cultura Colectiva](https://culturacolectiva.com/arte/que-es-el-glitch-art/), donde encontramos una interesante definición, mejor que la que pude encontrar en Wikipedia, que nos reza lo siguiente:


```
El Glitch Art es un estilo que recicla las fallas técnicas y las convierte en arte. 
Con imágenes borrosas, alteradas por la estática o por errores de código, 
nos salta al ojo esta peculiar corriente que convierte las imperfecciones caóticas digitales en obras de arte increíbles. 
La definición de glitch se atribuye al astronauta John Glenn, quien en 1965 lo definió 
como “un pico o cambio de voltaje de una corriente eléctrica

El Glitch Art nos da como resultado una versión corrupta de la transmisión de la información que resulta ligera o totalmente diferente al mensaje original. 
Las raíces nos recuerdan a la ideología surrealista y nos trasladan a un arte pop de vanguardia que busca dar un valor agregado a partir del error. 
Obliga al artista a improvisar para generar una pieza extruida del caos.
```
<br>

Entonces podríamos decir que con una foto podríamos generar nuevo arte "destrozando" la misma. 
Para ello me valgo de mi amado Python y un script que estuve haciendo para automatizar varios procesos.
Es interesante también hacerlo con Audacity como si de un efecto de sonido se tratara, pero esa es otra historia.

<br>

Partimos de esta imagen:

<div class='text-center'>
<img src='/images/01-mini.jpg' alt='foto' class='img-fluid'>
</div>

<br>

Y aplicándole un efecto Nro 2 del complemento **Gltich-this** de Python obtenemos esto:


<div class='text-center'>
<img src='/images/glitched_01-en_2-mini.jpg' alt='foto' class='img-fluid'>
</div>

<br><br>

Vemos que no hay grandes cambios. Pero con un efecto más potente, como el 5, podemos ver que la cosa va cambiando:


<div class='text-center'>
<img src='/images/glitched_01-en_5-mini.jpg' alt='foto' class='img-fluid'>
</div>

<br><br>

Y finalmente con 8, este resultado: 

<div class='text-center'>
<img src='/images/glitched_01-en_8-mini.jpg' alt='foto' class='img-fluid'>
</div>

<br><br>

El script completo se puede encontrar, obviamente en mi repo de Gitlab que titulo  <a href='https://gitlab.com/enlacepilar/scripts-de-python/-/tree/main/Glitcheando?ref_type=heads' target='_blank'>**"El camino del Script"**</a> 

Recomiendo descargar los 2 archivos que hay en ese directorio: 
El glitcheador y el de requerimientos. 

Por el momento funciona solo con imágenes jpg. Lo que hace es lo siguiente. Primero corremos el scritp con **python3 Glitcheador-Hugovksy.py**:


* Te pide el nombre de una imagen. Le ponemos la que tengamos en la misma ruta, en este caso ***01.jpg***. El nombre con la extensión es importante.

* Te solicita luego cuántas copias para el glitch vas a necesitar, podemos poner, por ejemplo 5, lo cual va a generar efectos Agudeza(Sharpness), Posterización (Posterize) y solarizado (Solarize).

* Con esos datos va a crear un gif animado y posteriormente, un gif glitcheado de ese GIF (glitched-nombre-de-gif.gif)

<br>
El resultado obtenido, con un glitch de 2 es este:

<div class='text-center'>
<img src='/images/obligadoglitch-2.gif' alt='foto' class='img-fluid'>
</div>

  