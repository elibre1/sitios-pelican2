Title: Opinando sobre la trampa de Javascript
Date: 2024-06-16 20:11
Autor: Hugovksy 
Slug: opinando-sobre-la-trampa-de-javascript
Lang: es
Tags: Javascript, Lectura, GNU
Category: Lectura
Summary: Recorramos un poco la lectura del artículo de Richard Stallman en la página de GNU, y también rememoramos algo de la web de antaño...    
Imagen: /images/fotoNASA-7134.jpg

Recomiendo para todos los que nos preguntamos qué paso con la web, esta lectura para su análisis:

[La trampa de Javascript](https://www.gnu.org/philosophy/javascript-trap.es.html)

Es muy interesante lo que escribe Stallman sobre la tranformación de un simple elemento para hacer sencillas cositas ornamentales dentro del sitio a un mounstruo que te come toda la información en forma silenciosa y puede hacer lo que se le antoje, si no lo bloqueás. 

Para ello es que han salido alternativas como el protocolo Gemini con sus cápsulas, del cual hablé antes de este artículo, una alternativa a esta web que se ha transformado es algo lleno de publicidad, lleno de suscripciones, lleno de gente que solo quiere hacer plata, de contenidos pobres, de empresas asquerosamente ricas, llenas de guita que controlan todo, dirigen y dicen dónde va a ir la web.

Tengo 45 años al momento de escribir estas líneas. Me acuerdo en 1996 cuando vi por primera vez, luego de las BBS, una página web de un diario en una Pentium I creo (no recuerdo si era eso o 586), tenía una HP, y quedé enloquecido, era el comienzo de la era digital para mí, del mundo de internet... al menos en ese momento y por unos cuantos años por teléfono y limitado, claro.


Era fácil encontrar cosas, era fácil posicionarte con tu sitio, de hecho tenía uno que trataba de ayudar a bandas a encontrar músicos que le faltaban. Se llamaba Under-Rock, la página del músico Under, jaja todo un nombre, no. Pero se me había llenado de gente. Prácticamente mi pasatiempo era ese, pero no como para entrar en detalles, si no para mencionar que era una simple página hecha con Frontpage (sí, en ese tiempo usaba software privativo, y por mucho tiempo hasta que entendí el camino del Software Libre). Google años después era un buscador bastanten decente... pero yo prefería Yahoo, un directorio. Siempre me gustaron los directorios... Después se fue todo a la mierda. Creo que fue con Facebook, o por lo menos lo analizo de ese modo. 

Mucha gente se quiso salvar haciendo páginas después del bum ese de los dominios en el 2000, todo empezó a engordar, a requerir más y más para leer un texto. ¿Tan complicado puede ser? De todos modos, Javascript no es culpable de la decadencia de la web, pero sí que se transformó en un arma que sirve para robar info al desprevenido o al que desconoce.

Volviendo a nuestra lectura en cuestión, un pasaje nos reza:

```

El propio lenguaje de JavaScript, en cuanto formato, es libre, y el uso de JavaScript en un sitio web no es necesariamente algo malo. Sin embargo, como hemos visto antes, sí puede ser malo, en el casos de que el programa de JavaScript no sea libre

```

y más adelante:

```

La carga y ejecución silenciosa de programas que no son libres es uno de los diversos problemas que presentan las «aplicaciones web». La expresión «aplicación web» se acuñó para obviar la distinción fundamental entre el software que se entrega a los usuarios y el que se ejecuta en un servidor.

```

Un alert saludando no puede representar problema alguno. Si bien mencionan que no es trivial si modifica el DOM, cambiar algunas cosillas mínimas tampoco es grave. El tema es la intención: todo pasa por la intención. 
Y como soy extremo, me dispuse a hacer mi propia prueba de prescidir de JS para ver si puedo sobrevivir. Creo que no es tan grave para el que solo busca leer contenido, ¿no?.
