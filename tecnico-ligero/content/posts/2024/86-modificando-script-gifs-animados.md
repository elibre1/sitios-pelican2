Title: Modificando el creador de Gifs Animados por scripts de Python
Date: 2024-08-11 17:51:41.046219
Autor: Hugovksy 
Slug: modificando-script-gifs-animados
Lang: es
Tags: Scripts, Python, Gifs
Category: Scripts
Summary: Meto mano a un script que tenía con el cual creo un Gif animado a partir de un manojo de JPGs, como buen incansable técnico ligero que soy  
Imagen: /images//mate_y_naturaleza-mini.jpg


Bien, sin preámbulos vamos a por las modificaciones:

```
from tkinter import Tk
from tkinter.filedialog import askdirectory

ImageFile.LOAD_TRUNCATED_IMAGES = True

ruta = os.getcwd()

# Oculta la ventana principal de Tkinter
Tk().withdraw()

pregunta = input ("¿Querés seleccionar la carpeta con entorno gráfico? - S/N: " )

if pregunta == 's' or pregunta == "S":

    # Abre el diálogo de selección de carpeta
    ruta = askdirectory(title="Selecciona la carpeta con las imágenes")

    if not ruta:
        print("No se seleccionó ninguna carpeta. El script se cerrará.")
        exit()
elif pregunta == 'n' or pregunta == 'N':
    ruta = input ("Escribí la ruta a mano (absoluta. Ej. /home/enlacepilar/Imagenes): \n")
else:
    print ("No es una opción correcta. Salgo del programa.")
    exit()
```

En este caso lo que hice al script original fue ofrecerle al usuario la posibilidad tipear la ruta o, en el caso de que se encuentre en un entorno gráfico, 
que seleccione el directorio con unos simples clics.

Este es el script completo

```
#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PIL import Image, ImageOps, ImageFile
import os
from tkinter import Tk
from tkinter.filedialog import askdirectory

ImageFile.LOAD_TRUNCATED_IMAGES = True

ruta = os.getcwd()

# Oculta la ventana principal de Tkinter
Tk().withdraw()

pregunta = input ("¿Querés seleccionar la carpeta con entorno gráfico? - S/N: " )

if pregunta == 's' or pregunta == "S":

    # Abre el diálogo de selección de carpeta
    ruta = askdirectory(title="Selecciona la carpeta con las imágenes")

    if not ruta:
        print("No se seleccionó ninguna carpeta. El script se cerrará.")
        exit()
elif pregunta == 'n' or pregunta == 'N':
    ruta = input ("Escribí la ruta a mano (absoluta. Ej. /home/enlacepilar/Imagenes): \n")
else:
    print ("No es una opción correcta. Salgo del programa.")
    exit()
    
imagenes = []

print ("Creando Gif...")

for raiz, directorio, archivos in os.walk(ruta):
    for archivo in archivos:
        if archivo.endswith ('jpg'):
            ruta_completa = os.path.join(raiz, archivo)
            im = Image.open(ruta_completa)
            im.thumbnail((600,300), Image.LANCZOS)
            im = ImageOps.equalize(im, mask = None)
            #im= ImageOps.solarize(im, threshold=8)
            im= ImageOps.posterize(im, 5)
           
            imagenes.append(im)
            

imagenes[0].save(
            'zz-animacion.gif',
            save_all=True,
            append_images=imagenes[1:],
            duration=800,
            loop=0)
print ("Gif creado")
```

Y se puede descargar desde aquí, en el repo bien llamado **<a href="https://gitlab.com/enlacepilar/scripts-de-python/">El camino del Script</a>**

<a href="https://gitlab.com/enlacepilar/scripts-de-python/-/raw/main/25%20-%20crea%20gifs.py?ref_type=heads&inline=false">Descarga</a>


He aquí un ejemplo:

<img src="https://gitlab.com/enlacepilar/scripts-de-python/-/raw/main/zz-animacion-prueba.gif?ref_type=heads" alt="Gif de prueba" srcset="">