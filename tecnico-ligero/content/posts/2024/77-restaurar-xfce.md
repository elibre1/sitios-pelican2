Title: Restaurar panel XFCE
Date: 2024-03-10 17:41:26.029914
Autor: Hugovksy 
Slug: restaurar-xfce
Lang: es
Tags: GNU/Linux, XFCE
Category: XFCE
Summary: Comando para restaurar el panel XFCE cuando por error desaparece  
Imagen: /theme/images/sin-imagen.png


Por motivos desconocidos, en algunas oportunidades, 
cuando borramos con el mouse algún elemento de la barra de tareas de 
nuestro entorno XFCE, por arte de magia se borra toda la barra :P

Es nuestro deber como técnicos ligeros tener este comando a la mano. 
Para ello debemos hacernos con la consola. Es decir, en mi caso y bien ligero, voy 
a la carpeta principal porque tengo un acceso en el escritorio y de ahí,
botón derecho, **abrir terminal aquí**.

Luego tipeamos:


```
xfce4-panel
```

Y nuevamente aparecerá la maldita barra desaparecedora.
