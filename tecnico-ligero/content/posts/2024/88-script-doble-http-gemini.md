Title: Probando script para escribir en forma dual el blog HTTP-Gemini
Date: 2024-08-31 15:07:12.735015
Autor: Hugovksy 
Slug: script-doble-http-gemini
Lang: es
Tags: Scripts, Pelican, Gemini, Blog
Category: Scripts
Summary: Basta de copiar y pegar como engendro me dije, he de evolucionar y hacer un script a lo amigo de Narkoz que automatice todo...  
Imagen: /images/olivera01-mini.jpg
    

Basta de copiar y pegar como engendro me dije, he de evolucionar y hacer un script a lo amigo de Narkoz que automatice todo...

Es por ello, que un técnico ligero que se precie ha de hacer scripts por doquier para automatizar todo lo que lleve más de 20 seg de su tiempo, dijo y he de tomarlo como un lema, por ello aquí un script que lo que hace pácticamente es lo siguiente:

1) Escribo en el gemlog (lo más sencilo porque es escribir). Hace unas generaciones de archivos y mierdecillas internas para mostrar las publicaciones al dia.

2) Con la variable del texto que guardo, la mando a una función para generar entonces el archivo markdown del sitio pelican, junto con el año como variable para que vaya al directorio indicado.

3) Ahí me pregunta una serie de otras mierdecillas para generar la página y como acto final...

4) Genera un sh (script de terminal de GNU-Linux, sería como más te guste) con las copas para hacer por VPN a las rutas indicadas, sumadas a unas sentencias mágicas que abren el envPelican (envuelve un entorno de Python) y voilà todo publicado.

No sé si esta engendrada podrá llegar a servirle a alguien más, solo me la entiendo yo... pero la pondré en "El camino del Scritp". Buscalo en la web principal.


<div class='text-center'>
<img src='/images/olivera01.jpg' alt='foto' class='img-fluid'>
</div>