Title: Modificando tema Geliba sin Javascript
Date: 2024-06-11 18:06
Autor: Hugovksy 
Slug: bifurcacion-tema-geliba
Lang: es
Tags: Bifurcacion, Tema, Geliba
Category: Tema
Summary: Estoy rediseñando mi tema Geliba para sitios estáticos Pelican, pero esta vez prescindiendo de Javascript, espero que para siempre.  
Imagen: theme/images/sin-imagen.png


Voy a ser sincero: siempre odié Javascript. Para mí es como un mal necesario. No soy el paladín del Software Libre o talibán que solo usa eso, porque en el laburo tengo que usar privativo, windows que va, y mantengo una web local de formularios que tiene mas carga que la mismísima miércoles y llna de datatables, select2, animate y muchas otras mierdas salvajes, pero sin publicidad, pero igual de gorda. 

Después de leer lo que dijo RMS que también odia JS no me sentí tan solo en este desprecio: ojo lo asincrónico con Async y Await está bueno, manejar el Dom con cositas estaba bueno antaño, conocerlo cómo funciona también está bueno, lo que no está es que capturen tu info porque sí para venderte cosas, registrar las en silencio, sin que sepas, que haya miles de códigos, engendradas y demases, cosas locas que va...

Recomiendo leer esta nota de la FSF, es genial sobre este tema:
[https://www.gnu.org/philosophy/javascript-trap.es.html](https://www.gnu.org/philosophy/javascript-trap.es.html)

Resumen nivel 5 de lo que me interesa o estaba hablando:

```
al principio se usaba para pequeños detalles ornamentales en páginas web, tales como bonitas pero innecesarias características de navegación y maquetación. Era aceptable considerarlos como meras extensiones del lenguaje de etiquetas HTML más que como verdadero software, y despreocuparse del asunto.

```

Bueno, de una vez por todas me decidí y empecé de a poquito, al menos en este sitio, que vendría a ser como una especie de conejillo de indias de todo lo que puedo probar, escribir, asentar, puntapié inicial, etc.

Veremos qué sucede más adelante. Hay muchas cosas que hice con JS que podría empezar a eliminar...
