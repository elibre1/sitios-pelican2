Title: Cómo poner el grupo público en Gitlab
Date: 2024-05-10 19:11:46.685894
Autor: Hugovksy 
Slug: grupo-publico-gitlab
Lang: es
Tags: Tutorial, Repositorio, Gitlab, Peertube
Category: Tutoriales
Summary: Breve explicación en video para poner el grupo en modo público en Gitlab, para poder clonar repos libremente sin claves  
Imagen: theme/images/sin-imagen.png


Video generado para la materia Tecnologías Web, de la Tecnicatura Universitaria en Software Libre, de la Universidad Nacional del Litoral, provincia de Santa Fe.  


<div class="text-center mb-4">
	<iframe src='https://media.taulamet.ar/videos/1WgrmblBZGp9NXYEdrqlyO6JQb2Pm07aSBwg4RL8je3ok5AnVavW1xD7zORGjNQ5' allowfullscreen='' width='580' height='390'></iframe>
</div>

