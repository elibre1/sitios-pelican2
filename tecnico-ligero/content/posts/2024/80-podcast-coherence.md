Title: Charlas de Telegram 01 con Towers - Coherence
Date: 2024-03-20 21:00
Author: Hugovksy
Slug: podcast-coherence
Status: published
Category: Podcast
Summary: Inauguramos esta sección de charlas en Telegram con Coherence, peli de 2013 de ciencia ficción muy interesante... pasen y escuchen.

<br>
<hr>

20 de marzo 2024

###01 - Charla sobre Coherence
Arrancamos esta sección con unas charlas de telegram después pegadas a loco (pero con Software Libre). En este caso que nos toca, empezamos con una recomendación de Towers, Cocherence, una peli de 2013 dirigida en ese entonces por el debutante James Ward Byrkit.

<audio controls>
    <source src="/images/charlas_telegram/01-charla-coherence.ogg" type="audio/ogg">
        Tu navegador no soporta audio HTML5.
</audio>
