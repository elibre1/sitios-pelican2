Title: Copiando un directorio con SCP a pura terminal
Date: 2024-09-01 19:51:07.303551
Autor: Hugovksy 
Slug: Copiando-un-directorio-con-SCP-a-pura-terminal
Lang: es
Tags: SCP, Terminal, Gnu-Linux
Category: Termina
Summary: Los pasos infalibles para copiar tu directorio y no perderlo en la red bajo ningún concepto...  
Imagen: /theme/images/sin-imagen.png
    

Los pasos infalibles para copiar tu directorio y no perderlo en la red bajo ningún concepto...

Eso es... Estoy trabajando en mi script automatizador HTTP-Gemini todo de una, y como quiero escribiry publicar a través de la VPN hube de hacer cosas de último momento, es decir, unir los dos scripts, pero en algún momento surge terminar esto en el laburo... asi que ¿pendrive para llevar los documentos? De ninguna manera. 
Un técnico ligero que se precie averigua sentencias en el insondable mundo del saber, porque para copiar archivos vaya y pase, pero para copiar directorios enteros, hemos de recurrir a pequeñas modificaciones en lo que vamos a escribir:

```
scp -r directorio_local/ usuario_remoto@10.8.0.2:/home/usuario_remoto/Escritorio/
``` 

La letra r en mínusculas implica la lectura del directorio en cuestión. Lo que sigue después es la dirección donde vamos a copiar dentro de la lan creada previamente por VPN. 

