Title: Generando un modo kiosco en la raspberry (que ande)
Date: 2024-12-02 19:17:10.389433
Autor: Hugovksy 
Slug: Generando-un-modo-kiosco-en-la-raspberry-(que-ande)
Lang: es
Tags: Rasbperry, Modo Kiosco, Clima
Category: Raspberry
Summary: Unos pasos legendarios para poder generar una aplicación web de clima en modo kiosco en una Raspberry   
Imagen: /images/el-clima-raspberry-mini.jpg
    

Estuve un tiempo largo intentando generarme una estación meteorológica alterntariva con la 4 Model B porque tenía una que habia comprado, esas que vienen con el sensor 
afuera como complemento, que por diversos motivos de buenas a primeras no anduvo más, asi que hube de impriovisar. 

Compré pantalla lcd de 7 y me lancé a la aventura... 
En primer lugar tenía que conseguir un sitio que me permitiera extraer los datos del clima de mi ciudad. 

Para ello fue que terminé dando con este que es el más decente: http://openweathermap.org

Tenés que registrarte y podés hacer bastantes consultas por hora. Bien ya teníamos el sitio donde servirnos.


Seguí por cómo lo iba a desarrollar, bueno, para hacerlo más retorcido fue Django, aunque no era necesario tanto, este backend de Python me encanta.
Para mostrar, luego de experimentar con los datos y ayudado por IA mostré la temperatrura, la humedad y con JS la hora. 
Esta página se refresca cada media hora para obtener datos actualizados


## El modo kiosco.

Para esto hice un script de inicio que con cada reinicio esperase unos segundos y arrancase. Igualmente tuve que valerme nuevamente de IA para solucionar algunas cosas (no me andaba el reboot):


```
 @reboot export DISPLAY=:0 && export XAUTHORITY=/home/enlacepilar/.Xauthority && /ruta/completa/iniciar_clima.sh
```

Luego, en el script propiamente dicho, le mando un sleep 10 y lanzo (luego de configurar como es debido el ambiente que no es de producción como buen ligero que soy y envolver el sitio):


```
chromium-browser --kiosk --disable-restore-session-state --app='http://localhost:8000'
```


<div class='text-center'><img src='/images/el-clima-raspberry.jpg' alt='foto' class='img-fluid'></div>