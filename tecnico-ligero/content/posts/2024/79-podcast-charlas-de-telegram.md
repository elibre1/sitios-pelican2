Title: Podcast Charlas de Telgram
Date: 2024-03-20 20:59
Autor: Hugovksy 
Slug: nueva-seccion-podcast
Lang: es
Tags: Podcast, Telegram, Software Libre
Category: Podscast
Summary: Nueva sección alocada con charlas sobre pelis (en principio) con mi colega Towers. Ojo no va a ser Titanic, capaz alguna para pensar, no sé   
Imagen: /images/mate_y_naturaleza-mini.jpg

En <a href="https://tecnico-ligero.enlacepilar.com.ar/posts/2024/podcast-coherence.html" target="_blank">esta sección de podcast</a> recientemente inaugurada por un servidor alojaré algun audillo grabado y emparchado con alguna música robada de por ahí, eso sí, sin derechos de autor, con la idea de ambientar alguna charla de vez en cuando que tengo con el bueno de Towers. 


Veníamos hablando muchas veces sobre algunas impresiones y me dije, qué tal de pegar todo eso en el Audacity con con un poco de reverberación y darle un toque edulcorado a lo Aspen con músiquilla del tres al cuarto y dale... salió eso.

