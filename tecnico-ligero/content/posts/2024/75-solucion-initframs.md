Title: Qué pasa cuando aparece ese initframs horrible en Ubuntu o Mint
Date: 2024-02-03 12:41:52.492396
Autor: Hugovksy 
Slug: solucion-initframs
Lang: es
Tags: initframs, terminal, gnu-linux, soluciones ligeras
Category: initframs
Summary: Una solución efectiva cuando sos medio boludo y apagás mal la PC, o te cortan la luz, o un amigo se pone como loco al dale que te dale al botón e apagado  
Imagen: /images/lacar-mil-mini.jpg


Para recordar siempre es bueno. Cuando aparece el initframs tipeamos:

```
fsck -c -y /dev/sda1
```

Nota: si no es sda1, probá con sda2, puede ser que te marque que no hay errores. Despues exit y listo, a gozar.
