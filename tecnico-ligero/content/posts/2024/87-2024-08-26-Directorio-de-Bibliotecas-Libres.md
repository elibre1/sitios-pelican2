Title: Proyecto Directorio de Bibliotecas Libres
Date: 2024-08-26 11:35
Autor: Hugovksy 
Slug: directorio-de-bibliotecas-libres
Lang: es
Tags: Proyectos, Django, Python, Libros, Bibliotecas
Category: Proyectos
Summary: Nuevo proyecto hecho en Django que intenta albergar en un directorio un listado de bibliotecas libres que andan dando vuelta internet. ¡Agregá la tuya!  
Imagen: /images//mate_y_naturaleza-mini.jpg


##Acerca del proyecto

Este proyecto surge con la idea de aunar bibliotecas de habla hispana que alberguen en su sitio contenidos libres, ya sea que se encuentren con licencias Creative Commons, Licencia de Prducción de Pares, en Dominio Público, etc. y que no prohíban su descarga y por el principal motivo que aboga este página: compartir libremente.

El sitio:
[Directorio](https://bibliotecas.enlacepilar.com.ar/)


Repo realizado en Django (marco de trabajo de Python) que te podés descargar libremente:
[Repo](https://gitlab.com/elibre1/directorio-de-bibliotecas-con-licencias-libres)

