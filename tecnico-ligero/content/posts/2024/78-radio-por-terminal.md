Title: Radio por Terminal en GNU/Linux
Date: 2024-03-11 21:05:28.668235
Autor: Hugovksy 
Slug: radio-por-terminal
Lang: es
Tags: Radio, Terminal, GNU/Linux
Category: Radio
Summary: Estaba buscando reproducir algunas radios argentinas por la terminal, como un verdadero técnico ligero, qué más...  
Imagen: /theme/images/sin-imagen.png


En el laburo siempre escucho la buena y edulcorada Aspen para pasar el rato,
pero como soy un ecléctico que no puede vivir solo de Stephen King y necesito
equilibrar siempre con un buen plato de Borges o Soriano para volver más tarde 
a Dick o Murakami lo mismo pasa con el rock, el pop o el estilo que sea.
Es por ello que estas breves líneas me traen con radios argentinas, a partir
de esta publicación:

<a href="https://gist.github.com/pisculichi/fae88a2f5570ab22da53" target="_blank">Radios</a>
                 
                                                         
Entonces fui a por el mplayer 

```
sudo apt install mplayer
```

Y me dije: basta de escuchar Aspen por la web, que encima tiene un blob y nunca la voy a poder descular ni inspeccionando.
Asi que di con la página mencionada y me voy a anotar los enlaces por acá, así en crudo para copiar y pegar en mi terminal cuando esté en el laburo.
Por eso como Cortázar con su Continuidad de los parques, esto es la continuidad de los cables, los de los parlantes de radio, acá va.

Ejemplo en mi pc:

<img src='/images/terminal-radio.png' alt='foto' class='img-fluid text-center'>

**Lista**

Aspen:
https://mdstrm.com/audio/60a2745ff943100826374a70/icecast.audio

Horizonte:
https://26453.live.streamtheworld.com/SAM06AAC243_SC

Rock & Pop:
https://22063.live.streamtheworld.com:443/ROCKANDPOP_SC

Estas tres están verificadas y andan

Tipeamos por ejemplo para Aspen:

```
mplayer https://mdstrm.com/audio/60a2745ff943100826374a70/icecast.audio

```






