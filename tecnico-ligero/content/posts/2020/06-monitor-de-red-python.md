Title: El monitor de red de Python más engendro que hayas podido ver (pero efectivo)
Date: 2020-06-19 20:00
Author: Hugovksy
Category: Python
Slug: monitor-de-red-python
Status: published

Llevo renegando varios días con la búsqueda de algo decente para hacer ping a la red y que no devuelva '0' porque os.system 'ping' devolvió 0 para todos porque cuando levanta la ventana para el todo es 0 aunque esté inactivo. Lo mismo para subprocess.run o call. Obviamente si usamos sobprocess.Popen tenemos un resultado decente porque trabaja desde el kernel y no sé que otras explicaciones súper informáticas encontré pero la verdad que volví a mis raíces: la seguridad del buen archivo de texto. Eso amigos, nunca falla. ¿Que el proceso es más largo? Bueno, puede ser. ¿Que todos esos pasos son innecesarios? Tal cual. Pero saben qué, me siento seguro y yo programo desde el lado del no matemático (Python para gente que sólo lee libros y busqué alguna vez). Entonces manos a la obra. Terminé por no confiar en ningún proceso engorroso. ¿Cómo? Sencillo: el viejo y querido os.system hace el ping, pero lo guardo en un archivo de texto. Después lo abro y me fijo si dice "Host Unreachable" (en  Linux. Para Windows es harina de otro costal, pero es lo mismo) dentro de la linea. Si lo dice, es que la ip no está viva. Y así, amigos me recorro la red y me fijo cuáles están activas. Es más, te da pie a guardar todas las ips activas en oootro archivo de texto y hacer muchas cosas, como copiar un archivo de ser necesario, ¡lo que quieras!

Acás rastreo la red 192.168.0.X (90 al 101 con range)

```
import os

ip = '192.168.0.'
  
for nro in range (90,102):
    resultado = os.system ('ping -c 1 '+ip+str(nro)+'>pingueo.txt')
    archivo = open ('pingueo.txt', 'r')
    lineas = archivo.read()
    archivo.close()
    if "Host Unreachablen" in lineas:
        print (ip+str(nro) + ' No está activa')
    else:
        print (ip+str(nro) + ' Está activa')
```