Title: Montar un Wordpress en una máquina virtual (remota). Pasos concretos e infalibles.
Date: 2020-07-21 10:37
Author: Hugovksy
Category: Wordpress
Slug: montar-un-wordpress
Status: published

Seguramente llegaste acá porque querés montar tu Wordpress y estás cansado de buscar miles de tutoriales al estilo: monta tu web y gana dinero mientras te vas a dormir. Este te aseguro que no es así. Estos son los pasos de un técnico ligero. Acá van, sin tanta vuelta.

1) Te bajás el wordpress desde <https://wordpress.org/>  
2) Descomprimís la carpeta (queda wordpress de comprimida en tu pc)  
3) Supongo que tenés una maquina virtual corriendo en algún lugar de Eternia... Bueno entonces el paso 3 es instalar Xampp o Lampp dependiendo del S.O. Como soy machito, elijo Lampp por consola. Fuente para esto (uno no nace sabiendo): <https://simplecodetips.wordpress.com/2018/06/14/instalacion-de-xampp-en-ubuntu-18-04/>


``` 
$ wget https://www.apachefriends.org/xampp-files/"Acá va la versión"/xampp-linux-x64-"Acá va la versión"-installer.run

```

Le damos derechos al archivo descargado para que no joda:

```
$ sudo chmod +x xampp-linux-x64-"version"-installer.run

```

Y por último instalamos:

```
$ sudo ./xampp-linux-x64-"versión-installer.run

```

Para arrancar:

```
$ sudo /opt/lampp/lampp start

```

4) Ya tenemos lampp corriendo. Escribimos la ip de la pc remota. Y cuando quieras entrar al phpmyadmin... no te va a dejar.  Una medida de seguridad. Rengué como el peor para poder encontrar qué era esto. Si en localhost de mi casa me deja. Qué pasa.

Esta es la solución correcta después de buscar como un engendro:

vamos a:  

```
cd /opt/lampp/etc/extra
sudo nano httpd-xampp.conf

```

y editamos estas líneas (fijate que están comentados los originales):

```
<Directory "/opt/lampp/phpmyadmin"\> 
#    AllowOverride AuthConfig Limit 
#    Require local 
#    ErrorDocument 403 /error/XAMPP\_FORBIDDEN.html.var 
AllowOverride AuthConfig Limit 
    Require all granted 
    Order allow,deny 
    Allow from all]{style="color: blue;"}[ \#esto es para que todos puedan entrar... PELIGROSO]{style="color: blue;"}

```


Para restringir el acceso, podés comentar lo la línea de arriba y poner solo tu ip 

```
  Allow from "Mi Dirección IP"

```

Listo. Ya podemos acceder a phpmyadmin. Ahora creamos la base de Wordpress donde va a ser instalado el sitio.

5) Ahora sí. Bajamos filezilla u otro FTP para copiar la carpeta wordpress a /opt/lampp/htdocs  

* Le damos derechos a esa carpeta copiada y a sus sub carpetas (recursivo como dicen los ñoños): sudo chmod -R 777 wordpress. Si estás en la carpeta /opt/lampp/htdocs. Si no, cargá toda la dirección antes.  
* Ahora sí, vamos a nuestro navegador y ponemos "mitsitio.com/wordpress". Y te pide la instalación. Ponemos la base que asignamos en el punto 4.  
* Luego nos pide nuestro usuario para conectarnos al panel. ¡Listo! Todo funcionando.
