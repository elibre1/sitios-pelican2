Title: Intentando replicar el calculo del número más grande en Python, en PHP
Date: 2020-08-03 12:41
Author: Hugovksy
Category: Python
Slug: numero-mas-grande-php
Status: published

Estoy meta intentos de aprender de una buena vez a defenderme con PHP y bueno... uno que quiere saber todo ya más la ansiedad... no, no. Eso no ayuda amigo, relax. Entonces a veces paro en medio del camino y me digo: para calmar los ánimos, ¿por qué no intentás hacer lo mismo que hacés en Python, pero en PHP? Buena ideaaaa. Allá vamos.

Un ejercicio simple. Imprimir el número más grande recorriendo una lista en Python. Intentar hacer lo mismo en PHP. Nada de usar max ni esas cosas. Hay que hacerlo a lo machito, comparando valores.

Veamos el ejemplo:

```
print ("Averiguar el número más grande")
print ("")
  
lista = \[\]
  
#Pido la carga de los números (todos int para evitar problemas)*  
a =15
while a != 00:
    a = int(input ("ingrese un numero (00 para salir): "))
    lista.append(a)
    
# ahora averiguo el nro. más grande: 
cant = len(lista)
mayor = 0
  
for elemento in lista:
    
    if elemento \> mayor:
        mayor = elemento

print ("El número más grande es: " + str(mayor) + " de un total de " + str(cant) + " números en la lista")]

```

El proceso es el siguiente: creo una lista vacía. Inicio un ciclo while al que le indico que mientras no se teclee "00" siga preguntando lo mismo (agregar un número). Eso que el usuario va agregando se inserta uno a uno en la lista, que puede llevar cualquier nombre, claro está, le puse lista para identificar más claramente. Después, imaginamos que yo no sé cuántos números va a insertar el usuario, necesito contar los elementos con len. Asigno después una variable "mayor" con valor inicial 0 para empezar a comparar con el condicional for, ayudado por if. El razonamiento sería éste: para cada elemento en la lista, si el elemento es mayor a "mayor", entonces mayor es igual al elemento. Esto recorre los valores uno a uno, siempre comparando el mayor con el elemento. Finalmente, imprimo el valor de mayor que quedó como más grande.

Ahora veamos cómo hacer lo mismo en PHP:

* Primero hay que pedir 2 numeros en html por ejemplo, separados por espacios, podría ser en un textarea:
  
```
    <textarea name=“numeros” rows=“2” cols = ’40’ placeholder=“Nros separados por espacios”></textarea> 

```


* Después tratamos esto en el backend:
  

```
if (isset($_POST[“enviar“])){


$valor=$_POST[“numeros“];


$lista = explode(“ “, $valor); //separa los espacios de lo que recibí en valor del textarea



$valor_temporal=0;

for ($i=0; $i <count($lista) ; $i++) { 

    if ($lista[$i] > $valor_temporal){

        $valor_temporal = $lista[$i];

    }

}

echo “<br><br><br>“;

echo “el numero más grande es $valor_temporal“;


}

```

Lo primero que hay que pensar es cómo trasladar los ingresos que yo hacía desde el script de Python, pero ahora en PHP. Miles de cajas de textos sería una locura. Podría ser un "textarea", como se ve en el body de la parte HTML. Usamos el Method POST para el formulario, que apunta a la misma página.

Encierro todo en un condicional IF para que no aparezca al inicio. Si se presiona el botón "enviar" que se inicie el procedimiento. Le indico al usuario que separe por espacios los numeros. Después uso la función "explode" para que lo almacenado en el string de la variable ***$valor*** se separe y forme un array. Con eso ya puedo recorrerlo con el bucle for, del mismo modo que lo hice en Python. La variable ***$valor_temporal*** es igual a la de Mayor de Python, comenzando con un valor de 0. Creo que el resto se puede entender, salvando las distancias entre un lenguaje y el otro, hace exactamente lo mismo: va almacenando el valor máximo y finalmente lo imprime en pantalla.
