Title: Me indigno y me empecino en leer un archivo de texto de la consola CMD, en Python
Date: 2020-07-02 09:23
Author: Hugovksy
Category: Python
Slug: leer-archivo-cmd
Status: published

Un día de los tantos que un técnico ligero tiene en su quehacer, quería abrir unos resultados de una lista de impresoras que exporté a txt. Pero Python empezó a tirar cosas locas. Qué es eso, me pregunté primero x0x0x0x0x. Bueno no hay drama, siempre está el buen for para recorrer línea a línea. El tema es que al imprimir, se veía el texto separado y la consola como que lo deformaba. Y en otros casos no se ~~leía un carajo~~ nada. Unos símobolos, dos palabras y ya. Qué está pasando, si con otros textos no tuve nunca drama, me preguntaba. Y ahí di con un módulo que se encarga de eso, que viene de versiones viejas de Python, llamado codecs. Entonces, para abrir archivos rebeldes, hacemos toda la misma operatoria que para abrir un archivo cualquiera, pero importamos el módulo en cuestión y en archivo, llamamos a la función codecs.open. Al final agregamos utf-16. Sí después me enteré, cosa que nunca hago, que al abrir el archivo de texto en el notepad te muestra la codificación abajo.


``` 
import codecs
archivo = codecs.open ('impresoras', 'r', 'utf-16')
lineas = archivo.readlines()

archivo.close()

nueva = []

for linea in lineas:
    linea = linea.strip('rn  ')
    print (linea)
    nueva.append(linea)

print (nueva)

```
