Title: Forma no muy elegante de fijar una impresora predeterminada con Python
Date: 2020-07-06 13:24
Author: Hugovksy
Category: Python
Slug: impresora-predeterminada-python
Status: published

Estaba buscando la forma de fijar una impresora como predeterminada sin la intervención del usuario porque, digámoslo así, se pierde mucho tiempo en conectarse por asistencia remota sumada a la interacción innecesaria con el mismo (podría estar haciendo otras cosas, como por ejemplo, leer, ver la vida desde otro ángulo, meditar, programar otros scripts, etc).  
Entonces me hice una serie de pasos en los comentarios para lograr ese objetivo. Antes, cabe aclara había fallado estrepitosamente con pstools. ¿Por qué? Ejecutaba el comando en un bat pero cuando quería fijar la impresora, se lo hacía al admin. Nunca supe cómo hacerlo al usuario de turno. Tampoco quise demorarme más buscando esa solución porque (pareciera ser el tema central de esta publicación, el tiempo) tardaba demasiado el psexec.

Bien dado el fracaso del que me dí cuenta muchos días después, pasé a este proyecto:

Lo que hago básicamente es pedir la ip y el usuario en cuestión. El wmic me tira las impresoras remotas pero sin decir fehacientemente cuál es la predeterminada, por eso no se lo pido. Le digo que lo guarde en un archivo txt y, como es utf-16, uso el codecs.open para leerlo bien en pantalla.

Ahi le pregunto al usuario: ¿bueno amiguillo, qué impresora quieres? Y me dice: tal. Ok ahí te va el archivo para que lo ejecutes. Copio la impresora y lo escribo en una archivo "predeterminada.bat", para que haga doble clic el usuario en su escritorio (revolviendo en internet di con ese comando RUNDLL32 PRINTUI.DLL,PrintUIEntry, que no sabía que existía. ¿Ya está? pregunto amablemente. Me dice sí. Bueno, adiós. Y ahí es cuando le borro el archivo con os.remove. ¿No es una ligereza genial?

Porque a un técnico ligero entusiasta del Software Libre, cuando no le queda que usar software privativo, como en el laburo por ejemplo, puede dar batalla usando estas pequeñas vanidades con SL.


```
import os
import shutil  
import codecs

#pedir la ip y el usuario:  
ip = input("ingrese IP: ")  
usuario = input("ingrese usuario: ")  
  
print ('Trabajando. Espere...')

#obtener lista de impresoras equipo remoto  
os.system("wmic /node:"+ip+" printer get name \>impresoras.txt")

#leer el archivo y ver qué impresora quiero poner como predeterminada  
archivo = codecs.open ('impresoras.txt', 'r', 'utf-16')  
lineas = archivo.read()  
archivo.close()  
print (lineas)

#copiar Archivo para que ejecute el usuario y la setee (que diga cuál quiere)  
nombre = input ("indique nombre impresora: ")   
archivo = open ("predeterminar.bat", 'w')  
escribir = 'RUNDLL32 PRINTUI.DLL,PrintUIEntry /y /n "'+nombre+'"'  
archivo.write (escribir)  
archivo.close()  
shutil.copy ('predeterminar.bat', '//'+ip+'/c\$/users/'+usuario+'/Desktop')  
input ("Archivo copiado... Espere hasta que el usuario lo ejecute y proseguir...")

#finalmente borrar el archivo  
os.remove ('//'+ip+'/c\$/users/'+usuario+'/Desktop/predeterminar.bat')  
  
print ("Archivo borrado. Adiosín")

#FIN

```