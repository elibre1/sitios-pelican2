Title: Comparando el monitor engendro de red con uno más decente en Python
Date: 2020-06-23 11:58
Author: Hugovksy
Slug: compara-monitor-red-python
Status: published
Category: Python
Summary: Se podía observar una forma poco convencional de hacer ping y obtener resultados del equipo

En publicación anterior se podía observar una forma poco convencional de hacer ping y obtener resultados del equipo (si estaba vivo o muerto) en una red desde Linux. Ahora vamos a hacer lo mismo desde Windows de dos maneras. La primera, llamando a la función **GETOUPUT** del módulo suprocess. Importamos también el datetime para que nos de la entrada y salida en una variable para poder calcular el tiempo de ejecución. El ping lo hacemos en un rango del 20 al 30. La variable "ip" da la ruta a la que asiganearemos este rango mencionado.

En la segunda, lo mismo pero llamando al modulo **OS** en vez de al subprocess y guardando el resultado en un archivo de texto, ip por ip. Como cada vez que guarda se sobrescribe, siempre que llamamos a leer el archivo va a ver el último ping. Después lo transformamos en una cadena con archivo.read() en la variable lineas. Y si se dan ciertas condiciones, dará que está activa o fuera de línea. Podrás decir, seguramente: ¡con tanto proceso el consumo es muchísimo mayor! Yo pensé lo mismo, pero fijate que no. Más abajo pongo los resultados de ambos procesos.  
De todos modos, a veces ganaba uno u otro, no siempre el segundo. Lo que vale aclarar es que la diferencia es mínima.

PRIMERO

```

from subprocess import getoutput as go
from datetime import datetime

tiempo_inicial = datetime.now() 

ip = ‘10.10.2.’


for nro in range (20,30):
    resultado = go (‘ping -n 1 ‘ +ip+str(nro))
    if ” bytes=32 tiempo” in resultado:
        print (ip+str(nro)+’: está activa’)
    elif “Host de destino inaccesible” or “Tiempo de espera agotado” in resultado:
        print (ip+str(nro)+’: fuera de línea’)

tiempo_final = datetime.now() 
tiempo_ejecucion = tiempo_final – tiempo_inicial

print (‘El tiempo de ejecucion fue:’,tiempo_ejecucion) #En segundos

```

SEGUNDO

```

from subprocess import getoutput as go
#from datetime import datetime
import os
tiempo_inicial = datetime.now()

ip = ‘10.10.2.’

for nro in range (20,30):
    resultado = os.system (‘ping -n 1 ‘+ip+str(nro)+’ >pingueo.txt’)
    archivo = open (‘pingueo.txt’, ‘r’)
    lineas = archivo.read()
    archivo.close()
    #print (lineas)
    if “bytes=32 tiempo” in lineas:
        print (ip+str(nro) + ‘: Está activa’)
    elif “Host de destino inaccesible” or “Tiempo de espera agotado” in lineas :
        print (ip+str(nro) + ‘: No está activa’)

tiempo_final = datetime.now()
tiempo_ejecucion = tiempo_final – tiempo_inicial

print (‘El tiempo de ejecucion fue:’,tiempo_ejecucion) #En segundos

```

```
10.10.2.20: fuera de línea
10.10.2.21: fuera de línea
10.10.2.22: fuera de línea
10.10.2.23: fuera de línea
10.10.2.24: está activa
10.10.2.25: fuera de línea
10.10.2.26: está activa
10.10.2.27: fuera de línea
10.10.2.28: está activa
10.10.2.29: está activa
El tiempo de ejecución fue: 0:00:24.220165

```