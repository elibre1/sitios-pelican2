Title: Código simple para saber los roles del usuario en Windows con Python, en un AD
Date: 2020-07-02 08:53
Author: Hugovksy
Category: Python
Slug: roles-usuario
Status: published


Con este código puedo obtener los roles de un usuario cualquiera que se encuentre dentro del dominio. Para qué tanto bardo si lo puedo hacer desde la consola de DOS te preguntarás. Bueno, por la depuración del texto, claro está. Eso es lo maravilloso de Python.


* Como primera medida importo subprocess. Después, le pido al usuario que ingrese el nombre de quien quiere saber los roles. Acto seguido corro el run de subprocess llamando al net user de windows, y que lo guarde en un txt.

* Después lo abro y le digo que recorra las línas. En este caso quiero que empiece a depurar desde un * que marca el inicio de determinados roles. Esta ubicación que encuentra el "find" quiero que la guarde en una variable, la linea1. Después abro oootra variable para guardar, de cada línea que va recorriendo desde el comienzo (que lo tenía almacenado en "linea1" hasta el -1 que sería el final de la línea. Finalmente esa variable linea2 la voy agregando en la lista "roles" que estaba vacía más arriba.

* Después, imprimo la lista roles recorriéndola línea a línea, solo con lo que necesitaba. 


```
import subprocess
usuario = input ("ingrese el usuario: ")
subprocess.run ("net user "+usuario+ " /dominio >usuario.txt", shell=True)

archivo = open ('usuario.txt', 'r')
lineas = archivo.readlines()
archivo.close()

roles = []

for linea in lineas:
    if "*Linea que quiero depurar" in linea:
        linea1 = linea.find("*") # Me fijo en que ubicación está el '*' de *Linea...
        linea2 = linea[linea1:-1] #corto todo desde ahí hasta el final
        roles.append(linea2) #agrego cada linea a la lista con esta característica.

print ('Los roles del '+usuario+' son:')
for linea in roles:
    print (linea)
```
