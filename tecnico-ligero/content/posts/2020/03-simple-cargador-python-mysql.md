Title: Simple cargador en Python, multi instancia, para una base en MySQL
Date: 2020-06-14 20:11
Author: Hugovksy
Category: Python
Slug: simple-cargador-python-mysql

Cabe aclarar que en [este post](https://www.memorias-tecnico-ligero.duckdns.org/2020/06/12/desarrollando-una-web-a-los-ponchazos-con-flask/) olvidé mencionar que la base la estuve trabajando en SQLite. ¿Por qué? Porque es simple, es un archivo portable y porque soy ligero y opto por el camino más sencillo y eficaz para mí. Pero no sólo de eficacia vive el hombre y a veces las cosas que uno anota como pendientes en un cuaderno hay que llevarlas adelante alguna vez, y esa vez aunque sea breve (sólo insertar y consultar) me llevaron a buscar cómo hacer un cargador para MySQL en Python. Lo más sencillo si estás en Windows es usar Xampp y listo, pero si estás en Linux la cosa no me funcionó así como así por lo que opté por: Máquina virtual con Linux Mint y un apache + mariadb + phpmyadmin. Cuando instalas mariadb de movida tenés que hacer lo cambios de clave "mysql native password" porque si no la cosa no funciona y no lo voy a explicar acá, no viene al caso y se puede googlear. Pero una vez solucionados los inconvenientes y creada la base (una simple de libros: "id, titulo, autor y genero) podemos empezar a cargar. ¿El módulo para la carga? Da lo mismo, todos funcionan más o menos igual. Yo probé con pymysql y con mysql.connector. El tema es que quería que me preguntara una y otra vez si quería seguir agregando, así que hice 2 menúes, uno de salida y el otro insertador. También hice un try un except porque me gusta cuando me avisa que está bien o que hice cagadas, es más elegante. Y confieso que el try y el except lo aprendí a los ponchazos, como todo. Aquí está:


```
import pymysql]
  

   menu = input ('Agregar mas? X para Salir: ')  
   if menu == "X":  
      print ("adios")  
   else:  
      insertador()  
  


def insertador():  
     
   titulo=input ('nInserte titulo del libro: ')  
   autor= input ('Inserte autor: ')  
   genero= input ('Inserte genero: ')  
  
   # Abrir conexión a la base de datos  
   con = pymysql.connect(  
   host="localhost o donde esté la base",   
   user="root",  
   password="la clave de root",  
   db="Nombre de la base"  
   )  
  
   msg = 'hola'  
   cursor = con.cursor()  
  
   #Usando el try sabemos fehacientemente si se insertaron los datos o no.  
  
   try:  
      sql = "INSERT INTO libros (titulo, autor, genero) VALUES (%s,%s,%s)"  
      val=(titulo, autor, genero)  
      cursor.execute(sql,val)  
  
      print("Ingresado")  
      con.commit()  
      msg = "Datos correctos"  
  
   except:  
      con.rollback()  
      msg = "Ocurrió un error"  
  
   finally:  
      print (msg)  
      con.close()  
   salida()  
  
insertador()
```