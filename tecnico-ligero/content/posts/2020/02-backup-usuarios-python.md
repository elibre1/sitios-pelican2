Title: Un scritp para backup de usuarios en Python
Date: 2020-06-13 20:28
Author: Hugovksy
Category: Python
Slug: backup-usuarios-python
Status: published
Summary: Podés hacer un backup sólo con la ip, el usuario y el fantástico Robocopy...
Imagen: /images/logo1.jpg

Si sos ténico en sistemas Windows y tenés la derechos de aministrador con esto podés hacer un backup sólo con la ip, el usuario y el fantástico Robocopy. Una genialidad para no tener que ir a la carpeta del usuario y copiar todo. Esto lo hacer por vos y discrimina algunos archivos molestos.

Como hoy estoy dicharachero voy a explicar paso por paso (es mentira, lo hago porque el script es corto):  
-Primero importamos el módulo subprocess, fundamental para llamar a instancias del S.O.  
-Después englobamos todo en una función.  
-Pedimos la ip con un input  
-Pedimos el usuario y el nombre donde va a ir (tiene que existir unidad D en tu pc en este caso)  
-La variable ruta arma todo con lo que pusiste arriba, más c\$/users, que es donde están todos los usuarios en Windows (hasta el día de hoy, W10)  
-El destino, donde vas a backupear.

-La variable copia lo que hace es llamar a Robocopy desde la funcion subprocess.call y le dice que use la ruta origen y la copie a destino, excluyendo el archivo NTuser y el direcctorio (al pedo claro está para este caso) APPdata. Está comentado, pero si querés registro podés crear al final un TXT.

Listo. Tirás la copia y en la variable te va a dar un número. Si es 9 anduvo todo bien. Si es 8, ya había algo en la carpeta, y si es 16 alguna cagada hiciste.

Finalmente, llamo a la función. Eso es todo.

¡Ahí va!

```

import subprocess

def backup():
 
    laip= input (‘ nIngrese la ip: ‘)
    usuario= input (‘Ingrese el usuario para backup: ‘)
    carpeta= input (‘Nombre carpeta destino: ‘)

    ruta = (‘//’+laip+’//c$/users/’+usuario+’/’)

    destino = (‘d:/backups/’+carpeta+’/’)

    copia = subprocess.call(‘robocopy ‘ +ruta+ ‘ ‘ +destino + ‘ /S /XF “NTUSER.*” /XD “Appdata” ‘) # si quiero registro —- /LOG:d:log-usuario.txt

    if copia == 9: #son numeros que da el sistema para exito, igualdad o error
        print (‘Backup realizado con exito…’)
    elif copia == 8:
        print (‘Ya habia contenido en esa carpeta :(‘)
        print (‘Escribi backup() y empeza de nuevo’)
    elif copia == 16:
        print (‘Por algun motivo fallo :(‘)
        print (‘Escribi backup() y empeza de nuevo’)

backup()

```
