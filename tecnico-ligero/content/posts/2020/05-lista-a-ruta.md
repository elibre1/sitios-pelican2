Title: Convertir en una ruta elementos de una lista con Python
Date: 2020-06-18 18:37
Author: Hugovksy
Category: Python
Slug: lista-a-ruta
Status: published

En una publicación anterior, de una **lista** con diversos valores armaba un **diccionario** para, posteriormente utilizarlos con algún fin, por ejemplo, crear una ruta con el usuario como key y la ip como value. En este caso lo que hago es prescindir de la creación del diccionario e ir eliminando en caliente cada elemento de la lista hasta que llegue a 0.

En vez de print (ruta) como en el ejmplo, se podría crear una acción de ser necesaria, como copiar a la ruta creada hasta llegar a 0, con el modulo os o shutil, por citar dos opciones.

```

    lista = [‘Herman’, ‘192.168.0.25’, ‘Juanca’, ‘192.168.0.33’, ‘Richard’, ‘192.168.0.54’]

    a = len(lista)

    while a > 0:
        for elementos in lista:
            ruta = (‘//’+lista.pop(1)+’/home/’+lista.pop(0))
            print (ruta)
            a = a – 2

    print (‘eso es todo’)

```