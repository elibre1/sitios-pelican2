Title: Convertir una lista en un diccionario en Python
Date: 2020-06-18 11:59
Author: Hugovksy
CCategory: Python
Slug: lista-a-diccionario-python
Status: published

Supongamos que tenemos este inconveniente: tenemos una lista con unos valores cargados (puede ser a través de unos datos que posteriormente se convirtieron a lista con lista.split()). Entonces lo que queremos hacer es cargar el primer dato (0) como clave del diccionario y el segundo (1) como valor. Pero eso no queda acá, porque nosotros no sabríamos qué cantidad de valores se han ingresado y para eso los contamos en con len(lista). De este modo sabemos dónde estamos parados (o mejor dicho, el script lo sabrá). Después hacemos un diccionario vacío.  Por último, analizando lo que hasta ahora hicimos, podemos concluir lo siguiente: mientras las cantidad sea mayor a 0 que agregue el elemento 0 como clave al diccionario y lo borre; y que elemento 1 de la lista sea el valor y que también lo borre acto seguido. Pero como la variable cantidad sigue teniendo el valor inicial de la lista, porque no se va actualizando, lo hacemos a mano de a pares, claro está: cantidad = cantidad -2.  
Eso es todo pero me rompí el coco un rato largo para que sea lo más sencillo posible. Había pensado al principio en separar los valores en 2 listas pero iba a extender el código y perdía la ligereza...

```
    lista = [‘Herman’, ‘192.168.0.25’, ‘Juanca’, ‘192.168.0.33’, ‘Richard’, ‘192.168.0.54’]

    nuevo_dic = {}

    cantidad = len(lista)

    while cantidad >0:
        nuevo_dic[lista.pop(0)] = lista.pop(1)
        cantidad = cantidad – 2


    print (nuevo_dic)

```