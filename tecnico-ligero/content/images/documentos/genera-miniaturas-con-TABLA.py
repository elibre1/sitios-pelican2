import os
from PIL import Image
import datetime

fecha_publicacion = datetime.date.today()
fecha_publicacion = fecha_publicacion.strftime('%Y-%m-%d %H:%M')


def escribe_archivo (datos, nro_public, mes, dia_public):
    archivo = open ('content/'+nro_public+'-'+mes+'.md', 'w')
    archivo.write("""Title: Galería de """+mes+"""
Date: """+dia_public+"""
Autor: Hugovksy 
Slug: """+mes+"""
Lang: es 
Tags: """+mes+""" 
Category: """+mes+"""
Summary: Galería de """+mes+""" 

\n
\n

"""+datos+"""

    """)
    archivo.close()


#obtengo el directorio actual
cwd = os.getcwd()


directorio = "images"
mes = input ("¿Qué mes vas a publicar (igual nombre de carpeta): ")
nro_publicacion = input("nro de publicacion: ")
nro_publicacion = str(nro_publicacion)

lee_desde = cwd+"/content/images/"+mes
print (lee_desde)

fotos = ''
fotos += '<table style="border: 1px solid black;"><tr>'
n = 1

for archivo in os.listdir(lee_desde):
    if archivo.endswith('.jpg'):
        #print (archivo)
        if (n < 4):
            im = Image.open(lee_desde+'/'+archivo)
            im.thumbnail((200,175), Image.ANTIALIAS)
            nom = archivo.split ('.')[0]
            tnom = "zz-mini_ "+ nom + ".jpg"
            im.save(lee_desde+"/"+tnom, "JPEG")
            fotos+= '<td style ="border: 1px solid black; border-radius: 10px;"><a href="%s/%s/%s" target="_blank"><img src="%s/%s/%s"></a></td>' % (directorio, mes, archivo, directorio, mes, tnom)
            n +=1
            print (archivo)
        else:
            im = Image.open(lee_desde+'/'+archivo)
            im.thumbnail((200,175), Image.ANTIALIAS)
            nom = archivo.split ('.')[0]
            tnom = "zz-mini_ "+ nom + ".jpg"
            im.save(lee_desde+"/"+tnom, "JPEG")
            fotos +='<td style ="border: 1px solid black; border-radius: 10px;"><a href="%s/%s/%s" target="_blank"><img src="%s/%s/%s"></a></td></tr>' % (directorio, mes, archivo, directorio, mes, tnom)
            n = 1
            print (archivo)

fotos+= ('</tr></table>')
                    
escribe_archivo (fotos, nro_publicacion, mes, fecha_publicacion)
                    


