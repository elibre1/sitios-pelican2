Title: Primera entrada
Date: 2022-07-11 20:18
Autor: Hugovksy 
Slug: arribando-por-estos-lares
lang: es
Category: Empezando...

### Empezando con el generador de sitios estáticos Pelican
Acá vengo tratando de empezar nuevamente un blog, como aquellos que he abandonado en el tiempo. Pero (creo), este pretende ser diferente, a saber:

- Lo empiezo con el generador de sitios estáticos Pelican [(más info acá)](https://docs.getpelican.com/){:target="_blank"}, cosas que me alejan de la tecnología dependiente de las corporaciones.
- Lo alojo en mi Raspberry en casa, intentando (simpre que disponga de una IP fija, claro) promover el autohosteo para alojar mi pequeño sitio. 
- Está desarrollado en Python y usa Jinja para los modelos de sitios, por lo que me resulta un poco más sencillo de entender.
- Que me sea más simple publicar.
- Ser más libre a la hora de disponer de la modificación y movimiento de los textos publicados.
- Me gusta haberme topado con este tipo de generadores como hice siempre en mi vida, buscando y buscando :)
- Me gusta pensar la web como cuando era joven y (más) inexperto.
- Tener un sitio web no tiene por qué tener una carga innecesaria de contenido.
- Me gusta el minimalismo que proponen los sitios estáticos.
- Sin demasiados abalorios, por no decir ninguno.
- ¡Autogestionado!
- ¡Software Libre!

Creo que debe de haber muchas cosas más que seguro voy a ir anotando por acá. 
<br><br>
#### Acerca de mí
<hr>
<br>
Está bueno saber cuando uno navega por las páginas de internet de por ahí, de qué va esta web, sobre todo las personales como los blogs, ¿no?

Este blog intenta reunir mis experiencias como técnico resolviendo algunas cosillas de la vida del, valga la redundancia, técnico. 
Qué tipo de técnico, te preguntarás. Uno que estudíó Gráfica y Animación digital, después una Tecnicatura en Software libre y ahora sigue estudiando un Ciclo de Licenciatura en Tecnologías para la Gestión de las Organizaciones. O sea, a partir de ese menjunje veremos qué sale. 

Hay épocas donde primarán las fotos, otras las resoluciones en cuestiones de programación, otras sobre algún esbozo de escritura, otros quién sabe...

¿Por qué el mote de "ligero"? Porque en el trabajo me cargaban que soy ligero y me comporto como el servicio mecánico de los seguros, que ofrecen mecánica ligera para poder salir del paso. En este caso serían soluciones no a la medida convencional, pero que funcionan y valga la redundancia, sirven para salir del paso. En muchos casos me ha sucedido así, como las bienamadas ***bifurquetas***, que no sería más que usar un solo cable UTP para generar 2 bocas de red cuando la cosa escasea. Pero esa es otra historia...

Empecé a ser un técnico ligero propiamente dicho por el 2009, cuando terminé una tecnicatura en gráfica digital en la unviersidad del litoral y me lancé a hacer un corto que quería hacer con software libre nada más. A partir de ahí todo cambió. Después entré en informática en un organismo del estado que en este micro resumen no viene al caso y le di duro a la ligereza hasta estos días. Por el momento la historia es así, capaz un día me pinte borrar todo y contarla de otro modo o desde otro ángulo. :)


##### Mi contacto por si me querés escribir (no hay formularios ni nada): [correo](mailto:enlacepilar@protonmail.com)
