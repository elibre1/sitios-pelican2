Title: Mi camino hacia el software libre
Date: 2023-01-13 11:06
Author: Hugovksy
Slug: mi-camino-hacia-el-software-libre
Status: published
Category: Software Libre



Un técnico ligero sin un derrotero que contar no es un técnico ligero. Y mi camino estuvo plagado al principio al software privativo, sin pensarlo demasiado, muchos windows pirateados y demás cosas que un buen día fueron cambiando con la llegada del Software Libre, que tocó a mi puerta una tarde de verano dentro de mi bello y bienamado mundillo digital (esto último fue demasiado poético y claramente no fue así). 

Estaba estudiando en la Universidad Nacional del Litoral la Tecnicatura en Gráfica y Animación Digita cuando me topé con una materia, Informática y Sociedad, que analizaba las bienaventuranzas de pasarse al lado libre de la informática. 

¿Qué lo qué? 

Mi único conocimiento sobre Linux a secas fue un Corel Linux que jamás logré amar alla por fines de los 90s, y que terminó abandonado en un cajón. Tenía una HP como Pc de escritorio con un mouse implacable que nunca se rompió. Pero más, más atrás, a principios de esa misma década, mi vida cambió cuando mis viejos me regalaron una XT sin disco rígido y con discos de 5 1/4. Toda una tecnología, monitor fósforo ámbar. El Soccer, el Italia 90, el Maniac Mansion... Pero sin dudas el  hito de mi vida fue el Monkey Island I... Bueno, me fui por las ramas. Prosigamos. ¿Qué era esto de poder compartir, no crakear programas, usar alternativas libres y demases que decía un profesor loco de la universidad que ni siquiera erá técnico y era más del palo de la filosofía? He de probarlo me dije y me instalé Ubuntu, después Mint, después Debian, Musix, Ututo, Tuquito... Y me lancé a hacer un corto llamado Simbiosis hecho enteramente con herramientas libres. El resultado fue un asco sin lugar a dudas pero bien valió la pena, y el reto fue haber podido hacerlo con íntegramente con Software Libre.

La lucha continúa al día de hoy, porque esto no es un cambio radical, pero puedo decir que en mi casa uso solo SL para mi PC, no así en el trabajo que todo se hace con privativo :(  Un técnico en un vuelo contradictorio, como diría Cobain. Pero tengo esperanzas de hacerme uno con el universo algún día y poder trabajar sólo con SL. Quién sabe...




