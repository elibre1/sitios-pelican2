AUTHOR = 'Enlacepilar'
SITENAME = 'Memorias de un Técnico Ligero'
SITEURL = ''
ATTRIBUTION = 'COMPARTIR'

#OUTPUT_PATH = '/var/www/html/tecnico-ligero/'

YEAR_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/index.html'
#YEAR_ARCHIVE_URL = 'posts/index.html'
#MONTH_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/{date:%b}/index.html'

ARTICLE_PATHS = ['posts']
ARTICLE_SAVE_AS = 'posts/{date:%Y}/{slug}.html'
ARTICLE_URL = 'posts/{date:%Y}/{slug}.html'

PATH = 'content'

TIMEZONE = 'America/Argentina/Buenos_Aires'

DEFAULT_LANG = 'es'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

LINKS = (('Biblioteca EnlaceLibre', 'https://biblioteca-enlacelibre.duckdns.org/'),
         ('Ocruxaves', 'https://ocruxaves.com.ar'),
         ('Biblioteca de pruebas en django', 'https://biblioteca-enlacelibre.enlacepilar.com.ar'),
         ('Radio Elibre Guerrilla', 'https://elibre.netlify.app/'),)

SOCIAL = (('No soy social', '#'),
          ('Acá tampoco hay enlaces', '#'),)
          

DEFAULT_PAGINATION = 8

DISQUS_SITENAME = "memorias-de-un-tecnico-ligero"

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

#en Huayra
#THEME = "/media/DATOS/Ubuntu-Huayra---Casa--cueva/Pelican/Temas-que-me-gustaron/tecnico-ligero-brutalist"
PORT = 8001
#en Lenovo
THEME = "/home/enlacepilar/Documentos/tema-geliba-para-pelican-sitios-estaticos-sin-js"
