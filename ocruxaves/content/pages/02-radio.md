Title: Reportaje Radio Simphony
Date: 2021-08-02 18:21
Author: Ocruxaves
Category: Reportaje
Tags: radio, reportaje
Slug: reportaje-radio-simphony 
Status: published
Cover: images/libro-fondo.jpg


Radio Simphony FM 91.3 - San Isidro

Programa: Reflexiones, con María Teresa G. E.

Fecha: 18 de noviembre de 2009

Operador: Pancho Noguer



<video width="640" height="480" controls>
  <source src="/images/videos/reportaje-radio.mp4" type=video/mp4>
</video>
