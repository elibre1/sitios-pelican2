Title: Cómo debe plantearse la edición de un libro de autor
Date: 2024-01-11
Autor: Hugo Enrique Boulocq
Cover: images/Hugo-E-Boulocq.jpg

Empezaré por una verdad de Perogrullo que, sin embargo, respetan pocos autores que quieren editar un libro: **Es necesario tener algo bien escrito.**

La edición nunca comienza en la oficina del editor de autor (aquel a quien se le paga para que nos edite un libro). A él no hay que llevarle textos sueltos, desordenados, desprolijos o en forma de rompecabezas; por el contrario, cuanto más coherente, ordenado, prolijo y armado esté nuestro manuscrito, no sólo nos aseguraremos de que se publique lo que realmente escribimos, sino que demostraremos una confianza que cualquier editor de autor siempre tiene en cuenta. Esa seguridad de parte nuestra hará posible que prestemos la debida atención al negocio en sí, porque lo que cuenta en este tipo de ediciones es, precisamente, lo que nos cuesta, lo que invertimos y lo que vamos a lograr a cambio de nuestro dinero. 

Elegir un tipo de papel determinado, una encuadernación conveniente y una portada atractiva se puede hacer bien si ya decidimos editar lo que tenemos escrito. Considero que esta recomendación también sirve para aquellas personas que contratan a un especialista para que les escriba el libro. 
Todas estas elecciones concretas que trataremos con el editor de autor, sólo deben hacerse como una proyección hacia el futuro y no como un contínuo regreso sobre lo que ya escribimos. Y si lo que ya escribimos lo queremos publicar, debemos hacerlo y no ponerlo en duda. 

Seguiré por algo que debe pensarse seria y sinceramente: ***No editar para vender.***
La posibilidad de que vendamos todos los libros que editemos es tan remota como incierta. La enorme mayoría de los escritores son desconocidos para el público y carecen de medios suficientes para armar una campaña publicitaria que los haga conocidos. Por lo general, todo el dinero disponible se lo lleva la edición y ya no quedarán medios suficientes para propaganda, publicidad y promoción comercial del libro.

Entonces, cifrar toda la ilusión y las expectativas de la edición en su posterior distribución y venta es un error. En este tipo de negocios, como lo es la edición de autor, el editor hace un negocio anticipadamente, porque cobra lo que gasta y además percibe sus honorarios dentro del precio de la edición. Si el autor quiere ganar algo genuino debe ***editar para ser leído.***

A partir de este cambio de verbos, cambiarán muchas de las perspectivas e ilusiones que se depositan erróneamente en la edición del libro.   


