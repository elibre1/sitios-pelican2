Title: Libros Editados
Date: 2022-09-03
Author: Ocruxaves
Category: Libros
Tags: libros, base de datos, ocruxaves, ediciones
Slug: libros-editados-prueba
Status: draft
Cover: images/libro-fondo.jpg


![libros](/images/libros.jpg)


**Libros editados por Ediciones Ocruxaves durante el período 1986-2012**
*Próximamente la base* 

<a href="https://biblioteca-enlacelibre.duckdns.org/media/libros/557Enroque_en_la_ventana_1987_-_Hugo_Enrique_Boulocq.pdf" target="_blank">Enroque en la Ventana</a>

<table class="table table-info table-bordered table-hover" >  
        <thead>  
            <td>Nro.</td>  
            <td>Año</td>  
            <td>Título</td>  
            <td>Género</td>
            <td>Autor</td>
            <td>Comentarios</td>  
        </thead>  
        <tbody id='tabla-datos'> </tbody>
</table>


<script>


let trae_libros = async() =>
{
     let tabla_datos = document.getElementById('tabla-datos')
   
    console.log ("Funciona!")

    tabla_datos.innerHTML =``
    
    const url = 'https://ocruxaves.com.ar/images/libros/consulta.php'
        const request = new Request(url, 
        {
            method: 'GET',
            cache: "no-cache",
            headers: {'X-CSRF-TOKEN': ('meta[name="csrf-token"]').attr('content')}
        
        });
        const response = await fetch(request);
        const data = await response.json();
        
        for (var i = 0; i < data.nombre.length; i++) 
        {
            console.log(data.nombre[i][0])
           tabla_datos.innerHTML +=`
           <tr><td> ${datos['id']} </td> <td> ${datos['año']} 
                    </td><td> ${datos['titulo']} </td> <td> ${datos['genero']} 
                    </td> <td> ${datos['autor']}</td> <td> ${datos['comentarios']}</td></tr>
        `;
        }
}
   

window.onload=trae_libros()

</script>