Title: Placa SADE
Date: 2021-08-02 18:21
Author: Ocruxaves
Category: Placa
Tags: placa, SADE Delta, Hugo Boulocq, Rachel Vivas
Slug: placa-sade-conmemorativa
Status: published
Cover: images/libro-fondo.jpg


Descubrimiento de las placas póstumas en el día del escritor para Rachel Vivas y Hugo Enrique Boulocq.

**13 de junio de 2012**

<video width="640" height="480" controls>
  <source src="/images/videos/placa-sade.mp4" type=video/mp4>
</video>
