Title: Ediciones Ocruxaves
Date: 2022-09-03
Cover: images/hugo-enrique-boulocq.jpg
Save_as: index.html
Status: hidden


Ocruxaves es una editorial de San Fernando fundada por Hugo Enrique Boulocq en 1985, prestando servicio a la comunidad de nuevos autores que desean publicar sus libros, como así también a eminencias literarias de la zona, tales como Carlos Enrique Urquía, Francisco Vázquez y Héctor Prado, entre otros.

Comenzó siendo una revista literaria, allá por mediados de los años ´80, para luego convertirse en editorial, que publicó más de 400 ediciones de autor hasta 2012. Se destacaban principalmente las ediciones de cuentos y poesía como así también novelas y, en menor medida, ensayos, trabajos de investigación y biografías, entre otras publicaciones.

**NOTICIA BIOBIBLIOGRÁFICA**

Hugo Boulocq - Editor y escritor de San Fernando

Nació en Buenos Aires, el 18 de octubre de 1952 y falleció el 28 de enero de 2012.

Fue Asistente Técnico de la Provincia de Buenos Aires en el área Talleres Literarios. Fundó y dirigió la revista literaria Ocruxaves y el periódico Prensa Literaria. Fue columnista del periódico Prensa Chica, colaborador de la revista Clepsidra y coeditor de la revista de poesía El barco ebrio.

Reunió sus cuentos publicados en el diario La Prensa, de Buenos Aires, y en las antologías de Ediciones Filofalsía en un libro titulado “Enroque en la ventana”, editado en 1987. En 2003 publicó su segundo volumen de cuentos “En la prisión de los bárbaros y otros cuentos”, y en 2008 su tercer libro de cuentos “Siempre llueven flores en Manantiales”, compuesto por textos premiados en concursos nacionales.

En 2006 publicó “Breve Teoría y Práctica del Cuento” en la colección Cuadernillos de Literatura 2005 de S.A.D.E. Delta Bonaerense.

Es coautor junto a Alejandra Murcho de “Un siglo de Literatura Sanfernandina - Diccionario Comentado de Escritores de San Fernando.1900-2004”, publicado en 2005 y reeditado en 2009, y de “Textos escogidos de la literatura sanfernandina – Cuentos”, editado en 2009.

En 2010, "Apuntes para una Biografía de Carlos Enrique Urquía. La vida del poeta Sanfernandino a través de sus libros".

En 2012, Alejandra Murcho publicó la obra que lamentablemente tuvo que terminar sola, "San Fernando de Hoy, Segunda Época Literaria". Este fue el último trabajo en vida junto con ella que llevó a cabo.

También el mismo año, la familia de Hugo Boulocq publicó un libro digital titulado "Breviario de Ensayos", el cual puede descargarse libremente de web de Ocruxaves.
