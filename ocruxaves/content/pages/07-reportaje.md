Title: Reportaje
Date: 2023-02-06
Author: Ocruxaves
Category: Libros
Tags: libros, Hugo Enrique Boulocq, reportaje
Slug: reportaje-hugo-boulocq
Status: published
Cover: images/libro-fondo.jpg



![libros](/images/libros.jpg)



**Reportaje**


**HUGO BOULOCQ EDITOR**

***¿Al ser usted escritor, le molesta al editar textos, tener que explicarle a un colega que a su juicio, su obra no es buena? Digamos que, en algunas ocasiones, tuvo que desempeñar el rol de mecenas y en otras el de verdugo. ¿ No le da miedo descartar a algún futuro fenómeno de la literatura, o simplemente lo toma como un trabajo y se abstiene de opinar?***

En realidad hago una sesuda distinción entre el trabajo editorial y el oficio de escritor. Nunca fui verdugo ni mecenas, sólo doy mi opinión cuando me la solicitan, pero jamás hice valer ninguna autoridad intelectual. Desde ya que me enfrenté muchas veces a situaciones delicadas con autores que se sobrevaluaban; en eso casos –aún hoy- sugiero una edición tentativa de muy pocos ejemplares, porque es verdad que uno nunca puede saber cuándo se encuentra frente a un futuro fenómeno de la literatura. Eso sí, cuando invertí en autores, lo hice con la certeza de su valor literario, como en el caso de la antología “Años de cenizas y escombros” que publiqué en la década del 80 y en la que hizo de antólogo Fernando Koffman, y que hoy es un libro de culto entre los poetas que vivieron los años de la dictadura militar.   

***¿Cómo evalúa el mercado editorial en Buenos Aires teniendo en cuenta que hay varios sellos independientes que hoy están publicando libros? .***

Por lo que sé, el mercado editorial de Buenos Aires maneja los mismos conceptos de las cadenas de comida rápida: poca sustancia alimenticia, mucha grasa y todo a las apuradas. Actores y deportistas devienen en escritores de la noche a la mañana por necesidades editoriales marcadas por el consumo, no por la calidad. Los sellos independientes no tienen ingerencia en el mercado, y los buenos que hubo (como Torres Agüero Editor, que es un caso emblemático) desaparecieron o fueron absorbidos por multinacionales que gozan de balances abultados pero de ningún prestigio.

***¿Qué zona del mercado editorial intenta abarcar el sello editorial que conduce? ¿Tiene en   cuenta el perfil del lector hacia el que van dirigidos sus impresos cuando los edita?***

Como publico textos literarios, los lectores de los libros que edito son por lo general escritores. Asiéndome de la utopía de Italo Svevo, una mitad de los que escriben lee lo que escribió la otra mitad, los que a su vez leen, a su turno, lo escrito por los primeros. El perfil, si lo hay y considerando que las tiradas de ejemplares son muy cortas, es el de lector-escritor.    

***¿Cómo se lleva a cabo la distribución de libros publicados por su sello?, me refiero a si llegan a librerías, se manejan por suscripciones o simplemente el escritor hace su distribución?***

Edito textos literarios y en gran medida poesía. Además y para colmo, de autores desconocidos para el público. No hay distribuidores que se interesen por estos libros y las librerías los exhiben con reticencias. Lamentablemente es el autor quien carga con el peso de hacer conocer su obra. 

***¿Qué características o tendencias ve en las obras que ha publicado y qué tipo de trabajos son los que llegan a la editorial con la esperanza de ser pu blicados, son meramente necesidades personales?***

Hay tendencias de vanguardia y también clásicas, pero un libro siempre responde a una necesidad personal, así sea de fama, de prestigio, de fortuna (que es absolutamente ilusoria) o simplemente de trascendencia, que es lo común: dejar algo que nos sobreviva y que resista el paso del tiempo. La cuestión es que algunos sueñan con trascender de cualquier forma y otros son más cuidadosos con lo que dejarán. El problema de la calidad de los libros o de la cantidad de los libros define claramente a unos y a otros. Pero la trascendencia está siempre presente.   



**HUGO BOULOCQ ESCRITOR**

***¿Dónde nació? ¿Cuál es el origen del apellido Boulocq?***

Nací en Avellaneda, provincia de Buenos Aires, pero mis padres vivían en San Telmo, cerca del parque Lezama. Como nací un 18 de octubre y en los últimos años del peronismo, debió ser uno de los pocos lugares abiertos y con guardia para partos a las 6 de la mañana. El origen de mi apellido es francés, mi abuelo era de Toulouse, se casó con mi abuela que era belga, y vinieron a vivir a San Antonio de Areco, donde aún tengo primos.

***¿Cómo definiría las palabras muerte y vida? ¿En qué orden las pondría?***

Como fui educado en un catolicismo preconciliar, llevo acendrado el concepto de muerte como vida después de la muerte, y el de vida como esperanza de otra vida después de la muerte. Hoy en día, ya muy alejado de aquel catolicismo, creo que la vida es el único tiempo posible y que su importancia es sólo espiritual y su valor solamente subjetivo. La muerte, ahora en segundo lugar, es el único error de estar vivos. Pero mi lucha interna entre ambos conceptos es intensa.

***¿Qué escritor merece toda su admiración y por qué? ¿Qué rasgos reconoce en él?***

Alejo Carpentier. Jamás leí nada igual al “Siglo de las luces”. Su forma de describir el entorno (y las palabras que emplea que hacerlo) es tan vívida y cargada de sensaciones que aún después de muchos años, siento todo lo que leí en ese libro. Para mí es el iniciador del realismo mágico latinoamericano y el maestro inconfesado de García Márquez, Alfonso Reyes, Vargas Llosa, Carlos Fuentes, Juan Rulfo, Miguel Angel Asturias, Mario Arguedas y tantos otros que hicieron el “boom” (estallido) de la literatura latinoamericana.

***¿Por qué escribe?, ¿para llevar a cabo un hecho estético, para transformar la realidad a modo de los escritores "comprometidos" o porque tiene la necesidad de expresarse a través de la escritura?***

Creo que hay más opciones, porque yo escribo para transformar mi propia realidad, pero comprometido sólo con el hecho estético de escribir. Como decía Carlos Urquía: “El poeta debe estar del lado de los pobres, de los esclavizados, de los reprimidos, de los traicionados, debe defender heroicamente la fórmula del pan y la fórmula del amor. Pero pobre de él si espera ser poeta por tales supremas actitudes”. Desde esta perspectiva y como autor de ficción, entiendo que la literatura es arte, artificio, y en definitiva todo es ficción porque el escritor trabaja con imágenes más que con ideas, y las imágenes literarias sólo se construyen con palabras, que es el mayor artificio del ser humano. Y la otra posibilidad que también considero válida es que escribo porque me gusta. Muchísimo.  

***¿Qué piensa de la fama y del ego de los escritores? ¿existe?***

Sí, existe y es un flagelo. La fama no es de por sí mala cuando va acompañada de una obra que la justifique, pero el ego raramente puede ser justificado.

***¿Tiene un estilo definido de escritura propio o semejante al de quien?***

Si bien soy muy crítico conmigo mismo, no puedo afirmar que tenga un estilo personal inconfundible. Lo estoy buscando, por lo menos sé eso.

***¿King, Kundera, Ecco, Cortázar, Borges, Amado, Carpenter, Casares, Capote, Cela, Poe,  y otros, todos ellos sumergidos en una amplia melange de maneras y épocas han podido atraerle hasta el punto de incorporar sus modismos y estilos  en su escritura?***

No, algunos por cuestión de época (escribir hoy con E. A. Poe es prácticamente imposible), otros por cuestión de versatilidad literaria y lingüística (como Ecco), otros por cuestiones de idiosincrasia y cosmovisión literaria (como sería el caso de Camilo Cela y Truman Capote), y que yo sepa nadie incorporó visiblemente a Jorge Amado ni a Bioy Casares ni a S. King; así que descontando mi admiración por Carpentier y mi descuidado respeto por Cortázar, sólo me queda un lugar común: todos tenemos algo de Borges aunque no lo admitamos.

***¿En sus cuentos se hace recurrente la infancia, lugares grises, costumbres obsoletas de la vida cotidiana, están en ellos reflejados parte de su vida o es simplemente imaginario?***

Mis cuentos son imaginarios y no tengo lugares ni momentos de mi vida a los que recurra para hacerlos. Nunca soy yo aunque escriba en primera persona, porque la realidad del cuento es autónoma. Uno puede incorporar obsesiones, las propias contradicciones y hasta los deseos más inconfesados, pero la autobiografía altera las reglas de un género tan exigente como el cuento. En general, la propia biografía es lo primero que debe ir descartándose si uno aspira a ser un buen cuentista.

***¿Tiene influencia en su escritura el modo de vida que le rodea, la sociedad, actual, la familia, sus amigos?***

Sólo me influyen las personas, las que amo, las que no quiero, las que admiro, las que me quieren o me quisieron, las que conozco y las que quisiera conocer. Pero siempre y cuando pueda “ficcionalizarlas”.

***¿Lleva una vida abuguersada, llena de complacencias o se rebela ante el medio social que lo rodea?***

Ni lo uno ni lo otro. Reconozco que llevo la vida que puedo como un contemporáneo más en nuestra sociedad actual, no me creo rebelde ni burgués (quizás nunca lo fui).

***¿Es para usted la escritura un ejercicio o un arte?***

En rigor es el ejercicio de un arte, y por esa razón la considero un oficio. De otro modo no se cumpliría aquello de que para escribir hay que saber escribir bien, por lo menos si uno pretende que lo lean.

***¿Su vocación como escritor en genética?***

No. No llevo genes de escritor y la vocación fue un descubrimiento propio.

***¿Quién influyó en su decisión?***

Los libros. Quienes nacimos con anterioridad al culto de la imagen, pudimos leer más (a lo mejor para no aburrirnos), y yo lo hacía desmedidamente. De todo y a todos los que pudiera. Intentar entrar en esos mundos fabulosos que algunos escritores habían construido con sus libros, fue el primer paso. Los siguientes fueron hacer mis propios fabulosos mundos. Las exigencias y el rigor llegan con el tiempo, hasta que se adquiere un poco de oficio y poco a poco, lentamente, se lo va mejorando.

***¿Se aprende  a escribir con un método o solamente con la imaginación?***

El método y la imaginación tienen un desarrollo posterior a la práctica del oficio de escritor, y si bien son indispensables ambos, para escribir hay que aprender a corregir y a tomar humilde y concienzudamente la crítica de los demás.

***¿El escritor nace o se hace?***

Según lo que ya expresé, es visible que creo que el escritor se hace. Lo único que quizás sea innato es el talento, pero un mal escritor con talento es un verdadero desperdicio.

***Borges decía que la literatura es una forma de la felicidad, por otra parte, muchos escritores se quejan del sacrifico que deben hacer. ¿En tu caso sufres o gozas la escritura?.***

Yo la disfruto mucho, en especial porque puedo “mentir” descaradamente e inventar todo lo que mi imaginación me permita. Pero eso no significa que sea complaciente con lo que escribo.

***También  afirmaba que la literatura no es más que un sueño dirigido. ¿Estás de acuerdo con esta afirmación?***

No, y hasta dudo que el mismo Borges –a la luz de su obra- lo haya creído mucho tiempo. Esa es su enorme gracia y una demostración de su genio: quebrar los límites entre lo falso y lo verdadero, en tanto que la literatura no admite esos valores.

***Me llama la atención como la sociedad genera en los individuos la duda de mostrarse o no como se es. Decidir ser honesto y no mentir ni ocultarse. ¿En la literatura está reflejado? ¿En dónde?***

Creo que la literatura no soporta juicios sociales ni rigores científicos (aunque se lo intentó de mil maneras). Nadie podría llamar a un escritor hipócrita, por ejemplo, haciendo referencia a su obra. La literatura no refleja estas preocupaciones de los individuos porque le son inocuas.

***¿Habla por usted a través de sus personajes, o habla por ellos?***

Escribo cuentos y en ellos predominan los hechos; quizás un novelista pueda contestar mejor esta pregunta. Los personajes de mis cuentos son todo lo imaginarios que puedo, y si no puedo hablo por mí a través de ellos. Pero sé que jamás haría lo que hacen y viven los personajes de mis cuentos.

***¿Puede alguien vivir de la literatura sin necesidad de dejar de lado sus emociones personales? ¿Están impresas ellas en su obra?***

Las emociones son inescindibles de cualquier obra literaria y si bien las personales pueden ocultarse detrás de las palabras, en un cuento afloran como efectos posibles y actúan como contrapeso de otras cargas subjetivas de la historia que se cuenta (como la intelectiva). El misterio más grande es cómo trasladar esas emociones al lector y hacerlo emocionar; para eso es imprescindible saber escribir, y hacerlo muy bien. Y el concepto no es mío, es de Joseph Conrad, quien lo explica muy bien en su prólogo a “El negro del Narcizo”.

***Flaubert pensó que cada cosa sólo puede ser dicha de un modo y que es obligación del escritor dar con ese modo. ¿Compartes esa preocupación?***

Absolutamente. Las palabras no son intercambiables y menos aún, un juego de sinónimos que pueden emplearse arbitrariamente. De todos modos, es curioso que Madame Bovary no sea una cabal demostración de ello.

***Conrad afirmó: “Escribo, es cierto, pero es como sumar un crimen a otro crimen, cada línea es tan odiosa como una mala acción. Soy como un hombre que ha perdido a sus dioses”. ¿Has perdido tus dioses?***

Todos. Lo que no sé si es una ventaja o un consuelo a la hora de escribir. La palabra llega a convertirse en el único dios posible, y en la construcción de la ficción sí, uno es un homicida en serie. Y todo eso a pesar de que fui un fanático lector de Gabriel Marcel, de su teatro y su filosofía. Pero no hubo caso, ni siquiera “El muro” pudo convencerme de que la literatura tuviera un sentido fuera de sí misma.  

***Muchas veces se  condena la crítica literaria en Argentina, según sostienen, ésta “murió hace tiempo”, ahora la crítica es sólo comercial y acomodada según las presiones de las editoriales. ¿Está de acuerdo?***

Creo que siempre fue así, salvo que estemos hablando de Paul Groussac. Hoy ya nadie se asusta de eso. Es un lugar común que forma parte de la corruptela del mercado editorial. Me gustan más los críticos analistas al estilo de Graciela Maturo, tan preparados y tan profundos como es posible suponer.

***Sartre comparó a la crítica con una apuesta. Cuando un libro aparece los críticos apuestan a que es bueno o a que es malo o a que es regular. ¿Cuál es su opinión?***

Esto tiene un poco que ver con la respuesta anterior. Pero el caso de Sastre es singular: siempre los críticos lo maltrataron, y cuando no acepto el Premio Nobel, se convirtió en una especie de tiro al blanco. “La Náusea” no sería lo que es de haber dependido de los críticos literarios franceses. Actualmente, creo que ni siquiera apuestan, porque ya han perdido esa libertad. Creo que los mejores críticos de una obra literaria son los mismos escritores, aunque sus opiniones no cuenten demasiado. Es el caso de “Zama” de Antonio Di Benedetto, tan maravillosamente acogida por Cortazar, Juan José Saer y Noe Jitrik, entre otros, ¿pero cuántos pueden jactarse de haber leído esa novela?

***Cuando Vargas Llosa compara el strip-tease con la novela, dice que al contrario de sus encantos, el novelista lo que exhibe son los demonios que lo atormentan y obsesionan, la parte más fea de si mismo, sus culpas y rencores. ¿Es ese tu caso?***

Pero Vargas Llosa no es cuentista, es novelista. Y lo que dice podría ser suscripto por Sábato, que también fue novelista. En el cuento no hay “strip-tease” posible, porque el cuento es siempre ficción, y la ficción sólo busca cómplices, no espectadores. La vulgaridad que se repite de “hacer el cuento” remite a la esencia de este género: fábula, ficción, fantasía son primas hermanas de la mentira. La enorme virtud de Borges en la cuentística moderna es haber hecho de esa “mentira” un juego. Pero un juego hermoso del que nos hacemos cómplices cada vez que lo leemos.

***A propósito de Vargas Llosa, ¿qué opinión tiene acerca de su Premio Nobel?***

Desconozco los mecanismos de la Academia Sueca y no sé por qué –y por ejemplo- Vargas Llosa es superior a Carlos Fuentes. Los motivos por los cuales se lo dieron bien pueden ser extra literarios, o no, no lo sé. De todos modos, es uno de los mejores y más prolíficos escritores contemporáneos.

***¿Cree que tiene algo en particular la escritura y la edición? o ¿sólo pasa por el arte de escribir en una caso y el otro por un acto comercial?***

La historia de la literatura universal está llena de ejemplos de que detrás de un buen escritor siempre hubo un buen editor. Ecco en “El péndulo de Focault” habla de ello y se mofa claramente de los pseudo editores. Existe sí el negocio editorial, pero es tan genuino como el escritor que vive de la literatura. Y el sentido critico de un buen editor es insoslayable para un escritor que aspire a llegar al gran público. 

***¿Qué le decidió a publicar un diccionario literario?***

El gusto personal de mostrar que en una zona chiquita del Gran Buenos Aires, como es San Fernando, hubo cientos de escritores que no merecían el olvido, y además cierta necesidad de ordenar lo que se mantenía desperdigado, disperso y hasta olvidado. Considero que es bueno dejarle a la posteridad elementos para la investigación y la recuperación del pasado.

***Sé que tiene muchos libros editados y muchos por editar, esa reticencia a hacerlos públicos esconde una vergüenza a la crítica o no cree necesario hacerlo?***

No tengo mucho de lo uno ni de lo otro, apenas publiqué tres libros de cuentos y con los nuevos alguna vez haré otro libro. Y no le tengo vergúenza a la crítica, no soy ni por asomo tan conocido para que alguien se ocupe de mis libros.

***¿Cómo es su proceso de trabajo?***

Absolutamente caótico. Comienzo los primeros párrafos de un cuento sin la menor idea de cómo sigue. En esos primeros párrafos planteo todo lo que debo resolver para que el cuento se convierta en un verdadero cuento, y la mayoría de las veces no sé cómo continuar. Entonces los abandono y los vuelvo a ver dentro de meses, si persiste la incertidumbre los abandono para siempre; en muy contados casos pude terminarlos con algún éxito. Y como no me baso en la realidad  =porque la invento= me cuesta un esfuerzo desmedido encontrar el sentido y el gusto de esa realidad. Personalmente no creo en la inspiración sino en la emoción literaria, que es un fenómeno inexplicable fuera de lo literario. Si esa emoción es excitante, seguramente continuaré con el cuento y podré a su vez emocionar a alguien que lo lea, lo que no me exime de haber escrito piezas mediocres muchas veces.

***¿Cuáles son los escritores argentinos que más le gustan y cuál cree que es el nivel de la literatura argentina?***

María Granata y su libro “Los viernes de la eternidad”, Ernesto Sábato y su “Sobre héroes y tumbas”, el Julio Cortazar de “Todos los juegos el fuego”, Dalmiro Saenz, Osvaldo Soriano, el Jorge Asis de “Carne picada”, Juan-Jacobo Bajarlía -de quien fui amigo y le edité un libro-, Abelardo Castillo; y entre los poetas Jacobo Fijman, Juan L. Ortiz, Alejandra Pizarnik, Francisco Urondo, Alberto Vanasco -de quien también fui amigo- y Juan Gelman.
El nivel de la literatura argentina es de los mejores del mundo. Borges, por ejemplo, es considerado uno de los mejores escritores universales, y las nuevas generaciones de escritores son muy talentosas, baste mencionar a Eduardo Sacheri y “La pregunta de sus ojos”, libro que se transformó en película y tuvo premios muy importantes. 