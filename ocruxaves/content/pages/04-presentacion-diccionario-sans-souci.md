Title: Presentación Dic. Escritores
Date: 2021-08-02 18:21
Author: Ocruxaves
Category: Presentación
Tags: placa, SADE Delta, Hugo Boulocq, Rachel Vivas
Slug: diccionario-escritores
Status: published
Cover: images/libro-fondo.jpg


Video de la presentación del libro de Alejandra Murcho y Hugo E. Boulocq. 
Diccionario Comentado de Escritores de San Fernando.1900-2004, realizada en el Palacio Sans Souci, 2009.


<video width="640" height="480" controls>
  <source src="/images/videos/sans-souci.m4v" type=video/mp4>
</video>