#from loadcsv import *
#PLUGINS = [loadcsv]

AUTHOR = 'ocruxaves'
SITENAME = 'Ediciones Ocruxaves'
SITEURL = ''

PATH = 'content'
#OUTPUT_PATH = '/var/www/html/ocruxaves'

TIMEZONE = 'America/Argentina/Buenos_Aires'

DEFAULT_LANG = 'es'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True


THEME = "/home/hugo/Documentos/attila"
CSS_OVERRIDE = ['images/css/propio.css']

DISQUS_SITENAME = "ocruxaves"

