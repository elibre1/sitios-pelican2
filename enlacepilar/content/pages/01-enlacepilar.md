Title: EnlacePilar
Date: 2022-09-03
Slug: pagina-inicio
Lang: es
Tags: Inicio
Category: Inicio
Summary: Principal
Cover: images/home-bg.jpg
Save_as: index.html

Bienvenidos a EnlacePilar, soporte y asesoramiento informático con Software Libre.  

Nuestra visión de la informática nos acerca más a las comunidades que a las multinacionales y creemos fervientemente que un equipo con el software adecuado, puede durar muchos años, por ende, estamos en contra de la obsolescencia programada.

Vivimos en una época donde todo es descartable y tanto el hardware como el software informático no se encuentran exentos de ello.

Es por ese motivo que desde nuestro lugar ofrecemos servicios a la medida de las posibilidades de cada usuario, sin tener que embarcarse en costosas instalaciones ni renovaciones de equipamiento, salvo que sea estricamente necesario.

**El Software Libre como punto de partida**

Desde EnlacePilar priorizamos el uso de Software Libre, o mejor dicho, lo vemos como la única alternativa posible para una informática mucho más cercana al usuario, que respete sus libertades. 


