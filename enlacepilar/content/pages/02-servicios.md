Title: Servicios
Date: 2022-10-05
Slug: servicios
Lang: es
Tags: Servicios
Category: Servicios
Summary: Servicios
Cover: images/home-bg.jpg


Contamos con una amplia variedad de servicios para el usuario y el pequeño/mediano comercio, entre los cuales podemos destacar:

* Instalación de GNU/Linux en tu equipo.

* Recomendamos la mejor distribución de GNU/Linux que se adapte a tus necesidades.

* Generamos pequeños sitios web estáticos anti obsolescentes para que tengas presencia en internet.

* También, de así requerirlo, gestionamos tu sitio con una conexión a base de datos, desarrollados en marcos de trabajo como Django o Laravel.

* Asesoramos qué hardware te conviene con inversión mínima para recuperar un equipo que en este momento te está funcionando lento.

* Tenemos pequeñas redes informáticas, en tu domicilio o comercio.


**No instalamos Windows, no insista :)** 
