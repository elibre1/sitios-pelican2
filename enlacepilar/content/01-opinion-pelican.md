Title: Mi opinión sobre el generador de sitios estáticos Pelican
Date: 2022-09-02 12:16
Autor: Hugovksy 
Slug: opinion-sitio-pelican
Lang: es
Tags: Pelican
Category: Pelican
Summary: Después de haber migrado de Wordpress a Pelican...


![Logo Pelican](/images/logo-pelican.svg)

Después de haber vivido la experiencia de migrar desde Wordpress, donde tenía un sitio alojado en una Raspberry Pi 4, hacia Pelican, un generador de sitios estáticos con Python como lenguaje base, puedo decir que no está nada pensado para quien no tiene conocimiento alguno, claramente, algún conocimiento y ganas de nerdear tenés que tener, no solo en lo básico para generar el sitio, sino si además querés profundizar y modificar algún tema y ponerlo en castellano... De hecho modificar este que tengo me costó bastante...

Entonces me pongo a pensar: ¿Cuál es la alternativa para el usuario de a pie que solo quiere tener un blog? ¿Sigue siendo Wordpress.com? Ojo, no quiero decir que si no le ponés un poco de ganas no lo sacás, de hecho con 4 comandos lo tenés andando sin personalizar nada, pero al principio entender que content es donde generaba el contenido, valga la redundancia si se me lo permite y  que tenía que usar todo lo que generaba en la carpeta ***output*** aunque pareciera evidente, a mí me costó comprenderlo.

En definitiva, voy a seguir buscando generadores de sitios estáticos, preferentemente hechos con base en Python, que es el lenguaje ligero por excelencia y que me encanta, y a lo mejor puedo llegar a dar una mano ligera reacondicionándolo alguna vez, como con la biblioteca autogestionada guerrística que estoy preparando para el proyecto final de la UNL, pero eso queda para otro tema, colegas.
    
  
