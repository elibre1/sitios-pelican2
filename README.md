# sitios Pelican2



Una bifurcación como a mí me gusta de los sitios que estoy desplegando en casa, en una Raspberry zero 2w. 
Antes estaba en una Raspberry Pi 4 model B, pero era demasiado para los sitios estáticos.

"Lo que hice hasta ahora no fue un error. Fue necesario para llegar hasta donde me encuentro."
