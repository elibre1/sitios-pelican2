$('#tabla_resultados').DataTable
({      
	"bPaginate": true,
    "bLengthChange": true,
    "bFilter": true,
    "bInfo": true,
    "bAutoWidth": true,
	"order": [[ 0, "asc" ]],
	pageLength : 20,
	lengthMenu: [[10, 20, 50, 100,  -1], [10, 20, 50, 100, 'Todos']],
	language: 
	{
	"lengthMenu": "Mostrar _MENU_ registros",
	"zeroRecords": "No se encontraron resultados",
	"info": "Datos del _START_ al _END_ de un total de _TOTAL_ registros",
	"infoEmpty": "Datos del 0 al 0 de un total de 0 registros",
	"infoFiltered": "(filtrado de un total de _MAX_ registros)",
	"sSearch": "Filtrar: ",
	"searchPlaceholder": "por palabra...",
	"oPaginate": 
		{
		"sFirst": "Primero",
		"sLast": "Último",
		"sNext": "Siguiente",
		"sPrevious": "Anterior"
		},
	"sProcessing": "Procesando...",
	},
	//para usar los botones   
	responsive: "true",
	dom: 'Blfrtip',
	buttons: 
	[
	{
		extend: 'csvHtml5',
		text: 'CSV ',
		titleAttr: 'Exportar a CSV (separado por comas, se puede abrir en Excel)',
		className: 'btn btn-success'
	},
	{
		extend: 'pdfHtml5',
		text: 'PDF ',
		titleAttr: 'Exportar a PDF',
		className: 'btn btn-danger'
	},
	{
		extend: 'print',
		text: 'Imprimir ',
		titleAttr: 'Imprimir',
		className: 'btn btn-info'
	}
	]
	
});