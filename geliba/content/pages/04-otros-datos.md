Title: Otros datos
Date: 2023-11-27 20:22
Autor: Hugo
Slug: otros-datos
lang: es


Este trabajo fue presentado el día 02 de diciembre de 2023 en la Sala de Conferencias de la Facultad de Ingeniería y Ciencias Hídricas de la Universidad de Nacional del Litoral, provincia de Santa Fe.

## Documentos para descargar
[Trabajo Final](/images/documentos/00-FINAL-Hugo-Boulocq.pdf){:target='_blank'}


[Presentación ODP](/images/documentos/presentacion-Geliba--FINAL.odp){:target='_blank'}


<hr>

#¿Qué repositorios Componen Geliba? 

1) El proyecto generador de sitios:
[Geliba](https://gitlab.com/enlacepilar/generador-libre-de-bibliotecas-automatizadas){:target='_blank'}

2) El tema del sitio estático para Pelican (generador Python de webs):
[Tema Geliba](https://gitlab.com/enlacepilar/tema-geliba-para-pelican-sitios-estaticos){:target='_blank'}

3) Este sitio web completo para ser descargado desde un repositorio:
[Sitio web Geliba](https://gitlab.com/enlacepilar/geliba-sitio-web){:target='_blank'}

<hr>

#Licencias del proyecto

1) Licencia de Geliba
<a href="https://gitlab.com/enlacepilar/generador-libre-de-bibliotecas-automatizadas/-/blob/master/LICENSE" target="_blank">LICENCIA PÚBLICA GENERAL DE GNU</a>

2) Licencia del contenido de este sitio:
<a href="https://endefensadelsl.org/ppl_deed_es.html" target="_blank">Producción de Pares</a>
