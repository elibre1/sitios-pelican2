Title: Descargá
Date: 2022-09-30 19:26
Autor: Hugo
Slug: geliba
lang: es

**Descargá el script para generar la biblio**
Con este script para GNU/Linux, vas a poder generar tu biblioteca (probado y verificado en distribuciones Debian. Si pudiste hacerlo en alguna otra, ¡no dudes en escribir!): 

<a href="https://gitlab.com/enlacepilar/generador-libre-de-bibliotecas-automatizadas/-/raw/master/instalador-geliba.sh?ref_type=heads&inline=false" class="m-3 p-2 alert-success text-center" target="_blank">Script </a>

Una vez descargado el archivo, andá a tu carpeta "Descargas" y ejecutá en la terminal lo siguiente:

```
sudo chmod 775 instalador-geliba.sh && sudo chmod +X instalador-geliba.sh

```

Una vez que le diste los derechos de admin y ejecución, podrás correrlo de la siguiente manera:


```
./instalador-geliba.sh

```

* Vas a necesitar clave de administrador para poder instalar Python, Tkinter y Apache, en caso de que requieras este último. Es por ello también que todo esto te lo ahorrás dándole los derechos inciales.

* Una vez instalado va a descargar el script de Python para que puedas seleccionar la carpeta donde están tus libros.

* Seleccioná la carpeta y poné abir. Lo recomendable es que tengas todos los PDFs en la raiz de esa carpeta, no en subcarpetas y que estén organizados (por el momento no lee subcarpetas).

* El script va a copiar en descargas, o sea, donde descargaste el script todos los libros en PDF de la carpeta que seleccionaste a una carpeta que se va a llamar **libros**. Además de eso, va a generar un archivo **index.html** para abrir en un explorador como Firefox.

* Si instalaste Apache, copiá la carpeta libros y el archivo index.html generados a /var/www/html

* Si no, llevalo con un dispositivo USB a otro equipo. Se puede ofrecer directamente abriendo el archivo ***index.html*** con cualquier navegador. 

* Si disponés de un router con una red de área local, van a poder ingresar desde otros equipos conectados a ella (se requiere que Apache esté instalado en el equipo anfitrión).
