Title: ¿A quién
Date: 2022-09-30 19:25
Autor: Hugo Boulocq
Slug: a-quien-va-dirigido
lang: es


**va dirigido y qué busca solucionar?**

En principio, GeLiBA está dirigido a usuarios carentes de conocimientos técnicos pero con intenciones de ofrecer una biblioteca de libros digitales en una red local, y que éstos puedan ser descargados libremente.

Este trabajo ofrece una solución sencilla pero efectiva a la creación de una página web con elementos para descargar sin tener que pasar por tediosos tiempos de espera, ni dudosos sitios de descarga, apuntando a compartir en círculos pequeños, comu­nidades amantes de la lectura (objetivo en primigenio) y por qué no, motivar a que se pueda mejorar para otros objetivos, de cualquier índole o necesidad.

## La pregunta de investigación
La pregunta que se formula para este trabajo es ***si resulta posible generar una apli­cación sencilla que pueda ser utilizada por un usuario con conocimientos míni­mos de informática*** y que además, le permita generar una página web de su biblioteca digital. 

## Múltiples posibilidades de aplicación
Geliba está pensado fundamentalmente para ofrecer un sitio web en una red de área local, ya sea desde el sitio donde fue generado o llevándola en un dispositivo USB a otro equipo. 

Un CPU de los denominados ***obsoletos*** puede ser un excelente punto partida para instalar el sitio generado por Geliba.

* Otras personas, de este modo, podrán ir con sus propios dispositivos y descargarse los documentos que se ofrezcan, esa es una alternativa.

* Si se dispone de un router en el lugar donde se encuentre el CPU, otros equipos pueden conectarse a través de la red local y acceder al sitio:
Ejemplo abriendo un navegador en su propio equipo y tipeando la dirección lan: 

```
192.168.0.20

```


      
      
