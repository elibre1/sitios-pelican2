Title: Qué es
Date: 2022-09-30 19:25
Autor: Walter Paulón y Hugo Boulocq
Slug: que-es
lang: es
Tags: que es, script, python
Category: Proyecto

Es una aplicación de escritorio hecha en ***Python con Tkinter***, que permite generar un sitio web (index.html) del listado de la carpeta con la biblioteca digital que el usuario elija (en PDF, Epub, etc). 

La idea es poder compartir este sitio en una red de área local (LAN por sus siglas en inglés). El resultado se trata de una página web estática, anti ob­solescente, es decir,  que se pueda “levantar” en cualquier computadora con un servidor web instalado como Apache, Nginx, Hiawatha, etc.

El resultado final como se menciona arriba es un archivo **index.html** (con un diseño minimalista) y la copia  del contenido de la carpeta elegida a un directorio en la misma ubicación que el index mencionado. El script creará una carpeta que se llamará “libros” y contendrá todo lo que la aplicación de Python haya leído desde la ubicación que se haya elegido como origen de los documentos.

Resumiendo: 

* La aplicación GeLiBA crea un sitio estático con el contenido de los libros o documentos del directorio elegido de la computadora.

* Este directorio es copiado a la carpeta "libros", creada por GeLiBA. A su vez, crea una página web para poder ver ese listado, buscar documentos por nombres, exportar el listado a un archivo PDF, etc 

* Es importante entonces que los documentos posean un tíulo legible, ya que de ello dependerá el éxito de la "legibilidad" del sitio. (De nada servirá que un documento se llame "libro.pdf", que no indicará de ninguna forma a qué se está refiriendo el enlace propuesto).

* Luego de ese resultado, el usuario podrá decidir qué hacer con ello, como se detalla­rá en siguiente publicaciones.

Ejemplo de la página web generada con Ge.Li.B.A.

<img src="/images/geliba_v2.jpg" class="img-fluid" alt="Ejemplo GeLiBA">


<h3><a href="https://biblioteca-enlacelibre.enlacepilar.com.ar/" target="_blank">Ejemplo del sitio (hacé click acá) - Biblioteca EnlaceLibre</a></h3>


**GELIBA ES SOFTWARE LIBRE**

Esto quiere decir que podés descargarte el script y:

* Utilizarlo para cualquier propósito.

* Ver cómo funciona chusmeado el código y modificarlo como quieras.

* Compartirlo con quien quieras.

* Compartir tus modificaciones del mismo modo.


El repositorio en Gitlab contiene información actualizada y la evolución del proyecto:

[https://gitlab.com/enlacepilar/generador-libre-de-bibliotecas-automatizadas](https://gitlab.com/enlacepilar/generador-libre-de-bibliotecas-automatizadas)

## Video explicativo:

<video src="/images/Geliba-presentacion.mp4" width="420" height="340" controls></video>

<br>
<hr>
<br>

# Cuándo fue creado GeLiBA 
El proyecto fue creado en septiembre de 2022 y expuesto como Trabajo Final de la Tecnicatura Universitaria en Software Libre, de la Universidad Nacional del Litoral, el día 2 de diciembre de 2023.

