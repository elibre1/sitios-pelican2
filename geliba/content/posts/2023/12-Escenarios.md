Title: Escenarios como ejemplo
Date: 2023-06-14 21:22
Autor: Hugo Boulocq
Slug: escenarios-posibles
lang: es
Tags: escenarios
Category: Escenarios
Summary: Una aproximación a lo que podría llegar a ser un escenario donde se podría llegar a aplicar el script de GeLibA
Imagen: /images/logo1.png

Imaginemos por un momento el siguiente escenario donde podría llegar a aplicarse Geliba. Un lugar con poca o escasa conectividad, una zona rural donde lamentablemente por diversas circunstancias internet llega en forma deficiente o no llega directamente, una escuela rural podría ser. Con un simple módem wifi de cualquier índole podríamos tener una lan funcionando en ese lugar, pero careceríamos de salida al exterior. Es allí donde GeLibA podría ser útil, si por ejemplo un maestro o profesor llevase previamente en un dispositivo USB el sitio generado con el aplicativo y lo pusiera en un equipo, cualquier equipo,disponible para descargar. Eso sería un caso, pero si como dijimos, pudiésemos disponer de un modem y previamente haber instalado apache, este sitio podría accederse desde la red de área local con la dirección IP del equipo en cuestión. 

Entonces con un celular conectado a esta red podría accederse por navegador a este equipo y se podría descargar los archivos ofrecidos en el sitio web generado con la aplicación.
