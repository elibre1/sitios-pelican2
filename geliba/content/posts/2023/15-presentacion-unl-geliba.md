Title: Presentación como proyecto final de GeLiBA en la UNL
Date: 2023-12-03 17:50:03.319208
Autor: Hugo 
Slug: presentacion-unl-geliba
Lang: es
Tags: Presentación, Geliba, UNL
Category: Presentación
Summary: Este sábado 2 de diciembre de 2023 tuvo lugar la presentación de Geliba en la UNL...  
Imagen: /images/logo1.png


Este sábado 2 de diciembre de 2023, en la Sala de Conferencias de la Facultad de Ingeniería y Ciencias Hídricas de la Universidad Nacional del Litoral, tuvo lugar, como proyecto final de la Tecnicatura en Software Libre que se dicta en dicha institución, la defensa de este proyecto Ge.Li.B.A.

Estuvieron presentes el coordinador de la carrera, Cristian Quinteros, los tutores Mariana Ramat y Federico Ternavasio y el profesor Juan Pablo Taulamet.


<div class="text-center">
    
<img src="/images/03-UNL-2-12-23-mini.jpg" class="img-fluid" alt="UNL">


</div>


##Descargar archivo de Diapositivas ODP 


<a href="/images/documentos/presentacion-Geliba--FINAL.odp">ODP presentación GeLiBA</a>