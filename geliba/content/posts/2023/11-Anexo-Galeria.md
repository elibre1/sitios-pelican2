Title: Anexo: Galería de fotos con Geliba
Date: 2023-01-13 10:32
Autor: Hugo Boulocq
Slug: galeria de fotos
lang: es
Tags: galeria, fotos, imagenes
Category: Galería
Summary: Generando una galería de fotos con GeLiBA
Imagen: /images/logo1.png


Como ejemplo y mostrando una de las tantas alternativas posibles que se pueden llegar a generar con la aplicación de Geliba, mostramos a continuación una modificación en el código para que en vez de ofrecer un listado de libros para descargar, se pueda crear un index con una tabla con fotografías en miniatura, permitiendo acceder haciendo clic en las mismas a su tamaño original. Este proceso se realiza con el módulo de Python **Pillow**, el cual es posible que deba ser instalado de la siguiente manera, a través de la consola:

```
pip3 install pillow

```

El script de la galería se puede descargar de acá:

[Script galería de fotos](geliba-galeria-fotos.py){:target="_blank"}

![Ejemplo](/images/galeria-geliba.png){:.img-fluid}


Y haciendo clic aquí abajo podés ver cómo quedaría terminado:


[Ejemplo de galería](/images/documentos/Ejemplo-galeria-fotos/){:target="_blank"}
