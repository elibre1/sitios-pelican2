Title: Presentación GeLiBA en la Universidad Nacional del Litoral
Date: 2023-11-28 18:33:38.686812
Autor: Hugo 
Slug: presentacion-unl
Lang: es
Tags: Presentación, UNL, Exposición
Category: Presentación
Summary: El día 2 de diciembre en la UNL tendrá lugar la defensa del trabajo final y presentación de GeLiBA  
Imagen: /images/logo1.png


La exposición se realizará el  día 2 de diciembre del 2023 y tendrá lugar a las 9:00 horas en la Sala de Conferencias de la Facultad de Ingeniería y Ciencias Hídricas.

<div class="text-center">
    
    <img src="/images/UNL1-mini.jpg" class="img-fluid" alt="UNL">
<br>
<br>

    <img src="/images/logo.png" class="img-fluid mt-2" alt="UNL">
<br>
<br>

    <img src="/images/UNL2-mini.jpg" class="img-fluid mt-2" alt="UNL">

</div>


<br>

<hr>

<br>

## Documento en PDF para descargar

[PDF Trabajo Final GeLiBA](/images/documentos/00-FINAL-Hugo-Boulocq.pdf)
