Title: Versión 2 de Geliba en marcha
Date: 2023-08-26 18:39
Autor: Hugo Boulocq
Slug: version2-geliba
lang: es
Tags: versiones
Category: versiones
Summary: Nueva versión de Geliba, con algunas (bastantes) modificaciones estructurales en el código y separación por módulos. Más detalles en esta publicación...
Imagen: /images/logo1.png

Hemos lanzando una nueva versión del Script GeLiBA, a partir de unas sugerencias más que bienvenidas del profe de Tecnologías Web, Juan Pablo Taulamet que permiten ahorrar en confusión y desde esa puntra de lanza permitieron extendernos un poco en el resultado final del script, para que sea más entendible para quien quiera utilizarlo. Si bien la idea cuando uno va avanzando es extender funciones y más funciones, a la larga en este tipo de proyectos pueden llegar a perjudicar y difuminar el objetivo final, que no es más que ofrecer un sitio web para alojarlo en una Pc con pocos requisitos y sin salida a internet, por ejemplo en una red de área local (LAN por las siglas en inglés).

Es por eso que hicimos algunas modificaciones estructurales, que son las siguientes:

* Separar el código en módulos para que sea más claro para quien quiera modificarlo y por qué no, para mí también (desarrollador) a la hora de volver a él.

* Mejorar la visualización de la ayuda gracias a unos retoques.

* Incorporar elementos estáticos (este es el cambio más notorio a nivel usuario), como Datatables y Bootstrap4, que facilitan la búsqueda y visualización del resultado, ya el que sitio generado incorpora un pequeño buscador interno y un paginador gracias al elemento Datatables.

* Se eliminó la subida por FTP a este sitio web para evitar confusiones sobre el objectivo principal

* Se han separado en carpetas, tanto lo que descargamos con el instalador_geliba como el sitio web generado, para que resulte más comprensible qué copiar y llevar en un pendrive a la PC que se desee.

* Se ha modificado la forma en que se incopora el contenido al resultado final. Partimos ahora de un i*index_base.html* donde se escribe el contenido resultante, se renombra y deja listo para navegar como *index.html*

* Se ha modificado el instalador para que baje directamente del repositorio el script de GeLiBA con todos sus elementos. Aún se está puliendo en detalle pero por el momento es plenamente funcional.

Además de ello, el Readme del repositorio fue mejorado para una lectura más amena y puntual, con referencias más claras a la utilización e instalación. 
En el repositorio se han separado en carpetas la versión 1 (V1) y versión 2 (V2) para poder acceder en caso que así se desee.

De más está decir que toda colaboración es bienvenida. ¡GeLiBA es Software Libre!

[Repositorio](https://gitlab.com/enlacepilar/generador-libre-de-bibliotecas-automatizadas)

[Instrucciones para descargar](https://geliba.enlacepilar.com.ar/pages/geliba.html)


