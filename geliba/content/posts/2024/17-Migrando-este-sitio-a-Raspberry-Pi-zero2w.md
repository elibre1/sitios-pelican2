Title: Migrando el sitio de una Raspberry Pi 4 a Raspberry Pi Zero 2w
Date: 2024-07-09 20:10
Autor: Hugo 
Slug: migracion-a-raspberry-2w
Lang: es
Tags: Raspberry, Zero 2w, Geliba
Category: Migración
Summary: Actualmente este sitio se encuentra migrado de una Raspberry 4 a una Zero 2w  
Imagen: /images/logo1.png

##El motivo de la migración

Actualmente este sitio estático no necesita grandes prestaciones de hardware para operar y de hecho una Raspberry Pi 4 Model b con 4GB de RAM tenía características sobradas, por lo cual quise experimentar adquiriendo una de inferior rendimiento, una Raspberry Pi Zero 2W, la cual es una monoplaca con menos posibilidades pero no por ello no preparada para este proyecto:

<p align="center">
  <img src="https://assets.raspberrypi.com/static/51035ec4c2f8f630b3d26c32e90c93f1/2b8d7/zero2-hero.webp" width="200" alt="Raspberry Pi 2W">
</p>

De [este enlace](https://www.raspberrypi.com/products/raspberry-pi-zero-2-w/) podemos extraer las siguientes características:


* 1GHz quad-core 64-bit Arm Cortex-A53 CPU
* 512MB SDRAM
* 2.4GHz 802.11 b/g/n wireless LAN
* Bluetooth 4.2, Bluetooth Low Energy (BLE), onboard antenna
* Mini HDMI® port and micro USB On-The-Go (OTG) 
* port microSD card slot
* CSI-2 camera connector
* HAT-compatible 40-pin header footprint (unpopulated)
* H.264, MPEG-4 decode (1080p30); H.264 encode (1080p30)
* OpenGL ES 1.1, 2.0 graphics
* Micro USB power
* Composite video and reset pins via solder test points
* 65mm x 30mm

Si comparamos la Zero 2W con la Rasberry Pi 4 model B las evidencias saltan a la vista en favor de la última. Sin embargo en este tiempo, que no fue mucho pero suficiente, cerca de un mes, no he observado grandes dificultades, por el contrario, responde muy bien.

Como sistema operativo le instalé en una tarjeta vieja que tenía en casa de 16GB un Raspbian sin entorno gráfico, donde además alojo 2 sitios más bajo el protocolo HTTP y uno sobre el protocolo Gemini.

El desempeño es más que aceptable y recomendable para promover el autoalojamiento de vuestros sitios y presentar batalla contra el cómputo que exige cada día más sin necesidad alguna. Como vemos se puede informar o presentar proyectos con ínfimos recursos.

