Title: Modificación del tema GeLiBA para Pelican, sin JS
Date: 2024-07-01 08:49
Autor: Hugo 
Slug: nuevo-tema-geliba-sin-js
Lang: es
Tags: Temas, Jinja2, Geliba
Category: Temas
Summary: Modifico el tema existente Geliba, para el generador de sitios estáticos Pelican de Python, pero esta vez prescindiendo de Javascript  
Imagen: /images/logo1.png

##¿De qué se trata?

Es un tema creado (casi) desde cero para el generador de sitios estáticos Pelican. Pero como nada nace de la nada, claro está, basé mis configuraciones en los temas [brutalist](https://github.com/mc-buckets/brutalist/tree/de551620221ec3f1958250adfaffbbc81e9b748c)y [bootlex](https://github.com/getpelican/pelican-themes/tree/master/bootlex), los cuales se encuentran en el repositorio oficial de Pelican Themes.
En esta bifuración prescindo de Javascript por muchas cosas, pero por sobre todas ellas porque JS es malo, malo, je; mejor decir que ya no es lo que era antes, un inofensivo modificador el DOM que mostraba ventanas emergentes y te cambiaba los colores.
  Mejor es que leas [esta publicación](https://www.gnu.org/philosophy/javascript-trap.es.html). A mí me gustó mucho y me dio la motivación final para seguir por este camino.

De más está decir que se puede probar, usar, modificar y volver a compartir ppara tus sitios web generados con Pelican

Podés ver el repo y descargarlo de acá:
[Tema Geliba sin JS](https://gitlab.com/elibre1/tema-geliba-para-pelican-sitios-estaticos-sin-js)
