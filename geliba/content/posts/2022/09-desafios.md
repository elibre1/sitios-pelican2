Title: Principales desafíos
Date: 2022-10-31 18:25
Autor: Hugo Boulocq
Slug: desafios
lang: es
Tags: desafíos
Category: Desafíos
Summary: Principales desafíos
Imagen: /images/logo1.png


El resultado de este trabajo, como bien se ha hecho mención desde un inicio, es intentar acercar una pequeña colaboración del lado del Software Libre para brindar un script que solucione un inconveniente para quien carece del conocimiento para resolverlo, está incursionando en desarrollo y quiere ver de qué va este trabajo o simplemente se ha topado con este proyecto y lo quiere probar, para que pueda listar sus documentos, los cuales desde ya también se sugiere fervientemente que posean licencias libres para evitar cualquier tipo de perjuicios futuros.

Existen antecedentes como los que se han listado aquí de gran calidad y con resultados tecnológicamente mejores. Este proyecto no buscar competir con ellos, si no ofrecer una alternativa mínima, sencilla y aplicable para cualquier tipo de usuario. 

Creo, para concluir, que responde a la pregunta inicial que ofició de disparador, y es que cualquier persona puede usarlo y con muy poco se puede lograr un resultado práctico, rápido y que le permita hacerlo disponible en diferentes entornos.
