Title: Etapas del proyecto
Date: 2022-10-27 17:45
Autor: Walter Paulón y Hugo Boulocq
Slug: etapas
lang: es
Tags: etapas
Category: Etapas
Summary: Etapas del proyecto
Imagen: /images/logo1.png

### Etapa 1: A quién va dirigido, cómo realizarlo
Elaborar un bosquejo del proyecto (ya presentado) sobre el alcance de la  aplicación a crear. Lenguaje a utilizar. División del trabajo con mi compañero Walter Paulon. Como el proyecto va a estar diferenciado en 2 ramas: autohosteo y despliegue en hosting gratuito, efectuar búsquedas e info sobre las posibilidades de esta última opción. 
Primeros esbozos del script con Tkinter para la generación del Html con enlaces a los libros para descargar. Prueba en un hosting gratuito (Netlify) realizando despliegue. 
Tiempo aproximado: un mes. 

### Etapa 2: Empezando a pulir el desarrollo del script
Una vez verificado que el proyecto se encuentra en funcionamiento correctamente en un hosting gratuito, comenzamos a trabajar en el desarrollo de script automatizador de Python, puliendo detalles visuales y simplificando la puesta en marcha por parte del usuario. 
Como primera medida tomamos el punto de partida la rama 1, lo cual implica generar un script bash que se encargará de realizar las siguientes verificaciones:
- Comprobar si está Python instalado, caso contrario instalar
- Comprobar si está Tkinter instalado, caso contrario instalar
-Comprobar si está Apache instalado, caso contrario instalar.
-Descarga del script de Python “Generador de Bibliotecas Libres Autoinstalables”
Tiempo aproximado: 1 mes.

### Etapa 3: Preparación de la documentación instructiva para rama 2: “despliegue en hosting”
Empezamos a preparar una documento auxiliar en PDF para el despliegue en un hosting, lo cual implica:
-Indicaciones para generar un repositorio Gitlab.
-Capturas de los pasos.
-Generar cuenta en Netlify.
-Desplegar en el sitio.
-Capturas de pantalla de todos los procesos.
Tiempo aproximado: 15 días

### Etapa 4: Preparación de la documentación instructiva para rama 1: “autohosteo”
Generamos el documento auxiliar en formato PDF para el guiar al usuario en temas clave para que el autohosteo sea exitoso. Para ello se tomarán en cuenta los siguientes parámetros
-Verificar que el usuario tenga ip pública disponible
-Configuración de la ip fija del equipo.
-Ingresar en el router si es posible y apuntar DMZ a la ip del equipo.
-Capturas con los procesos.
Tiempo aproximado: 20 días

### Etapa 5: Despliegue de prueba final. Repositorio disponible. TP con los procesos realizados.
Tiempo aproximado: 1 mes.
