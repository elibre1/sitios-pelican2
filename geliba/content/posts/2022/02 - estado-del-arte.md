Title: Estado del Arte
Date: 2022-09-30 19:29
Autor: Walter Paulón y Hugo Boulocq
Slug: estado-del-arte
lang: es
Tags: estado
Category: Estado
Summary: Estado del arte
Imagen: /images/logo1.png

### ¿Ya se intentó solucionar este problema o abordar esta situación desde el software libre?. 

Encontré un proyecto muy interesante que se llama <a href="https://copiona.com/proyectos/2020/05/05/bibliobox.html" target="_blank">Bibliobox</a>


<a href="https://cybercirujas.rebelion.digital/foro/viewtopic.php?t=27" target="_blank">O este: </a>

<a href="https://github.com/elKaZe/biblioteca-guerrilla" target="_blank">O este:</a>

O este mucho más extremo, que es un simple pendrive empotrado en la pared, pero ya es <a href="https://deaddrops.com/" target="_blank">otra historia</a>

### ¿De qué manera?
El mejor preparado es sin dudas el de Bibliobox, que intenta solucionar el problema de una biblioteca digital móvil, ya que se encuentra montada en una Raspberry, y con posibilidad de conectarse desde cualquier lado si se la adjunta una celda de carga.

### ¿Qué estrategias implementaron?
En el caso mencionado en el punto anterior los pasos fueron los siguientes. 
-Primero, instalar Calibre y catalogar todos los libros disponibles. 
-Después se genera el sitio estáitco clonando el repo de biblioteca-guerrilla, que a su vez copia los datos de la base de datos de Calibre
-Pasar todo a la Raspberry
-Generar el punto de acceso en la misma.

<a href="https://bibliobox.copiona.com/posts/biblio-box.html" target="_blank">Enlace </a>

**¿Funcionaron satisfactoriamente?**

Es uno de los casos más originales que encontré, de hecho hay una nota que lo menciona en <a href="https://radioslibres.net/crea-tu-biblioteca-digital-portatil/" target="_blank">Radios Libres </a>

