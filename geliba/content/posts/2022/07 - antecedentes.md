Title: Antecedentes del proyecto
Date: 2022-10-27 17:55
Autor: Walter Paulón y Hugo Boulocq
Slug: antecedentes
lang: es
Tags: antecedentes
Category: Antecedentes
Summary: Antecedentes del proyecto
Imagen: /images/logo1.png

Los antecedentes a los que aquí se van a hacer referencia tienen que ver con 4 proyectos disponibles en internet. Veremos a continuación, en base a los datos recabados y la información provista, si con estas herramientas a las que pude acceder en base a búsquedas en la web, era posible fácilmente llegar a un objetivo acorde al proyecto, esto es, ofrecer una biblioteca simple digital generada por un usuario (o similar) sin demasiados conocimientos sobre el manejo de una consola de GNU/Linux o sobre desarrollo orientado a páginas web. Cabe aclarar que si bien los principios técnicos de estos proyectos no son completamente iguales, ofrecen cierto resultado que se acerca al objetivo de la pregunta de investigación,  la cual se trata de ofrecer sin pasos complejos un listado de libros digitales que pueda ser descargado por alguien más, en cualesquiera que sean sus condiciones, es decir, desde una computadora personal conectada a internet, desde un teléfono celular conectado a una red lan, etc. 


Las herramientas analizadas como antecedentes son las siguientes:

<a href="https://github.com/elKaZe/biblioteca-guerrilla" target="_blank">Biblioteca Guerrilla</a>

Este proyecto genera a partir del programa Calibre, una aplicación de escritorio gesto­ra de bibliotecas digitales, un catálogo web de los libros que el usuario haya preselec­cionado en este programa. En principio hemos de decir que este sistema no presenta mayores inconvenientes para generar el sitio, siempre y cuando se posea algún cono­cimiento sobre clonar repositorios y modificar algún código extra, además de tener or­ganizada correctamente la biblioteca en Calibre con sus tapas correspondientes.

![Biblioteca Guerrilla](images/01-guerrilla.jpg){:.img-fluid}


<a href="https://bibliobox.copiona.com/posts/biblio-box.html" target="_blank">Bibliobox</a>

Ofrece un paso más allá de la anterior Biblioteca Guerrilla. Se trata de generar el resultado tal cual se menciona en el instructivo del anterior proyecto, agre­gando la posibilidad de “embeberlo” en una Raspberry Pi (en el instructivo sugiere a partir de su versión número 3) y generar a partir de un script una conexión de red lan disponible, lo cual implica portabilidad, siempre y cuando dispongamos de una celda de carga, claro está, u otro dispositivo portable generador de la energía requerida para operar.

![Bibliobox](/images/02-bibliobox.jpg){:.img-fluid}

<a href="https://github.com/gcoop-libre/letras_viajeras/" target="_blank">Letras Viajeras</a>

Como reza en su repositorio de internet, es una iniciativa de la Dirección de Bibliote­cas y Promoción de la Lectura del Gobierno de la Provincia de Buenos Aires, que per­mitirá a los pasajeros de larga y media distancia tener acceso gratuito a libros digita­les.
Es un trabajo realizado por la Cooperativa de Software Libre Gcoop, argumentando que “La solución consiste en un Router Inalámbrico al que se le instala OpenWRT, un Firmware libre basado en el kernel Linux”.


![Bibliobox](/images/03-letras.jpg){:.img-fluid}

<a href="https://deaddrops.com/es/" target="_blank">Dead Drop</a>

Este proyecto, creado en 2010 por el artista alemán Aram Bartholl, fomenta el inter­cambio de archivos entre usuarios anónimos sin necesidad de conectarse a Internet (un P2P fuera de línea). El objetivo es muy simple: empotrar un pendrive en la pared dejando el conector visibile, de manera tal que cualquiera con una conexión usb pue­da acceder al mismo con algún dispositivo que posibilite esta conexión.

![Dead Drop 1](/images/04-dead-drop.jpg){:.img-fluid}

![Dead Drop 2](/images/04b-dead-drop.jpg){:.img-fluid}
