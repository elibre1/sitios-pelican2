Title: ¿Por qué automatizada y qué idea o ideas persigue el proyecto? 
Date: 2022-10-27 17:35
Autor: Walter Paulón y Hugo Boulocq
Slug: por-que-automatizada
lang: es
Tags: automatizada
Category: Características
Summary: ¿Por qué automatizada y qué idea o ideas persigue el proyecto? 
Imagen: /images/logo1.png

Porque con un script visual, que no deja de ser un archivo con unas pocas líneas de código, intenta generar un documento HTML leyendo una ruta que el usuario asigne sin que requiera mucho esfuerzo por parte de éste. El proyecto está a favor de una computación de bajo consumo, ya que podría desplegarse en una simple Raspberry Pi instalada en su hogar. Si no se cuenta con un equipo de este estilo puede ser un CPU, si bien no de bajo consumo, de los considerados “obsoletos”, como por ejemplo un Athlon 2500 de más de 10 años de antigüedad. La inspiración sobre estos temas surge en parte de la lectura de estos artículos escritos por miembros del grupo en in­ternet denominado [“Cybercirujas”](https://cybercirujas.sutty.nl/textos/2022/05/24/por-qu%C3%A9-peleamos-por-una-computaci%C3%B3n-de-bajo-consumo.html){:target="_blank"}