Title: Acerca de este proyecto
Date: 2022-09-30 19:29
Autor: Walter Paulón y Hugo Boulocq
Slug: acerca-de-este-proyecto
lang: es
Tags: presentacion, script, biblioteca
Category: Proyecto
Imagen: /images/logo1.png


## ¿Cómo va a estar diagramado?

Vamos a dividir la descripción en 2:

1) Generación de la página web biblioteca de lucha anti obsolescencia :)

2) ¡Plantar bandera con tus libros en internet!

Para el punto 1, vamos a necesitar:

* Tener instalado Python en la distro. Generalmente viene por defecto en casi todas las distribuciones.

Para el punto 2, o sea, donde vamos a ofrecer el sitio.

a)  Verificar que tenés salida a internet, o sea, que tu ip de internet sea verdaderamente una ip pública y que esté en el rango de 100.x.x.x. 
Esto último querrá decir que tu proveedor de internet te tiene detrás de lo que se  llama CG-NAT, o sea que usa un Switch donde divide la ip publica entre varios usuarios.

<a href="https://www.cual-es-mi-ip.net/" target="_blank">Cuál es tu Ip pública.</a>

Hay posibilidades de <a href="https://www.sdos.es/blog/ngrok-una-herramienta-con-la-que-hacer-publico-tu-localhost-de-forma-facil-y-rapida" target="_blank">saltarse esto :) </a>


Sin embargo, lo recomendable sería que busques un sitio que te permita subir tu web estática gratuitamente y tus libros como podría ser:
<a href="https://www.netlify.com/" target="_blank">Netlify</a>


b) Tener instalado un servidor web, como puede ser Apache o Nginx
En esa PC ejecutamos:

```
sudo apt install apache2
```

## Tenemos presente que el proyecto debe:

* Promover el uso de software libre mediante distintas estrategias.

```
Lo hace, porque está hecho en Python y está dirigido a equipos que usen GNU/Linux
```

* Articular los distintos tipos de saberes y habilidades adquiridas durante la carrera. (La orientación o trayecto elegido no es limitante para el tipo de trabajo que se quiera realizar).

```
He reunido saberes que tienen no sólo con lo técnico como los adquiridos en Introducción al Desarrollo de Software, Programación 2, Tecnologías Web, Administración Linux I, si no también de materias orientadas a lo social como Culturas Digitales Libres o Comunidades de Software Libre.
```
   
* Ser factible de implementar en el corto o mediano plazo.

```
Es factible ya que el proyecto en sí no es complejo y sin dimensiones desproporcionadas. Sí tengo en claro que tiene que estar, aunque sencillo, bien diagramado para que el usuario de a pie pueda realizarlas.
``` 

* Tener relevancia social, debe ofrecer una respuesta o intentar resolver algún problema, interés o dificultad concreta.

```      
Considero que lo que intenta resolver es, con la creación sencilla de un sitio web, poder compartir y acercar a otro cultura a través de libros digitales.
```

* Debe estar dirigido a una institución (pública o privada),organización de la sociedad civil, o de forma más general, a un sector de la sociedad.
      Como menciono arriba, está dirigida a personas que no poseen demasiados conocimientos técnicos pero que sí desean tener su página web autogestionada.

* Quedar disponible para su difusión bajo licencia libre.

```
Mi idea es liberarla bajo la licencia de 
```
<a href="https://endefensadelsl.org/ppl_deed_es.html" target="_blank">Producción de Pares</a>

      
      