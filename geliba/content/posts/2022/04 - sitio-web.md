Title: ¿Dónde se encuentra alojada esta web?
Date: 2022-10-03 17:52
Autor: Walter Paulón y Hugo Boulocq
Slug: sitio-web
lang: es
Tags: disponibilidad
Category: Alojamiento
Summary: Alojamiento del proyecto
Imagen: /images/logo1.png

Este sitio está alojado en una Raspberry Pi 4, una microcomputadora hogareña, favoreciendo así el autohosteo, también pilar no menos importante que tiene que ver con todo lo que pregona el proyecto de GeLiBA, es decir, ser anti obsolescente, escapar de las grandes infraestructuras y abogar por un internet de bajo consumo y baja tecnología.

El dominio principal es <a href="https://geliba.enlacepilar.com.ar/" target="_blank">https://geliba.enlacepilar.com.ar/</a>, pero si por la razón que fuere este no se encontrara funcionando (puede pasar por diversos motivos), existe un espejo creado para razones extraoridnarias al que se puede acceder de la siguiente manera:

<a href="https://geliba.netlify.app/" target="_blank">https://geliba.netlify.app/</a>


## Repositorio
Para hacerle modificaciones, contribuir, mejorar en tu propio proyeecto, etc:
<a href="https://gitlab.com/enlacepilar/generador-libre-de-bibliotecas-automatizadas" target="_blank">Repo GeLiBA</a>


