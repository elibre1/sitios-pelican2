Title: Posibles aplicaciones de este trabajo
Date: 2022-10-27 18:25
Autor: Walter Paulón y Hugo Boulocq
Slug: aplicaciones
lang: es
Tags: aplicaciones
Category: Utilidades
Summary: Posibles aplicaciones de este trabajo
Imagen: /images/logo1.png


Partiendo de estas 4 herramientas anteriormente citadas, se buscó simplificar el re­curso, con la finalidad de hacer disponible la herramienta para que se encuentre al al­cance de la mejor forma de posible (un sitio web estático) y que personas con esta in­quietud puedan acceder a un resultado mediante un procedimiento sencillo.


Es preciso aclarar que algunos pasos no se pudieron obviar y en principio para ejecu­tar el script bash que se encargará de todo hará falta del uso de la consola, invaria­blemente. Si bien el usuario podrá hacer lo que desee con el resultado final, que no es más que un simple archivo index.html que enlaza con su carpeta de libros en PDF u otro formato disponible (esto no es limitante, pueden ser inclusive accesos a foto­grafías, archivos de música o lo que se encuentre en la carpeta seleccionada), se ofrecen documentos que permiten al usuario hacer disponible su sitio en internet des­de diversos dispositivos y escenarios, por ejemplo una Rasperry Pi, un equipo viejo que pudo haber sido “resucitado” con un sistema operativo GNU/Linux preparados para PCs de bajos recursos de hardware, apoyando principalmente el autohosteo, o bien si ello no fuera posible, subiendo el recurso a un hosting gratuito.   


Si se dispusiera y optara por el autohosteo en una Raspberry Pi, es preciso aclarar que con esto también se reduce el consumo energético al mínimo, cumpliendo con el objetivo, además, de lograr una informática de bajo consumo para estos fines.


Desgraciadamente para este autohosteo y posterior salida a internet será necesario como se dijo al principio de este trabajo de una IP que ofrezca salida a internet, ya que algunos proveedores, debido a la limitada cantidad de direcciones Ips disponi­bles, ofrecen su servicio detrás de un dispositivos que “enmascaran” su dirección visi­ble 
