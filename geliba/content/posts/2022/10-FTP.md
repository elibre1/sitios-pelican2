Title: Subida opcional por FTP
Date: 2022-12-25 21:33
Autor: Hugo Boulocq
Slug: ftp
lang: es
Tags: ftp, subida
Category: FTP
Summary: Subir por FTP tu proyecto a Geliba
Imagen: /images/logo1.png


## YA NO SE ENCUENTRA EN FUNCIONAMIENTO

Ahora con la aplicación Python GeLiBA es posible subir por FTP a este sitio tu página de libros generada, en forma opcional y a modo de complemento por si estás interesado en ver inmediatamente publicada tu lista de libros.

<h2>¿Cómo funciona?</h2> 

Es sencillo, luego de generar la página de libros, te va a aparecer una ventana preguntando si deseás subir por FTP tu página con los libros. Si contestás afirmativamente los sube en forma automática a este sitio, con la dirección que hayas puesto como nombre cuando te lo pregunte, es decir
*** https://geliba.enlacepilar.com.ar/libros_externos/tu_nombre ***




