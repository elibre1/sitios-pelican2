AUTHOR = 'enlacepilar'
SITENAME = 'Generador Libre de Bibliotecas Automatizadas'
SITEURL = ''
ATTRIBUTION ='compartir igual'

YEAR_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/index.html'

ARTICLE_PATHS = ['posts']
ARTICLE_SAVE_AS = 'posts/{date:%Y}/{slug}.html'
ARTICLE_URL = 'posts/{date:%Y}/{slug}.html'

PATH = 'content'
#OUTPUT_PATH = '/var/www/html/geliba'

TIMEZONE = 'America/Argentina/Buenos_Aires'

DEFAULT_LANG = 'es'

FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

LINKS = (
        ('Cybercirujas', 'http://cybercirujas.rebelion.digital/foro'),
        ('Bibliobox', 'https://bibliobox.copiona.com/'),
        ('Memorias de un Técnico Ligero', 'https://tecnico-ligero.enlacepilar.com.ar'),
        ('Biblioteca EnlaceLibre', 'https://biblioteca-enlacelibre.enlacepilar.com.ar'),
         )

SOCIAL = (('No soy social', '#'),
          ('Acá tampoco hay enlaces', '#'),)

DEFAULT_PAGINATION = 5

DISQUS_SITENAME = "geliba"

THEME = "/home/enlacepilar/Documentos/tema-geliba-para-pelican-sitios-estaticos-sin-js"



